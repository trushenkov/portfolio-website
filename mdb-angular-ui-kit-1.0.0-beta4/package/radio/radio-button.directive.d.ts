export declare class MdbRadioDirective {
    get name(): string;
    set name(value: string);
    private _name;
    get checked(): boolean;
    set checked(value: boolean);
    private _checked;
    get value(): any;
    set value(value: any);
    private _value;
    get disabled(): boolean;
    set disabled(value: boolean);
    private _disabled;
    get isDisabled(): boolean;
    get isChecked(): boolean;
    get nameAttr(): string;
    constructor();
    _updateName(value: string): void;
    _updateChecked(value: boolean): void;
    _updateDisabledState(value: boolean): void;
}
