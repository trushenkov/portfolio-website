import { Observable } from 'rxjs';
export declare abstract class MdbAbstractFormControl<T> {
    readonly stateChanges: Observable<void>;
    readonly labelActive: boolean;
}
