import { ElementRef, AfterViewInit, AfterContentInit, Renderer2, OnDestroy } from '@angular/core';
import { MdbAbstractFormControl } from './form-control';
import { ContentObserver } from '@angular/cdk/observers';
import { Subject } from 'rxjs';
export declare class MdbFormControlComponent implements AfterViewInit, AfterContentInit, OnDestroy {
    private _renderer;
    private _contentObserver;
    _notchLeading: ElementRef;
    _notchMiddle: ElementRef;
    _input: ElementRef;
    _formControl: MdbAbstractFormControl<any>;
    _label: ElementRef;
    outline: boolean;
    constructor(_renderer: Renderer2, _contentObserver: ContentObserver);
    readonly _destroy$: Subject<void>;
    private _notchLeadingLength;
    private _labelMarginLeft;
    private _labelGapPadding;
    private _labelScale;
    ngAfterViewInit(): void;
    ngAfterContentInit(): void;
    ngOnDestroy(): void;
    private _getLabelWidth;
    private _updateBorderGap;
    private _updateLabelActiveState;
    private _isLabelActive;
}
