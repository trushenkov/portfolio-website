import { ElementRef, Renderer2 } from '@angular/core';
import { Subject } from 'rxjs';
import { MdbAbstractFormControl } from './form-control';
export declare class MdbInputDirective implements MdbAbstractFormControl<any> {
    private _elementRef;
    private _renderer;
    constructor(_elementRef: ElementRef, _renderer: Renderer2);
    readonly stateChanges: Subject<void>;
    private _focused;
    get disabled(): boolean;
    set disabled(value: boolean);
    private _disabled;
    get readonly(): boolean;
    set readonly(value: boolean);
    private _readonly;
    get value(): string;
    set value(value: string);
    private _value;
    _onFocus(): void;
    _onBlur(): void;
    get hasValue(): boolean;
    get focused(): boolean;
    get labelActive(): boolean;
}
