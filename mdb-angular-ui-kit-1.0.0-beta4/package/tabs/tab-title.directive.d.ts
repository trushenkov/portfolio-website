import { InjectionToken, TemplateRef } from '@angular/core';
export declare const MDB_TAB_TITLE: InjectionToken<MdbTabTitleDirective>;
export declare class MdbTabTitleDirective {
    template: TemplateRef<any>;
    constructor(template: TemplateRef<any>);
}
