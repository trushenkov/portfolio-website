import { AfterContentInit, EventEmitter, OnDestroy, QueryList } from '@angular/core';
import { Subject } from 'rxjs';
import { MdbTabComponent } from './tab.component';
export declare class MdbTabChange {
    index: number;
    tab: MdbTabComponent;
}
export declare class MdbTabsComponent implements AfterContentInit, OnDestroy {
    tabs: QueryList<MdbTabComponent>;
    readonly _destroy$: Subject<void>;
    fill: boolean;
    justified: boolean;
    pills: boolean;
    vertical: boolean;
    private _selectedIndex;
    activeTabChange: EventEmitter<MdbTabChange>;
    constructor();
    ngAfterContentInit(): void;
    setActiveTab(index: number): void;
    private _getTabChangeEvent;
    private _getClosestTabIndex;
    ngOnDestroy(): void;
}
