import { TemplatePortal } from '@angular/cdk/portal';
import { ElementRef, OnInit, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subject } from 'rxjs';
export declare class MdbTabComponent implements OnInit {
    private _elementRef;
    private _renderer;
    private _vcr;
    _lazyContent: TemplateRef<any>;
    _titleContent: TemplateRef<any>;
    _content: TemplateRef<any>;
    readonly activeStateChange$: Subject<boolean>;
    disabled: boolean;
    title: string;
    get active(): boolean;
    get content(): TemplatePortal | null;
    get titleContent(): TemplatePortal | null;
    get shouldAttach(): boolean;
    private _contentPortal;
    private _titlePortal;
    set active(value: boolean);
    private _active;
    constructor(_elementRef: ElementRef, _renderer: Renderer2, _vcr: ViewContainerRef);
    ngOnInit(): void;
    private _createContentPortal;
    private _createTitlePortal;
}
