import { InjectionToken, TemplateRef } from '@angular/core';
export declare const MDB_TAB_CONTENT: InjectionToken<MdbTabContentDirective>;
export declare class MdbTabContentDirective {
    template: TemplateRef<any>;
    constructor(template: TemplateRef<any>);
}
