import { CdkPortalOutlet } from '@angular/cdk/portal';
import { ComponentFactoryResolver, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { Subject } from 'rxjs';
import { MdbTabComponent } from './tab.component';
export declare class MdbTabPortalOutlet extends CdkPortalOutlet implements OnInit, OnDestroy {
    readonly _destroy$: Subject<void>;
    tab: MdbTabComponent;
    constructor(_cfr: ComponentFactoryResolver, _vcr: ViewContainerRef, _document: any);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
