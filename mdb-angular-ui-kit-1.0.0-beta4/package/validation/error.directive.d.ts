import { ElementRef, Renderer2, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
export declare class MdbErrorDirective implements OnInit, OnDestroy {
    private _elementRef;
    private renderer;
    id: string;
    errorMsg: boolean;
    messageId: string;
    readonly _destroy$: Subject<void>;
    constructor(_elementRef: ElementRef, renderer: Renderer2);
    private _getClosestEl;
    ngOnInit(): void;
    ngOnDestroy(): void;
}
