import { ElementRef, OnInit } from '@angular/core';
export declare class MdbCarouselItemComponent implements OnInit {
    private _elementRef;
    interval: number | null;
    carouselItem: boolean;
    active: boolean;
    next: boolean;
    prev: boolean;
    start: boolean;
    end: boolean;
    get host(): HTMLElement;
    constructor(_elementRef: ElementRef);
    ngOnInit(): void;
}
