import { AfterViewInit, ChangeDetectorRef, ElementRef, EventEmitter, OnDestroy, QueryList } from '@angular/core';
import { MdbCarouselItemComponent } from './carousel-item.component';
export declare enum Direction {
    UNKNOWN = 0,
    NEXT = 1,
    PREV = 2
}
export declare class MdbCarouselComponent implements AfterViewInit, OnDestroy {
    private _elementRef;
    private _cdRef;
    _items: QueryList<MdbCarouselItemComponent>;
    get items(): MdbCarouselItemComponent[];
    animation: 'slide' | 'fade';
    controls: boolean;
    dark: boolean;
    indicators: boolean;
    ride: boolean;
    get interval(): number;
    set interval(value: number);
    private _interval;
    keyboard: boolean;
    pause: boolean;
    wrap: boolean;
    slide: EventEmitter<void>;
    slideChange: EventEmitter<void>;
    get activeSlide(): number;
    set activeSlide(index: number);
    private _activeSlide;
    private _lastInterval;
    private _isPlaying;
    private _isSliding;
    private readonly _destroy$;
    onMouseEnter(): void;
    onMouseLeave(): void;
    constructor(_elementRef: ElementRef, _cdRef: ChangeDetectorRef);
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    private _setActiveSlide;
    private _restartInterval;
    private _resetInterval;
    play(): void;
    stop(): void;
    to(index: number): void;
    next(): void;
    prev(): void;
    private _slide;
    private _animateSlides;
    private _reflow;
    private _emulateTransitionEnd;
    private _getNewSlideIndex;
    private _getNextSlideIndex;
    private _getPrevSlideIndex;
}
