import { MdbCollapseDirective } from './collapse.directive';
import { NgModule } from '@angular/core';
export class MdbCollapseModule {
}
MdbCollapseModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbCollapseDirective],
                exports: [MdbCollapseDirective],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Ii4uLy4uLy4uL3Byb2plY3RzL21kYi1hbmd1bGFyLXVpLWtpdC8iLCJzb3VyY2VzIjpbImNvbGxhcHNlL2NvbGxhcHNlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBTXpDLE1BQU0sT0FBTyxpQkFBaUI7OztZQUo3QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3BDLE9BQU8sRUFBRSxDQUFDLG9CQUFvQixDQUFDO2FBQ2hDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWRiQ29sbGFwc2VEaXJlY3RpdmUgfSBmcm9tICcuL2NvbGxhcHNlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtNZGJDb2xsYXBzZURpcmVjdGl2ZV0sXG4gIGV4cG9ydHM6IFtNZGJDb2xsYXBzZURpcmVjdGl2ZV0sXG59KVxuZXhwb3J0IGNsYXNzIE1kYkNvbGxhcHNlTW9kdWxlIHt9XG4iXX0=