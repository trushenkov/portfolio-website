import { Directive, InjectionToken, TemplateRef } from '@angular/core';
export const MDB_TAB_CONTENT = new InjectionToken('MdbTabContentDirective');
export class MdbTabContentDirective {
    constructor(template) {
        this.template = template;
    }
}
MdbTabContentDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbTabContent]',
                providers: [{ provide: MDB_TAB_CONTENT, useExisting: MdbTabContentDirective }],
            },] }
];
MdbTabContentDirective.ctorParameters = () => [
    { type: TemplateRef }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiLWNvbnRlbnQuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Ii4uLy4uLy4uL3Byb2plY3RzL21kYi1hbmd1bGFyLXVpLWtpdC8iLCJzb3VyY2VzIjpbInRhYnMvdGFiLWNvbnRlbnQuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV2RSxNQUFNLENBQUMsTUFBTSxlQUFlLEdBQUcsSUFBSSxjQUFjLENBQXlCLHdCQUF3QixDQUFDLENBQUM7QUFPcEcsTUFBTSxPQUFPLHNCQUFzQjtJQUNqQyxZQUFtQixRQUEwQjtRQUExQixhQUFRLEdBQVIsUUFBUSxDQUFrQjtJQUFHLENBQUM7OztZQU5sRCxTQUFTLFNBQUM7Z0JBQ1QsK0NBQStDO2dCQUMvQyxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQixTQUFTLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsV0FBVyxFQUFFLHNCQUFzQixFQUFFLENBQUM7YUFDL0U7OztZQVJtQyxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbmplY3Rpb25Ub2tlbiwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGNvbnN0IE1EQl9UQUJfQ09OVEVOVCA9IG5ldyBJbmplY3Rpb25Ub2tlbjxNZGJUYWJDb250ZW50RGlyZWN0aXZlPignTWRiVGFiQ29udGVudERpcmVjdGl2ZScpO1xuXG5ARGlyZWN0aXZlKHtcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkaXJlY3RpdmUtc2VsZWN0b3JcbiAgc2VsZWN0b3I6ICdbbWRiVGFiQ29udGVudF0nLFxuICBwcm92aWRlcnM6IFt7IHByb3ZpZGU6IE1EQl9UQUJfQ09OVEVOVCwgdXNlRXhpc3Rpbmc6IE1kYlRhYkNvbnRlbnREaXJlY3RpdmUgfV0sXG59KVxuZXhwb3J0IGNsYXNzIE1kYlRhYkNvbnRlbnREaXJlY3RpdmUge1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT4pIHt9XG59XG4iXX0=