import { Directive, InjectionToken, TemplateRef } from '@angular/core';
export const MDB_TAB_TITLE = new InjectionToken('MdbTabTitleDirective');
export class MdbTabTitleDirective {
    constructor(template) {
        this.template = template;
    }
}
MdbTabTitleDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbTabTitle]',
                providers: [{ provide: MDB_TAB_TITLE, useExisting: MdbTabTitleDirective }],
            },] }
];
MdbTabTitleDirective.ctorParameters = () => [
    { type: TemplateRef }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiLXRpdGxlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIuLi8uLi8uLi9wcm9qZWN0cy9tZGItYW5ndWxhci11aS1raXQvIiwic291cmNlcyI6WyJ0YWJzL3RhYi10aXRsZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXZFLE1BQU0sQ0FBQyxNQUFNLGFBQWEsR0FBRyxJQUFJLGNBQWMsQ0FBdUIsc0JBQXNCLENBQUMsQ0FBQztBQU85RixNQUFNLE9BQU8sb0JBQW9CO0lBQy9CLFlBQW1CLFFBQTBCO1FBQTFCLGFBQVEsR0FBUixRQUFRLENBQWtCO0lBQUcsQ0FBQzs7O1lBTmxELFNBQVMsU0FBQztnQkFDVCwrQ0FBK0M7Z0JBQy9DLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixTQUFTLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLG9CQUFvQixFQUFFLENBQUM7YUFDM0U7OztZQVJtQyxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbmplY3Rpb25Ub2tlbiwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGNvbnN0IE1EQl9UQUJfVElUTEUgPSBuZXcgSW5qZWN0aW9uVG9rZW48TWRiVGFiVGl0bGVEaXJlY3RpdmU+KCdNZGJUYWJUaXRsZURpcmVjdGl2ZScpO1xuXG5ARGlyZWN0aXZlKHtcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkaXJlY3RpdmUtc2VsZWN0b3JcbiAgc2VsZWN0b3I6ICdbbWRiVGFiVGl0bGVdJyxcbiAgcHJvdmlkZXJzOiBbeyBwcm92aWRlOiBNREJfVEFCX1RJVExFLCB1c2VFeGlzdGluZzogTWRiVGFiVGl0bGVEaXJlY3RpdmUgfV0sXG59KVxuZXhwb3J0IGNsYXNzIE1kYlRhYlRpdGxlRGlyZWN0aXZlIHtcbiAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+KSB7fVxufVxuIl19