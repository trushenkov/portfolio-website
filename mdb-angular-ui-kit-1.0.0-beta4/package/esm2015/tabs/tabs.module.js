import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdbTabComponent } from './tab.component';
import { MdbTabsComponent } from './tabs.component';
import { PortalModule } from '@angular/cdk/portal';
import { MdbTabContentDirective } from './tab-content.directive';
import { MdbTabPortalOutlet } from './tab-outlet.directive';
import { MdbTabTitleDirective } from './tab-title.directive';
export class MdbTabsModule {
}
MdbTabsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    MdbTabComponent,
                    MdbTabContentDirective,
                    MdbTabTitleDirective,
                    MdbTabPortalOutlet,
                    MdbTabsComponent,
                ],
                imports: [CommonModule, PortalModule],
                exports: [
                    MdbTabComponent,
                    MdbTabContentDirective,
                    MdbTabTitleDirective,
                    MdbTabPortalOutlet,
                    MdbTabsComponent,
                ],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiLi4vLi4vLi4vcHJvamVjdHMvbWRiLWFuZ3VsYXItdWkta2l0LyIsInNvdXJjZXMiOlsidGFicy90YWJzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBbUI3RCxNQUFNLE9BQU8sYUFBYTs7O1lBakJ6QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLGVBQWU7b0JBQ2Ysc0JBQXNCO29CQUN0QixvQkFBb0I7b0JBQ3BCLGtCQUFrQjtvQkFDbEIsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDO2dCQUNyQyxPQUFPLEVBQUU7b0JBQ1AsZUFBZTtvQkFDZixzQkFBc0I7b0JBQ3RCLG9CQUFvQjtvQkFDcEIsa0JBQWtCO29CQUNsQixnQkFBZ0I7aUJBQ2pCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE1kYlRhYkNvbXBvbmVudCB9IGZyb20gJy4vdGFiLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNZGJUYWJzQ29tcG9uZW50IH0gZnJvbSAnLi90YWJzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQb3J0YWxNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jZGsvcG9ydGFsJztcbmltcG9ydCB7IE1kYlRhYkNvbnRlbnREaXJlY3RpdmUgfSBmcm9tICcuL3RhYi1jb250ZW50LmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBNZGJUYWJQb3J0YWxPdXRsZXQgfSBmcm9tICcuL3RhYi1vdXRsZXQuZGlyZWN0aXZlJztcbmltcG9ydCB7IE1kYlRhYlRpdGxlRGlyZWN0aXZlIH0gZnJvbSAnLi90YWItdGl0bGUuZGlyZWN0aXZlJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgTWRiVGFiQ29tcG9uZW50LFxuICAgIE1kYlRhYkNvbnRlbnREaXJlY3RpdmUsXG4gICAgTWRiVGFiVGl0bGVEaXJlY3RpdmUsXG4gICAgTWRiVGFiUG9ydGFsT3V0bGV0LFxuICAgIE1kYlRhYnNDb21wb25lbnQsXG4gIF0sXG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIFBvcnRhbE1vZHVsZV0sXG4gIGV4cG9ydHM6IFtcbiAgICBNZGJUYWJDb21wb25lbnQsXG4gICAgTWRiVGFiQ29udGVudERpcmVjdGl2ZSxcbiAgICBNZGJUYWJUaXRsZURpcmVjdGl2ZSxcbiAgICBNZGJUYWJQb3J0YWxPdXRsZXQsXG4gICAgTWRiVGFic0NvbXBvbmVudCxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgTWRiVGFic01vZHVsZSB7fVxuIl19