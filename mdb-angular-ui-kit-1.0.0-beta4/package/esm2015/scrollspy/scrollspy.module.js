import { NgModule } from '@angular/core';
import { MdbScrollspyDirective } from './scrollspy.directive';
import { MdbScrollspyLinkDirective } from './scrollspy-link.directive';
import { MdbScrollspyElementDirective } from './scrollspy-element.directive';
import { MdbScrollspyService } from './scrollspy.service';
import { MdbScrollspyWindowDirective } from './scrollspy-window.directive';
export class MdbScrollspyModule {
}
MdbScrollspyModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    MdbScrollspyDirective,
                    MdbScrollspyLinkDirective,
                    MdbScrollspyElementDirective,
                    MdbScrollspyWindowDirective,
                ],
                exports: [
                    MdbScrollspyDirective,
                    MdbScrollspyLinkDirective,
                    MdbScrollspyElementDirective,
                    MdbScrollspyWindowDirective,
                ],
                providers: [MdbScrollspyService],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsc3B5Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIuLi8uLi8uLi9wcm9qZWN0cy9tZGItYW5ndWxhci11aS1raXQvIiwic291cmNlcyI6WyJzY3JvbGxzcHkvc2Nyb2xsc3B5Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXpDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzlELE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3ZFLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQzFELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBaUIzRSxNQUFNLE9BQU8sa0JBQWtCOzs7WUFmOUIsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixxQkFBcUI7b0JBQ3JCLHlCQUF5QjtvQkFDekIsNEJBQTRCO29CQUM1QiwyQkFBMkI7aUJBQzVCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxxQkFBcUI7b0JBQ3JCLHlCQUF5QjtvQkFDekIsNEJBQTRCO29CQUM1QiwyQkFBMkI7aUJBQzVCO2dCQUNELFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2FBQ2pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgTWRiU2Nyb2xsc3B5RGlyZWN0aXZlIH0gZnJvbSAnLi9zY3JvbGxzcHkuZGlyZWN0aXZlJztcbmltcG9ydCB7IE1kYlNjcm9sbHNweUxpbmtEaXJlY3RpdmUgfSBmcm9tICcuL3Njcm9sbHNweS1saW5rLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBNZGJTY3JvbGxzcHlFbGVtZW50RGlyZWN0aXZlIH0gZnJvbSAnLi9zY3JvbGxzcHktZWxlbWVudC5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgTWRiU2Nyb2xsc3B5U2VydmljZSB9IGZyb20gJy4vc2Nyb2xsc3B5LnNlcnZpY2UnO1xuaW1wb3J0IHsgTWRiU2Nyb2xsc3B5V2luZG93RGlyZWN0aXZlIH0gZnJvbSAnLi9zY3JvbGxzcHktd2luZG93LmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIE1kYlNjcm9sbHNweURpcmVjdGl2ZSxcbiAgICBNZGJTY3JvbGxzcHlMaW5rRGlyZWN0aXZlLFxuICAgIE1kYlNjcm9sbHNweUVsZW1lbnREaXJlY3RpdmUsXG4gICAgTWRiU2Nyb2xsc3B5V2luZG93RGlyZWN0aXZlLFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgTWRiU2Nyb2xsc3B5RGlyZWN0aXZlLFxuICAgIE1kYlNjcm9sbHNweUxpbmtEaXJlY3RpdmUsXG4gICAgTWRiU2Nyb2xsc3B5RWxlbWVudERpcmVjdGl2ZSxcbiAgICBNZGJTY3JvbGxzcHlXaW5kb3dEaXJlY3RpdmUsXG4gIF0sXG4gIHByb3ZpZGVyczogW01kYlNjcm9sbHNweVNlcnZpY2VdLFxufSlcbmV4cG9ydCBjbGFzcyBNZGJTY3JvbGxzcHlNb2R1bGUge31cbiJdfQ==