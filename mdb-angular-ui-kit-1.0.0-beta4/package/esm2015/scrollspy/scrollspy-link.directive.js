import { Directive, Input, HostListener, HostBinding, ChangeDetectorRef, Inject, } from '@angular/core';
import { DOCUMENT } from '@angular/common';
export class MdbScrollspyLinkDirective {
    constructor(cdRef, document) {
        this.cdRef = cdRef;
        this.document = document;
        this._scrollIntoView = true;
        this.scrollspyLink = true;
        this.active = false;
    }
    get scrollIntoView() {
        return this._scrollIntoView;
    }
    set scrollIntoView(value) {
        this._scrollIntoView = value;
    }
    get section() {
        return this._section;
    }
    set section(value) {
        if (value) {
            this._section = value;
        }
    }
    get id() {
        return this._id;
    }
    set id(newId) {
        if (newId) {
            this._id = newId;
        }
    }
    onClick() {
        if (this.section && this.scrollIntoView === true) {
            this.section.scrollIntoView();
        }
    }
    detectChanges() {
        this.cdRef.detectChanges();
    }
    assignSectionToId() {
        this.section = this.document.documentElement.querySelector(`#${this.id}`);
    }
    ngOnInit() {
        this.assignSectionToId();
    }
}
MdbScrollspyLinkDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbScrollspyLink]',
            },] }
];
MdbScrollspyLinkDirective.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
MdbScrollspyLinkDirective.propDecorators = {
    scrollIntoView: [{ type: Input }],
    id: [{ type: Input, args: ['mdbScrollspyLink',] }],
    scrollspyLink: [{ type: HostBinding, args: ['class.scrollspy-link',] }],
    active: [{ type: HostBinding, args: ['class.active',] }],
    onClick: [{ type: HostListener, args: ['click', [],] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsc3B5LWxpbmsuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Ii4uLy4uLy4uL3Byb2plY3RzL21kYi1hbmd1bGFyLXVpLWtpdC8iLCJzb3VyY2VzIjpbInNjcm9sbHNweS9zY3JvbGxzcHktbGluay5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFFVCxLQUFLLEVBQ0wsWUFBWSxFQUNaLFdBQVcsRUFDWCxpQkFBaUIsRUFDakIsTUFBTSxHQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQU0zQyxNQUFNLE9BQU8seUJBQXlCO0lBcUJwQyxZQUFvQixLQUF3QixFQUE0QixRQUFhO1FBQWpFLFVBQUssR0FBTCxLQUFLLENBQW1CO1FBQTRCLGFBQVEsR0FBUixRQUFRLENBQUs7UUFiN0Usb0JBQWUsR0FBRyxJQUFJLENBQUM7UUEwQi9CLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBR3JCLFdBQU0sR0FBRyxLQUFLLENBQUM7SUFoQnlFLENBQUM7SUFwQnpGLElBQ0ksY0FBYztRQUNoQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDOUIsQ0FBQztJQUNELElBQUksY0FBYyxDQUFDLEtBQWM7UUFDL0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUdELElBQUksT0FBTztRQUNULE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN2QixDQUFDO0lBQ0QsSUFBSSxPQUFPLENBQUMsS0FBa0I7UUFDNUIsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztTQUN2QjtJQUNILENBQUM7SUFNRCxJQUNJLEVBQUU7UUFDSixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDbEIsQ0FBQztJQUNELElBQUksRUFBRSxDQUFDLEtBQWE7UUFDbEIsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztTQUNsQjtJQUNILENBQUM7SUFTRCxPQUFPO1FBQ0wsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssSUFBSSxFQUFFO1lBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDL0I7SUFDSCxDQUFDO0lBRUQsYUFBYTtRQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELGlCQUFpQjtRQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7WUE1REYsU0FBUyxTQUFDO2dCQUNULCtDQUErQztnQkFDL0MsUUFBUSxFQUFFLG9CQUFvQjthQUMvQjs7O1lBUkMsaUJBQWlCOzRDQThCOEIsTUFBTSxTQUFDLFFBQVE7Ozs2QkFwQjdELEtBQUs7aUJBc0JMLEtBQUssU0FBQyxrQkFBa0I7NEJBVXhCLFdBQVcsU0FBQyxzQkFBc0I7cUJBR2xDLFdBQVcsU0FBQyxjQUFjO3NCQUcxQixZQUFZLFNBQUMsT0FBTyxFQUFFLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIE9uSW5pdCxcbiAgSW5wdXQsXG4gIEhvc3RMaXN0ZW5lcixcbiAgSG9zdEJpbmRpbmcsXG4gIENoYW5nZURldGVjdG9yUmVmLFxuICBJbmplY3QsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRE9DVU1FTlQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5ARGlyZWN0aXZlKHtcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkaXJlY3RpdmUtc2VsZWN0b3JcbiAgc2VsZWN0b3I6ICdbbWRiU2Nyb2xsc3B5TGlua10nLFxufSlcbmV4cG9ydCBjbGFzcyBNZGJTY3JvbGxzcHlMaW5rRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KClcbiAgZ2V0IHNjcm9sbEludG9WaWV3KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9zY3JvbGxJbnRvVmlldztcbiAgfVxuICBzZXQgc2Nyb2xsSW50b1ZpZXcodmFsdWU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLl9zY3JvbGxJbnRvVmlldyA9IHZhbHVlO1xuICB9XG4gIHByaXZhdGUgX3Njcm9sbEludG9WaWV3ID0gdHJ1ZTtcblxuICBnZXQgc2VjdGlvbigpOiBIVE1MRWxlbWVudCB7XG4gICAgcmV0dXJuIHRoaXMuX3NlY3Rpb247XG4gIH1cbiAgc2V0IHNlY3Rpb24odmFsdWU6IEhUTUxFbGVtZW50KSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICB0aGlzLl9zZWN0aW9uID0gdmFsdWU7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgX3NlY3Rpb246IEhUTUxFbGVtZW50O1xuICBwcml2YXRlIF9pZDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY2RSZWY6IENoYW5nZURldGVjdG9yUmVmLCBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBhbnkpIHt9XG5cbiAgQElucHV0KCdtZGJTY3JvbGxzcHlMaW5rJylcbiAgZ2V0IGlkKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX2lkO1xuICB9XG4gIHNldCBpZChuZXdJZDogc3RyaW5nKSB7XG4gICAgaWYgKG5ld0lkKSB7XG4gICAgICB0aGlzLl9pZCA9IG5ld0lkO1xuICAgIH1cbiAgfVxuXG4gIEBIb3N0QmluZGluZygnY2xhc3Muc2Nyb2xsc3B5LWxpbmsnKVxuICBzY3JvbGxzcHlMaW5rID0gdHJ1ZTtcblxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmFjdGl2ZScpXG4gIGFjdGl2ZSA9IGZhbHNlO1xuXG4gIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgW10pXG4gIG9uQ2xpY2soKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuc2VjdGlvbiAmJiB0aGlzLnNjcm9sbEludG9WaWV3ID09PSB0cnVlKSB7XG4gICAgICB0aGlzLnNlY3Rpb24uc2Nyb2xsSW50b1ZpZXcoKTtcbiAgICB9XG4gIH1cblxuICBkZXRlY3RDaGFuZ2VzKCk6IHZvaWQge1xuICAgIHRoaXMuY2RSZWYuZGV0ZWN0Q2hhbmdlcygpO1xuICB9XG5cbiAgYXNzaWduU2VjdGlvblRvSWQoKTogdm9pZCB7XG4gICAgdGhpcy5zZWN0aW9uID0gdGhpcy5kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQucXVlcnlTZWxlY3RvcihgIyR7dGhpcy5pZH1gKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuYXNzaWduU2VjdGlvblRvSWQoKTtcbiAgfVxufVxuIl19