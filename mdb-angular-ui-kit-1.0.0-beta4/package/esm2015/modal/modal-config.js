// tslint:disable: no-inferrable-types
export class MdbModalConfig {
    constructor() {
        this.animation = true;
        this.backdrop = true;
        this.ignoreBackdropClick = false;
        this.keyboard = true;
        this.modalClass = '';
        this.containerClass = '';
        this.data = null;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtY29uZmlnLmpzIiwic291cmNlUm9vdCI6Ii4uLy4uLy4uL3Byb2plY3RzL21kYi1hbmd1bGFyLXVpLWtpdC8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxzQ0FBc0M7QUFDdEMsTUFBTSxPQUFPLGNBQWM7SUFBM0I7UUFDRSxjQUFTLEdBQWEsSUFBSSxDQUFDO1FBQzNCLGFBQVEsR0FBYSxJQUFJLENBQUM7UUFDMUIsd0JBQW1CLEdBQWEsS0FBSyxDQUFDO1FBQ3RDLGFBQVEsR0FBYSxJQUFJLENBQUM7UUFDMUIsZUFBVSxHQUFZLEVBQUUsQ0FBQztRQUN6QixtQkFBYyxHQUFZLEVBQUUsQ0FBQztRQUM3QixTQUFJLEdBQWMsSUFBSSxDQUFDO0lBRXpCLENBQUM7Q0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuLy8gdHNsaW50OmRpc2FibGU6IG5vLWluZmVycmFibGUtdHlwZXNcbmV4cG9ydCBjbGFzcyBNZGJNb2RhbENvbmZpZzxEID0gYW55PiB7XG4gIGFuaW1hdGlvbj86IGJvb2xlYW4gPSB0cnVlO1xuICBiYWNrZHJvcD86IGJvb2xlYW4gPSB0cnVlO1xuICBpZ25vcmVCYWNrZHJvcENsaWNrPzogYm9vbGVhbiA9IGZhbHNlO1xuICBrZXlib2FyZD86IGJvb2xlYW4gPSB0cnVlO1xuICBtb2RhbENsYXNzPzogc3RyaW5nID0gJyc7XG4gIGNvbnRhaW5lckNsYXNzPzogc3RyaW5nID0gJyc7XG4gIGRhdGE/OiBEIHwgbnVsbCA9IG51bGw7XG4gIHZpZXdDb250YWluZXJSZWY/OiBWaWV3Q29udGFpbmVyUmVmO1xufVxuIl19