import { Component, ElementRef, HostBinding, Input } from '@angular/core';
export class MdbCarouselItemComponent {
    constructor(_elementRef) {
        this._elementRef = _elementRef;
        this.interval = null;
        this.carouselItem = true;
        this.active = false;
        this.next = false;
        this.prev = false;
        this.start = false;
        this.end = false;
    }
    get host() {
        return this._elementRef.nativeElement;
    }
    ngOnInit() { }
}
MdbCarouselItemComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-carousel-item',
                template: '<ng-content></ng-content>'
            },] }
];
MdbCarouselItemComponent.ctorParameters = () => [
    { type: ElementRef }
];
MdbCarouselItemComponent.propDecorators = {
    interval: [{ type: Input }],
    carouselItem: [{ type: HostBinding, args: ['class.carousel-item',] }],
    active: [{ type: HostBinding, args: ['class.active',] }],
    next: [{ type: HostBinding, args: ['class.carousel-item-next',] }],
    prev: [{ type: HostBinding, args: ['class.carousel-item-prev',] }],
    start: [{ type: HostBinding, args: ['class.carousel-item-start',] }],
    end: [{ type: HostBinding, args: ['class.carousel-item-end',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiLi4vLi4vLi4vcHJvamVjdHMvbWRiLWFuZ3VsYXItdWkta2l0LyIsInNvdXJjZXMiOlsiY2Fyb3VzZWwvY2Fyb3VzZWwtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRixNQUFNLE9BQU8sd0JBQXdCO0lBaUJuQyxZQUFvQixXQUF1QjtRQUF2QixnQkFBVyxHQUFYLFdBQVcsQ0FBWTtRQWhCbEMsYUFBUSxHQUFrQixJQUFJLENBQUM7UUFHeEMsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFFUyxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBRUgsU0FBSSxHQUFHLEtBQUssQ0FBQztRQUNiLFNBQUksR0FBRyxLQUFLLENBQUM7UUFDWixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLFFBQUcsR0FBRyxLQUFLLENBQUM7SUFNTixDQUFDO0lBSi9DLElBQUksSUFBSTtRQUNOLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDeEMsQ0FBQztJQUlELFFBQVEsS0FBVSxDQUFDOzs7WUF4QnBCLFNBQVMsU0FBQztnQkFDVCwrQ0FBK0M7Z0JBQy9DLFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLFFBQVEsRUFBRSwyQkFBMkI7YUFDdEM7OztZQU5tQixVQUFVOzs7dUJBUTNCLEtBQUs7MkJBRUwsV0FBVyxTQUFDLHFCQUFxQjtxQkFHakMsV0FBVyxTQUFDLGNBQWM7bUJBRTFCLFdBQVcsU0FBQywwQkFBMEI7bUJBQ3RDLFdBQVcsU0FBQywwQkFBMEI7b0JBQ3RDLFdBQVcsU0FBQywyQkFBMkI7a0JBQ3ZDLFdBQVcsU0FBQyx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGNvbXBvbmVudC1zZWxlY3RvclxuICBzZWxlY3RvcjogJ21kYi1jYXJvdXNlbC1pdGVtJyxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+Jyxcbn0pXG5leHBvcnQgY2xhc3MgTWRiQ2Fyb3VzZWxJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgaW50ZXJ2YWw6IG51bWJlciB8IG51bGwgPSBudWxsO1xuXG4gIEBIb3N0QmluZGluZygnY2xhc3MuY2Fyb3VzZWwtaXRlbScpXG4gIGNhcm91c2VsSXRlbSA9IHRydWU7XG5cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5hY3RpdmUnKSBhY3RpdmUgPSBmYWxzZTtcblxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmNhcm91c2VsLWl0ZW0tbmV4dCcpIG5leHQgPSBmYWxzZTtcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5jYXJvdXNlbC1pdGVtLXByZXYnKSBwcmV2ID0gZmFsc2U7XG4gIEBIb3N0QmluZGluZygnY2xhc3MuY2Fyb3VzZWwtaXRlbS1zdGFydCcpIHN0YXJ0ID0gZmFsc2U7XG4gIEBIb3N0QmluZGluZygnY2xhc3MuY2Fyb3VzZWwtaXRlbS1lbmQnKSBlbmQgPSBmYWxzZTtcblxuICBnZXQgaG9zdCgpOiBIVE1MRWxlbWVudCB7XG4gICAgcmV0dXJuIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudDtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWYpIHt9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7fVxufVxuIl19