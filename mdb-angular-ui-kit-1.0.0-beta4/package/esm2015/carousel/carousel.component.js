import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChildren, ElementRef, EventEmitter, HostListener, Input, Output, } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MdbCarouselItemComponent } from './carousel-item.component';
export var Direction;
(function (Direction) {
    Direction[Direction["UNKNOWN"] = 0] = "UNKNOWN";
    Direction[Direction["NEXT"] = 1] = "NEXT";
    Direction[Direction["PREV"] = 2] = "PREV";
})(Direction || (Direction = {}));
export class MdbCarouselComponent {
    constructor(_elementRef, _cdRef) {
        this._elementRef = _elementRef;
        this._cdRef = _cdRef;
        this.animation = 'slide';
        this.controls = false;
        this.dark = false;
        this.indicators = false;
        this.ride = true;
        this._interval = 5000;
        this.keyboard = true;
        this.pause = true;
        this.wrap = true;
        this.slide = new EventEmitter();
        this.slideChange = new EventEmitter();
        this._activeSlide = 0;
        this._isPlaying = false;
        this._isSliding = false;
        this._destroy$ = new Subject();
    }
    get items() {
        return this._items && this._items.toArray();
    }
    get interval() {
        return this._interval;
    }
    set interval(value) {
        this._interval = value;
        if (this.items) {
            this._restartInterval();
        }
    }
    get activeSlide() {
        return this._activeSlide;
    }
    set activeSlide(index) {
        if (this.items.length && this._activeSlide !== index) {
            this._activeSlide = index;
            this._restartInterval();
        }
    }
    onMouseEnter() {
        if (this.pause && this._isPlaying) {
            this.stop();
        }
    }
    onMouseLeave() {
        if (this.pause && !this._isPlaying) {
            this.play();
        }
    }
    ngAfterViewInit() {
        Promise.resolve().then(() => {
            this._setActiveSlide(this._activeSlide);
            if (this.interval > 0 && this.ride) {
                this.play();
            }
        });
        if (this.keyboard) {
            fromEvent(this._elementRef.nativeElement, 'keydown')
                .pipe(takeUntil(this._destroy$))
                // tslint:disable-next-line: deprecation
                .subscribe((event) => {
                if (event.key === 'ArrowRight') {
                    this.next();
                }
                else if (event.key === 'ArrowLeft') {
                    this.prev();
                }
            });
        }
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
    _setActiveSlide(index) {
        const currentSlide = this.items[this._activeSlide];
        currentSlide.active = false;
        const newSlide = this.items[index];
        newSlide.active = true;
        this._activeSlide = index;
    }
    _restartInterval() {
        this._resetInterval();
        const activeElement = this.items[this.activeSlide];
        const interval = activeElement.interval ? activeElement.interval : this.interval;
        if (!isNaN(interval) && interval > 0) {
            this._lastInterval = setInterval(() => {
                const nInterval = +interval;
                if (this._isPlaying && !isNaN(nInterval) && nInterval > 0) {
                    this.next();
                }
                else {
                    this.stop();
                }
            }, interval);
        }
    }
    _resetInterval() {
        if (this._lastInterval) {
            clearInterval(this._lastInterval);
            this._lastInterval = null;
        }
    }
    play() {
        if (!this._isPlaying) {
            this._isPlaying = true;
            this._restartInterval();
        }
    }
    stop() {
        if (this._isPlaying) {
            this._isPlaying = false;
            this._resetInterval();
        }
    }
    to(index) {
        if (index > this.items.length - 1 || index < 0) {
            return;
        }
        if (this.activeSlide === index) {
            this.stop();
            this.play();
            return;
        }
        const direction = index > this.activeSlide ? Direction.NEXT : Direction.PREV;
        this._animateSlides(direction, this.activeSlide, index);
        this.activeSlide = index;
        this._cdRef.markForCheck();
    }
    next() {
        if (!this._isSliding) {
            this._slide(Direction.NEXT);
        }
        this._cdRef.markForCheck();
    }
    prev() {
        if (!this._isSliding) {
            this._slide(Direction.PREV);
        }
        this._cdRef.markForCheck();
    }
    _slide(direction) {
        const isFirst = this._activeSlide === 0;
        const isLast = this._activeSlide === this.items.length - 1;
        if (!this.wrap) {
            if ((direction === Direction.NEXT && isLast) || (direction === Direction.PREV && isFirst)) {
                return;
            }
        }
        const newSlideIndex = this._getNewSlideIndex(direction);
        this._animateSlides(direction, this.activeSlide, newSlideIndex);
        this.activeSlide = newSlideIndex;
        this.slide.emit();
    }
    _animateSlides(direction, currentIndex, nextIndex) {
        const currentItem = this.items[currentIndex];
        const nextItem = this.items[nextIndex];
        const currentEl = currentItem.host;
        const nextEl = nextItem.host;
        this._isSliding = true;
        if (this._isPlaying) {
            this.stop();
        }
        if (direction === Direction.NEXT) {
            nextItem.next = true;
            setTimeout(() => {
                this._reflow(nextEl);
                currentItem.start = true;
                nextItem.start = true;
            }, 0);
            const transitionDuration = 600;
            fromEvent(currentEl, 'transitionend')
                .pipe(take(1))
                // tslint:disable-next-line: deprecation
                .subscribe(() => {
                nextItem.next = false;
                nextItem.start = false;
                nextItem.active = true;
                currentItem.active = false;
                currentItem.start = false;
                currentItem.next = false;
                this.slideChange.emit();
                this._isSliding = false;
            });
            this._emulateTransitionEnd(currentEl, transitionDuration);
        }
        else if (direction === Direction.PREV) {
            nextItem.prev = true;
            setTimeout(() => {
                this._reflow(nextEl);
                currentItem.end = true;
                nextItem.end = true;
            }, 0);
            const transitionDuration = 600;
            fromEvent(currentEl, 'transitionend')
                .pipe(take(1))
                // tslint:disable-next-line: deprecation
                .subscribe(() => {
                nextItem.prev = false;
                nextItem.end = false;
                nextItem.active = true;
                currentItem.active = false;
                currentItem.end = false;
                currentItem.prev = false;
                this.slideChange.emit();
                this._isSliding = false;
            });
            this._emulateTransitionEnd(currentEl, transitionDuration);
        }
        if (!this._isPlaying && this.interval > 0) {
            this.play();
        }
    }
    _reflow(element) {
        return element.offsetHeight;
    }
    _emulateTransitionEnd(element, duration) {
        let eventEmitted = false;
        const durationPadding = 5;
        const emulatedDuration = duration + durationPadding;
        fromEvent(element, 'transitionend')
            .pipe(take(1))
            // tslint:disable-next-line: deprecation
            .subscribe(() => {
            eventEmitted = true;
        });
        setTimeout(() => {
            if (!eventEmitted) {
                element.dispatchEvent(new Event('transitionend'));
            }
        }, emulatedDuration);
    }
    _getNewSlideIndex(direction) {
        let newSlideIndex;
        if (direction === Direction.NEXT) {
            newSlideIndex = this._getNextSlideIndex();
        }
        if (direction === Direction.PREV) {
            newSlideIndex = this._getPrevSlideIndex();
        }
        return newSlideIndex;
    }
    _getNextSlideIndex() {
        const isLast = this._activeSlide === this.items.length - 1;
        if (!isLast) {
            return this._activeSlide + 1;
        }
        else if (this.wrap && isLast) {
            return 0;
        }
        else {
            return this._activeSlide;
        }
    }
    _getPrevSlideIndex() {
        const isFirst = this._activeSlide === 0;
        if (!isFirst) {
            return this._activeSlide - 1;
        }
        else if (this.wrap && isFirst) {
            return this.items.length - 1;
        }
        else {
            return this._activeSlide;
        }
    }
}
MdbCarouselComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-carousel',
                template: "<div\n  class=\"carousel slide\"\n  [class.carousel-fade]=\"animation === 'fade'\"\n  [class.carousel-dark]=\"dark\"\n>\n  <div class=\"carousel-indicators\" *ngIf=\"indicators\">\n    <button\n      *ngFor=\"let item of items; let i = index\"\n      type=\"button\"\n      [class.active]=\"i === activeSlide\"\n      [attr.aria-current]=\"i === activeSlide\"\n      (click)=\"to(i)\"\n    ></button>\n  </div>\n\n  <div class=\"carousel-inner\">\n    <ng-content></ng-content>\n  </div>\n\n  <button *ngIf=\"controls\" class=\"carousel-control-prev\" type=\"button\" (click)=\"prev()\">\n    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n    <span class=\"visually-hidden\">Previous</span>\n  </button>\n  <button *ngIf=\"controls\" class=\"carousel-control-next\" type=\"button\" (click)=\"next()\">\n    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n    <span class=\"visually-hidden\">Next</span>\n  </button>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush
            },] }
];
MdbCarouselComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: ChangeDetectorRef }
];
MdbCarouselComponent.propDecorators = {
    _items: [{ type: ContentChildren, args: [MdbCarouselItemComponent,] }],
    animation: [{ type: Input }],
    controls: [{ type: Input }],
    dark: [{ type: Input }],
    indicators: [{ type: Input }],
    ride: [{ type: Input }],
    interval: [{ type: Input }],
    keyboard: [{ type: Input }],
    pause: [{ type: Input }],
    wrap: [{ type: Input }],
    slide: [{ type: Output }],
    slideChange: [{ type: Output }],
    onMouseEnter: [{ type: HostListener, args: ['mouseenter',] }],
    onMouseLeave: [{ type: HostListener, args: ['mouseleave',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Ii4uLy4uLy4uL3Byb2plY3RzL21kYi1hbmd1bGFyLXVpLWtpdC8iLCJzb3VyY2VzIjpbImNhcm91c2VsL2Nhcm91c2VsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsdUJBQXVCLEVBQ3ZCLGlCQUFpQixFQUNqQixTQUFTLEVBQ1QsZUFBZSxFQUNmLFVBQVUsRUFDVixZQUFZLEVBQ1osWUFBWSxFQUNaLEtBQUssRUFFTCxNQUFNLEdBRVAsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDMUMsT0FBTyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUVyRSxNQUFNLENBQU4sSUFBWSxTQUlYO0FBSkQsV0FBWSxTQUFTO0lBQ25CLCtDQUFPLENBQUE7SUFDUCx5Q0FBSSxDQUFBO0lBQ0oseUNBQUksQ0FBQTtBQUNOLENBQUMsRUFKVyxTQUFTLEtBQVQsU0FBUyxRQUlwQjtBQVFELE1BQU0sT0FBTyxvQkFBb0I7SUFnRS9CLFlBQW9CLFdBQXVCLEVBQVUsTUFBeUI7UUFBMUQsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFtQjtRQTFEckUsY0FBUyxHQUFxQixPQUFPLENBQUM7UUFDdEMsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixTQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2IsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBYWIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUVoQixhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQUssR0FBRyxJQUFJLENBQUM7UUFDYixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBRVgsVUFBSyxHQUF1QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQy9DLGdCQUFXLEdBQXVCLElBQUksWUFBWSxFQUFFLENBQUM7UUFZdkQsaUJBQVksR0FBRyxDQUFDLENBQUM7UUFHakIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBRVYsY0FBUyxHQUFrQixJQUFJLE9BQU8sRUFBUSxDQUFDO0lBZ0JpQixDQUFDO0lBOURsRixJQUFJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUM5QyxDQUFDO0lBUUQsSUFDSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxJQUFJLFFBQVEsQ0FBQyxLQUFhO1FBQ3hCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBRXZCLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3pCO0lBQ0gsQ0FBQztJQVVELElBQUksV0FBVztRQUNiLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDO0lBRUQsSUFBSSxXQUFXLENBQUMsS0FBYTtRQUMzQixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssS0FBSyxFQUFFO1lBQ3BELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3pCO0lBQ0gsQ0FBQztJQVVELFlBQVk7UUFDVixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFHRCxZQUFZO1FBQ1YsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNsQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFJRCxlQUFlO1FBQ2IsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDMUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFFeEMsSUFBSSxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNsQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDYjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUM7aUJBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNoQyx3Q0FBd0M7aUJBQ3ZDLFNBQVMsQ0FBQyxDQUFDLEtBQW9CLEVBQUUsRUFBRTtnQkFDbEMsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLFlBQVksRUFBRTtvQkFDOUIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2lCQUNiO3FCQUFNLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxXQUFXLEVBQUU7b0JBQ3BDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDYjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRU8sZUFBZSxDQUFDLEtBQWE7UUFDbkMsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbkQsWUFBWSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFFNUIsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRU8sZ0JBQWdCO1FBQ3RCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNuRCxNQUFNLFFBQVEsR0FBRyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBRWpGLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksUUFBUSxHQUFHLENBQUMsRUFBRTtZQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQyxHQUFHLEVBQUU7Z0JBQ3BDLE1BQU0sU0FBUyxHQUFHLENBQUMsUUFBUSxDQUFDO2dCQUM1QixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksU0FBUyxHQUFHLENBQUMsRUFBRTtvQkFDekQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2lCQUNiO3FCQUFNO29CQUNMLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDYjtZQUNILENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUNkO0lBQ0gsQ0FBQztJQUVPLGNBQWM7UUFDcEIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7U0FDM0I7SUFDSCxDQUFDO0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3pCO0lBQ0gsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztJQUVELEVBQUUsQ0FBQyxLQUFhO1FBQ2QsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFDOUMsT0FBTztTQUNSO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLEtBQUssRUFBRTtZQUM5QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDWixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDWixPQUFPO1NBQ1I7UUFFRCxNQUFNLFNBQVMsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUU3RSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBRXpCLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3QjtRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3QjtRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVPLE1BQU0sQ0FBQyxTQUFvQjtRQUNqQyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxLQUFLLENBQUMsQ0FBQztRQUN4QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUUzRCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNkLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxFQUFFO2dCQUN6RixPQUFPO2FBQ1I7U0FDRjtRQUVELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUV4RCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDO1FBRWpDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVPLGNBQWMsQ0FBQyxTQUFvQixFQUFFLFlBQW9CLEVBQUUsU0FBaUI7UUFDbEYsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM3QyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZDLE1BQU0sU0FBUyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUM7UUFDbkMsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztRQUU3QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUV2QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7UUFFRCxJQUFJLFNBQVMsS0FBSyxTQUFTLENBQUMsSUFBSSxFQUFFO1lBQ2hDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRXJCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDckIsV0FBVyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVOLE1BQU0sa0JBQWtCLEdBQUcsR0FBRyxDQUFDO1lBRS9CLFNBQVMsQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDO2lCQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNkLHdDQUF3QztpQkFDdkMsU0FBUyxDQUFDLEdBQUcsRUFBRTtnQkFDZCxRQUFRLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUV2QixXQUFXLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDM0IsV0FBVyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Z0JBQzFCLFdBQVcsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2dCQUV6QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztZQUVMLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztTQUMzRDthQUFNLElBQUksU0FBUyxLQUFLLFNBQVMsQ0FBQyxJQUFJLEVBQUU7WUFDdkMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFFckIsVUFBVSxDQUFDLEdBQUcsRUFBRTtnQkFDZCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNyQixXQUFXLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQztnQkFDdkIsUUFBUSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7WUFDdEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRU4sTUFBTSxrQkFBa0IsR0FBRyxHQUFHLENBQUM7WUFFL0IsU0FBUyxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUM7aUJBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2Qsd0NBQXdDO2lCQUN2QyxTQUFTLENBQUMsR0FBRyxFQUFFO2dCQUNkLFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixRQUFRLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztnQkFDckIsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBRXZCLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixXQUFXLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztnQkFDeEIsV0FBVyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7Z0JBRXpCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1lBRUwsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1NBQzNEO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRU8sT0FBTyxDQUFDLE9BQW9CO1FBQ2xDLE9BQU8sT0FBTyxDQUFDLFlBQVksQ0FBQztJQUM5QixDQUFDO0lBRU8scUJBQXFCLENBQUMsT0FBb0IsRUFBRSxRQUFnQjtRQUNsRSxJQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDekIsTUFBTSxlQUFlLEdBQUcsQ0FBQyxDQUFDO1FBQzFCLE1BQU0sZ0JBQWdCLEdBQUcsUUFBUSxHQUFHLGVBQWUsQ0FBQztRQUVwRCxTQUFTLENBQUMsT0FBTyxFQUFFLGVBQWUsQ0FBQzthQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2Qsd0NBQXdDO2FBQ3ZDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDZCxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBRUwsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ2pCLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQzthQUNuRDtRQUNILENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFTyxpQkFBaUIsQ0FBQyxTQUFvQjtRQUM1QyxJQUFJLGFBQXFCLENBQUM7UUFFMUIsSUFBSSxTQUFTLEtBQUssU0FBUyxDQUFDLElBQUksRUFBRTtZQUNoQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDM0M7UUFFRCxJQUFJLFNBQVMsS0FBSyxTQUFTLENBQUMsSUFBSSxFQUFFO1lBQ2hDLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUMzQztRQUVELE9BQU8sYUFBYSxDQUFDO0lBQ3ZCLENBQUM7SUFFTyxrQkFBa0I7UUFDeEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFM0QsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNYLE9BQU8sSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7U0FDOUI7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxFQUFFO1lBQzlCLE9BQU8sQ0FBQyxDQUFDO1NBQ1Y7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztTQUMxQjtJQUNILENBQUM7SUFFTyxrQkFBa0I7UUFDeEIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFlBQVksS0FBSyxDQUFDLENBQUM7UUFFeEMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNaLE9BQU8sSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7U0FDOUI7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksT0FBTyxFQUFFO1lBQy9CLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1NBQzlCO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7WUE5VUYsU0FBUyxTQUFDO2dCQUNULCtDQUErQztnQkFDL0MsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLDY5QkFBd0M7Z0JBQ3hDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO2FBQ2hEOzs7WUF2QkMsVUFBVTtZQUhWLGlCQUFpQjs7O3FCQTRCaEIsZUFBZSxTQUFDLHdCQUF3Qjt3QkFLeEMsS0FBSzt1QkFDTCxLQUFLO21CQUNMLEtBQUs7eUJBQ0wsS0FBSzttQkFDTCxLQUFLO3VCQUVMLEtBQUs7dUJBYUwsS0FBSztvQkFDTCxLQUFLO21CQUNMLEtBQUs7b0JBRUwsTUFBTTswQkFDTixNQUFNOzJCQW9CTixZQUFZLFNBQUMsWUFBWTsyQkFPekIsWUFBWSxTQUFDLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBBZnRlclZpZXdJbml0LFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcbiAgQ2hhbmdlRGV0ZWN0b3JSZWYsXG4gIENvbXBvbmVudCxcbiAgQ29udGVudENoaWxkcmVuLFxuICBFbGVtZW50UmVmLFxuICBFdmVudEVtaXR0ZXIsXG4gIEhvc3RMaXN0ZW5lcixcbiAgSW5wdXQsXG4gIE9uRGVzdHJveSxcbiAgT3V0cHV0LFxuICBRdWVyeUxpc3QsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgZnJvbUV2ZW50LCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlLCB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBNZGJDYXJvdXNlbEl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Nhcm91c2VsLWl0ZW0uY29tcG9uZW50JztcblxuZXhwb3J0IGVudW0gRGlyZWN0aW9uIHtcbiAgVU5LTk9XTixcbiAgTkVYVCxcbiAgUFJFVixcbn1cblxuQENvbXBvbmVudCh7XG4gIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogY29tcG9uZW50LXNlbGVjdG9yXG4gIHNlbGVjdG9yOiAnbWRiLWNhcm91c2VsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Nhcm91c2VsLmNvbXBvbmVudC5odG1sJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG59KVxuZXhwb3J0IGNsYXNzIE1kYkNhcm91c2VsQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcbiAgQENvbnRlbnRDaGlsZHJlbihNZGJDYXJvdXNlbEl0ZW1Db21wb25lbnQpIF9pdGVtczogUXVlcnlMaXN0PE1kYkNhcm91c2VsSXRlbUNvbXBvbmVudD47XG4gIGdldCBpdGVtcygpOiBNZGJDYXJvdXNlbEl0ZW1Db21wb25lbnRbXSB7XG4gICAgcmV0dXJuIHRoaXMuX2l0ZW1zICYmIHRoaXMuX2l0ZW1zLnRvQXJyYXkoKTtcbiAgfVxuXG4gIEBJbnB1dCgpIGFuaW1hdGlvbjogJ3NsaWRlJyB8ICdmYWRlJyA9ICdzbGlkZSc7XG4gIEBJbnB1dCgpIGNvbnRyb2xzID0gZmFsc2U7XG4gIEBJbnB1dCgpIGRhcmsgPSBmYWxzZTtcbiAgQElucHV0KCkgaW5kaWNhdG9ycyA9IGZhbHNlO1xuICBASW5wdXQoKSByaWRlID0gdHJ1ZTtcblxuICBASW5wdXQoKVxuICBnZXQgaW50ZXJ2YWwoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5faW50ZXJ2YWw7XG4gIH1cbiAgc2V0IGludGVydmFsKHZhbHVlOiBudW1iZXIpIHtcbiAgICB0aGlzLl9pbnRlcnZhbCA9IHZhbHVlO1xuXG4gICAgaWYgKHRoaXMuaXRlbXMpIHtcbiAgICAgIHRoaXMuX3Jlc3RhcnRJbnRlcnZhbCgpO1xuICAgIH1cbiAgfVxuICBwcml2YXRlIF9pbnRlcnZhbCA9IDUwMDA7XG5cbiAgQElucHV0KCkga2V5Ym9hcmQgPSB0cnVlO1xuICBASW5wdXQoKSBwYXVzZSA9IHRydWU7XG4gIEBJbnB1dCgpIHdyYXAgPSB0cnVlO1xuXG4gIEBPdXRwdXQoKSBzbGlkZTogRXZlbnRFbWl0dGVyPHZvaWQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgc2xpZGVDaGFuZ2U6IEV2ZW50RW1pdHRlcjx2b2lkPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBnZXQgYWN0aXZlU2xpZGUoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5fYWN0aXZlU2xpZGU7XG4gIH1cblxuICBzZXQgYWN0aXZlU2xpZGUoaW5kZXg6IG51bWJlcikge1xuICAgIGlmICh0aGlzLml0ZW1zLmxlbmd0aCAmJiB0aGlzLl9hY3RpdmVTbGlkZSAhPT0gaW5kZXgpIHtcbiAgICAgIHRoaXMuX2FjdGl2ZVNsaWRlID0gaW5kZXg7XG4gICAgICB0aGlzLl9yZXN0YXJ0SW50ZXJ2YWwoKTtcbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBfYWN0aXZlU2xpZGUgPSAwO1xuXG4gIHByaXZhdGUgX2xhc3RJbnRlcnZhbDogYW55O1xuICBwcml2YXRlIF9pc1BsYXlpbmcgPSBmYWxzZTtcbiAgcHJpdmF0ZSBfaXNTbGlkaW5nID0gZmFsc2U7XG5cbiAgcHJpdmF0ZSByZWFkb25seSBfZGVzdHJveSQ6IFN1YmplY3Q8dm9pZD4gPSBuZXcgU3ViamVjdDx2b2lkPigpO1xuXG4gIEBIb3N0TGlzdGVuZXIoJ21vdXNlZW50ZXInKVxuICBvbk1vdXNlRW50ZXIoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMucGF1c2UgJiYgdGhpcy5faXNQbGF5aW5nKSB7XG4gICAgICB0aGlzLnN0b3AoKTtcbiAgICB9XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCdtb3VzZWxlYXZlJylcbiAgb25Nb3VzZUxlYXZlKCk6IHZvaWQge1xuICAgIGlmICh0aGlzLnBhdXNlICYmICF0aGlzLl9pc1BsYXlpbmcpIHtcbiAgICAgIHRoaXMucGxheSgpO1xuICAgIH1cbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWYsIHByaXZhdGUgX2NkUmVmOiBDaGFuZ2VEZXRlY3RvclJlZikge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgUHJvbWlzZS5yZXNvbHZlKCkudGhlbigoKSA9PiB7XG4gICAgICB0aGlzLl9zZXRBY3RpdmVTbGlkZSh0aGlzLl9hY3RpdmVTbGlkZSk7XG5cbiAgICAgIGlmICh0aGlzLmludGVydmFsID4gMCAmJiB0aGlzLnJpZGUpIHtcbiAgICAgICAgdGhpcy5wbGF5KCk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBpZiAodGhpcy5rZXlib2FyZCkge1xuICAgICAgZnJvbUV2ZW50KHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2tleWRvd24nKVxuICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fZGVzdHJveSQpKVxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGRlcHJlY2F0aW9uXG4gICAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBLZXlib2FyZEV2ZW50KSA9PiB7XG4gICAgICAgICAgaWYgKGV2ZW50LmtleSA9PT0gJ0Fycm93UmlnaHQnKSB7XG4gICAgICAgICAgICB0aGlzLm5leHQoKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleSA9PT0gJ0Fycm93TGVmdCcpIHtcbiAgICAgICAgICAgIHRoaXMucHJldigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgdGhpcy5fZGVzdHJveSQubmV4dCgpO1xuICAgIHRoaXMuX2Rlc3Ryb3kkLmNvbXBsZXRlKCk7XG4gIH1cblxuICBwcml2YXRlIF9zZXRBY3RpdmVTbGlkZShpbmRleDogbnVtYmVyKTogdm9pZCB7XG4gICAgY29uc3QgY3VycmVudFNsaWRlID0gdGhpcy5pdGVtc1t0aGlzLl9hY3RpdmVTbGlkZV07XG4gICAgY3VycmVudFNsaWRlLmFjdGl2ZSA9IGZhbHNlO1xuXG4gICAgY29uc3QgbmV3U2xpZGUgPSB0aGlzLml0ZW1zW2luZGV4XTtcbiAgICBuZXdTbGlkZS5hY3RpdmUgPSB0cnVlO1xuICAgIHRoaXMuX2FjdGl2ZVNsaWRlID0gaW5kZXg7XG4gIH1cblxuICBwcml2YXRlIF9yZXN0YXJ0SW50ZXJ2YWwoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVzZXRJbnRlcnZhbCgpO1xuICAgIGNvbnN0IGFjdGl2ZUVsZW1lbnQgPSB0aGlzLml0ZW1zW3RoaXMuYWN0aXZlU2xpZGVdO1xuICAgIGNvbnN0IGludGVydmFsID0gYWN0aXZlRWxlbWVudC5pbnRlcnZhbCA/IGFjdGl2ZUVsZW1lbnQuaW50ZXJ2YWwgOiB0aGlzLmludGVydmFsO1xuXG4gICAgaWYgKCFpc05hTihpbnRlcnZhbCkgJiYgaW50ZXJ2YWwgPiAwKSB7XG4gICAgICB0aGlzLl9sYXN0SW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICAgIGNvbnN0IG5JbnRlcnZhbCA9ICtpbnRlcnZhbDtcbiAgICAgICAgaWYgKHRoaXMuX2lzUGxheWluZyAmJiAhaXNOYU4obkludGVydmFsKSAmJiBuSW50ZXJ2YWwgPiAwKSB7XG4gICAgICAgICAgdGhpcy5uZXh0KCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5zdG9wKCk7XG4gICAgICAgIH1cbiAgICAgIH0sIGludGVydmFsKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIF9yZXNldEludGVydmFsKCk6IHZvaWQge1xuICAgIGlmICh0aGlzLl9sYXN0SW50ZXJ2YWwpIHtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5fbGFzdEludGVydmFsKTtcbiAgICAgIHRoaXMuX2xhc3RJbnRlcnZhbCA9IG51bGw7XG4gICAgfVxuICB9XG5cbiAgcGxheSgpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuX2lzUGxheWluZykge1xuICAgICAgdGhpcy5faXNQbGF5aW5nID0gdHJ1ZTtcbiAgICAgIHRoaXMuX3Jlc3RhcnRJbnRlcnZhbCgpO1xuICAgIH1cbiAgfVxuXG4gIHN0b3AoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuX2lzUGxheWluZykge1xuICAgICAgdGhpcy5faXNQbGF5aW5nID0gZmFsc2U7XG4gICAgICB0aGlzLl9yZXNldEludGVydmFsKCk7XG4gICAgfVxuICB9XG5cbiAgdG8oaW5kZXg6IG51bWJlcik6IHZvaWQge1xuICAgIGlmIChpbmRleCA+IHRoaXMuaXRlbXMubGVuZ3RoIC0gMSB8fCBpbmRleCA8IDApIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5hY3RpdmVTbGlkZSA9PT0gaW5kZXgpIHtcbiAgICAgIHRoaXMuc3RvcCgpO1xuICAgICAgdGhpcy5wbGF5KCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgZGlyZWN0aW9uID0gaW5kZXggPiB0aGlzLmFjdGl2ZVNsaWRlID8gRGlyZWN0aW9uLk5FWFQgOiBEaXJlY3Rpb24uUFJFVjtcblxuICAgIHRoaXMuX2FuaW1hdGVTbGlkZXMoZGlyZWN0aW9uLCB0aGlzLmFjdGl2ZVNsaWRlLCBpbmRleCk7XG4gICAgdGhpcy5hY3RpdmVTbGlkZSA9IGluZGV4O1xuXG4gICAgdGhpcy5fY2RSZWYubWFya0ZvckNoZWNrKCk7XG4gIH1cblxuICBuZXh0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5faXNTbGlkaW5nKSB7XG4gICAgICB0aGlzLl9zbGlkZShEaXJlY3Rpb24uTkVYVCk7XG4gICAgfVxuXG4gICAgdGhpcy5fY2RSZWYubWFya0ZvckNoZWNrKCk7XG4gIH1cblxuICBwcmV2KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5faXNTbGlkaW5nKSB7XG4gICAgICB0aGlzLl9zbGlkZShEaXJlY3Rpb24uUFJFVik7XG4gICAgfVxuXG4gICAgdGhpcy5fY2RSZWYubWFya0ZvckNoZWNrKCk7XG4gIH1cblxuICBwcml2YXRlIF9zbGlkZShkaXJlY3Rpb246IERpcmVjdGlvbik6IHZvaWQge1xuICAgIGNvbnN0IGlzRmlyc3QgPSB0aGlzLl9hY3RpdmVTbGlkZSA9PT0gMDtcbiAgICBjb25zdCBpc0xhc3QgPSB0aGlzLl9hY3RpdmVTbGlkZSA9PT0gdGhpcy5pdGVtcy5sZW5ndGggLSAxO1xuXG4gICAgaWYgKCF0aGlzLndyYXApIHtcbiAgICAgIGlmICgoZGlyZWN0aW9uID09PSBEaXJlY3Rpb24uTkVYVCAmJiBpc0xhc3QpIHx8IChkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5QUkVWICYmIGlzRmlyc3QpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zdCBuZXdTbGlkZUluZGV4ID0gdGhpcy5fZ2V0TmV3U2xpZGVJbmRleChkaXJlY3Rpb24pO1xuXG4gICAgdGhpcy5fYW5pbWF0ZVNsaWRlcyhkaXJlY3Rpb24sIHRoaXMuYWN0aXZlU2xpZGUsIG5ld1NsaWRlSW5kZXgpO1xuICAgIHRoaXMuYWN0aXZlU2xpZGUgPSBuZXdTbGlkZUluZGV4O1xuXG4gICAgdGhpcy5zbGlkZS5lbWl0KCk7XG4gIH1cblxuICBwcml2YXRlIF9hbmltYXRlU2xpZGVzKGRpcmVjdGlvbjogRGlyZWN0aW9uLCBjdXJyZW50SW5kZXg6IG51bWJlciwgbmV4dEluZGV4OiBudW1iZXIpOiB2b2lkIHtcbiAgICBjb25zdCBjdXJyZW50SXRlbSA9IHRoaXMuaXRlbXNbY3VycmVudEluZGV4XTtcbiAgICBjb25zdCBuZXh0SXRlbSA9IHRoaXMuaXRlbXNbbmV4dEluZGV4XTtcbiAgICBjb25zdCBjdXJyZW50RWwgPSBjdXJyZW50SXRlbS5ob3N0O1xuICAgIGNvbnN0IG5leHRFbCA9IG5leHRJdGVtLmhvc3Q7XG5cbiAgICB0aGlzLl9pc1NsaWRpbmcgPSB0cnVlO1xuXG4gICAgaWYgKHRoaXMuX2lzUGxheWluZykge1xuICAgICAgdGhpcy5zdG9wKCk7XG4gICAgfVxuXG4gICAgaWYgKGRpcmVjdGlvbiA9PT0gRGlyZWN0aW9uLk5FWFQpIHtcbiAgICAgIG5leHRJdGVtLm5leHQgPSB0cnVlO1xuXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgdGhpcy5fcmVmbG93KG5leHRFbCk7XG4gICAgICAgIGN1cnJlbnRJdGVtLnN0YXJ0ID0gdHJ1ZTtcbiAgICAgICAgbmV4dEl0ZW0uc3RhcnQgPSB0cnVlO1xuICAgICAgfSwgMCk7XG5cbiAgICAgIGNvbnN0IHRyYW5zaXRpb25EdXJhdGlvbiA9IDYwMDtcblxuICAgICAgZnJvbUV2ZW50KGN1cnJlbnRFbCwgJ3RyYW5zaXRpb25lbmQnKVxuICAgICAgICAucGlwZSh0YWtlKDEpKVxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGRlcHJlY2F0aW9uXG4gICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgIG5leHRJdGVtLm5leHQgPSBmYWxzZTtcbiAgICAgICAgICBuZXh0SXRlbS5zdGFydCA9IGZhbHNlO1xuICAgICAgICAgIG5leHRJdGVtLmFjdGl2ZSA9IHRydWU7XG5cbiAgICAgICAgICBjdXJyZW50SXRlbS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICBjdXJyZW50SXRlbS5zdGFydCA9IGZhbHNlO1xuICAgICAgICAgIGN1cnJlbnRJdGVtLm5leHQgPSBmYWxzZTtcblxuICAgICAgICAgIHRoaXMuc2xpZGVDaGFuZ2UuZW1pdCgpO1xuICAgICAgICAgIHRoaXMuX2lzU2xpZGluZyA9IGZhbHNlO1xuICAgICAgICB9KTtcblxuICAgICAgdGhpcy5fZW11bGF0ZVRyYW5zaXRpb25FbmQoY3VycmVudEVsLCB0cmFuc2l0aW9uRHVyYXRpb24pO1xuICAgIH0gZWxzZSBpZiAoZGlyZWN0aW9uID09PSBEaXJlY3Rpb24uUFJFVikge1xuICAgICAgbmV4dEl0ZW0ucHJldiA9IHRydWU7XG5cbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLl9yZWZsb3cobmV4dEVsKTtcbiAgICAgICAgY3VycmVudEl0ZW0uZW5kID0gdHJ1ZTtcbiAgICAgICAgbmV4dEl0ZW0uZW5kID0gdHJ1ZTtcbiAgICAgIH0sIDApO1xuXG4gICAgICBjb25zdCB0cmFuc2l0aW9uRHVyYXRpb24gPSA2MDA7XG5cbiAgICAgIGZyb21FdmVudChjdXJyZW50RWwsICd0cmFuc2l0aW9uZW5kJylcbiAgICAgICAgLnBpcGUodGFrZSgxKSlcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkZXByZWNhdGlvblxuICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICBuZXh0SXRlbS5wcmV2ID0gZmFsc2U7XG4gICAgICAgICAgbmV4dEl0ZW0uZW5kID0gZmFsc2U7XG4gICAgICAgICAgbmV4dEl0ZW0uYWN0aXZlID0gdHJ1ZTtcblxuICAgICAgICAgIGN1cnJlbnRJdGVtLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgIGN1cnJlbnRJdGVtLmVuZCA9IGZhbHNlO1xuICAgICAgICAgIGN1cnJlbnRJdGVtLnByZXYgPSBmYWxzZTtcblxuICAgICAgICAgIHRoaXMuc2xpZGVDaGFuZ2UuZW1pdCgpO1xuICAgICAgICAgIHRoaXMuX2lzU2xpZGluZyA9IGZhbHNlO1xuICAgICAgICB9KTtcblxuICAgICAgdGhpcy5fZW11bGF0ZVRyYW5zaXRpb25FbmQoY3VycmVudEVsLCB0cmFuc2l0aW9uRHVyYXRpb24pO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5faXNQbGF5aW5nICYmIHRoaXMuaW50ZXJ2YWwgPiAwKSB7XG4gICAgICB0aGlzLnBsYXkoKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIF9yZWZsb3coZWxlbWVudDogSFRNTEVsZW1lbnQpOiBudW1iZXIge1xuICAgIHJldHVybiBlbGVtZW50Lm9mZnNldEhlaWdodDtcbiAgfVxuXG4gIHByaXZhdGUgX2VtdWxhdGVUcmFuc2l0aW9uRW5kKGVsZW1lbnQ6IEhUTUxFbGVtZW50LCBkdXJhdGlvbjogbnVtYmVyKTogdm9pZCB7XG4gICAgbGV0IGV2ZW50RW1pdHRlZCA9IGZhbHNlO1xuICAgIGNvbnN0IGR1cmF0aW9uUGFkZGluZyA9IDU7XG4gICAgY29uc3QgZW11bGF0ZWREdXJhdGlvbiA9IGR1cmF0aW9uICsgZHVyYXRpb25QYWRkaW5nO1xuXG4gICAgZnJvbUV2ZW50KGVsZW1lbnQsICd0cmFuc2l0aW9uZW5kJylcbiAgICAgIC5waXBlKHRha2UoMSkpXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGRlcHJlY2F0aW9uXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgZXZlbnRFbWl0dGVkID0gdHJ1ZTtcbiAgICAgIH0pO1xuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBpZiAoIWV2ZW50RW1pdHRlZCkge1xuICAgICAgICBlbGVtZW50LmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCd0cmFuc2l0aW9uZW5kJykpO1xuICAgICAgfVxuICAgIH0sIGVtdWxhdGVkRHVyYXRpb24pO1xuICB9XG5cbiAgcHJpdmF0ZSBfZ2V0TmV3U2xpZGVJbmRleChkaXJlY3Rpb246IERpcmVjdGlvbik6IG51bWJlciB7XG4gICAgbGV0IG5ld1NsaWRlSW5kZXg6IG51bWJlcjtcblxuICAgIGlmIChkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5ORVhUKSB7XG4gICAgICBuZXdTbGlkZUluZGV4ID0gdGhpcy5fZ2V0TmV4dFNsaWRlSW5kZXgoKTtcbiAgICB9XG5cbiAgICBpZiAoZGlyZWN0aW9uID09PSBEaXJlY3Rpb24uUFJFVikge1xuICAgICAgbmV3U2xpZGVJbmRleCA9IHRoaXMuX2dldFByZXZTbGlkZUluZGV4KCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ld1NsaWRlSW5kZXg7XG4gIH1cblxuICBwcml2YXRlIF9nZXROZXh0U2xpZGVJbmRleCgpOiBudW1iZXIge1xuICAgIGNvbnN0IGlzTGFzdCA9IHRoaXMuX2FjdGl2ZVNsaWRlID09PSB0aGlzLml0ZW1zLmxlbmd0aCAtIDE7XG5cbiAgICBpZiAoIWlzTGFzdCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2FjdGl2ZVNsaWRlICsgMTtcbiAgICB9IGVsc2UgaWYgKHRoaXMud3JhcCAmJiBpc0xhc3QpIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGhpcy5fYWN0aXZlU2xpZGU7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBfZ2V0UHJldlNsaWRlSW5kZXgoKTogbnVtYmVyIHtcbiAgICBjb25zdCBpc0ZpcnN0ID0gdGhpcy5fYWN0aXZlU2xpZGUgPT09IDA7XG5cbiAgICBpZiAoIWlzRmlyc3QpIHtcbiAgICAgIHJldHVybiB0aGlzLl9hY3RpdmVTbGlkZSAtIDE7XG4gICAgfSBlbHNlIGlmICh0aGlzLndyYXAgJiYgaXNGaXJzdCkge1xuICAgICAgcmV0dXJuIHRoaXMuaXRlbXMubGVuZ3RoIC0gMTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMuX2FjdGl2ZVNsaWRlO1xuICAgIH1cbiAgfVxufVxuIl19