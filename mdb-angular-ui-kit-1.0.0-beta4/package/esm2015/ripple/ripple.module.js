import { NgModule } from '@angular/core';
import { MdbRippleDirective } from './ripple.directive';
export class MdbRippleModule {
}
MdbRippleModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbRippleDirective],
                imports: [],
                exports: [MdbRippleDirective],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmlwcGxlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIuLi8uLi8uLi9wcm9qZWN0cy9tZGItYW5ndWxhci11aS1raXQvIiwic291cmNlcyI6WyJyaXBwbGUvcmlwcGxlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBT3hELE1BQU0sT0FBTyxlQUFlOzs7WUFMM0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO2dCQUNsQyxPQUFPLEVBQUUsRUFBRTtnQkFDWCxPQUFPLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQzthQUM5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNZGJSaXBwbGVEaXJlY3RpdmUgfSBmcm9tICcuL3JpcHBsZS5kaXJlY3RpdmUnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtNZGJSaXBwbGVEaXJlY3RpdmVdLFxuICBpbXBvcnRzOiBbXSxcbiAgZXhwb3J0czogW01kYlJpcHBsZURpcmVjdGl2ZV0sXG59KVxuZXhwb3J0IGNsYXNzIE1kYlJpcHBsZU1vZHVsZSB7fVxuIl19