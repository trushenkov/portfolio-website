import { Component, ChangeDetectionStrategy, HostBinding, ViewChild, ContentChild, ElementRef, Renderer2, } from '@angular/core';
import { MdbAbstractFormControl } from './form-control';
import { MdbInputDirective } from './input.directive';
import { MdbLabelDirective } from './label.directive';
import { ContentObserver } from '@angular/cdk/observers';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class MdbFormControlComponent {
    constructor(_renderer, _contentObserver) {
        this._renderer = _renderer;
        this._contentObserver = _contentObserver;
        this.outline = true;
        this._destroy$ = new Subject();
        this._notchLeadingLength = 9;
        this._labelMarginLeft = 0;
        this._labelGapPadding = 8;
        this._labelScale = 0.8;
    }
    ngAfterViewInit() { }
    ngAfterContentInit() {
        if (this._label) {
            this._updateBorderGap();
        }
        else {
            this._renderer.addClass(this._input.nativeElement, 'placeholder-active');
        }
        this._updateLabelActiveState();
        if (this._label) {
            this._contentObserver
                .observe(this._label.nativeElement)
                .pipe(takeUntil(this._destroy$))
                .subscribe(() => {
                this._updateBorderGap();
            });
        }
        this._formControl.stateChanges.pipe(takeUntil(this._destroy$)).subscribe(() => {
            this._updateLabelActiveState();
            if (this._label) {
                this._updateBorderGap();
            }
        });
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.unsubscribe();
    }
    _getLabelWidth() {
        return this._label.nativeElement.clientWidth * this._labelScale + this._labelGapPadding;
    }
    _updateBorderGap() {
        const notchLeadingWidth = `${this._labelMarginLeft + this._notchLeadingLength}px`;
        const notchMiddleWidth = `${this._getLabelWidth()}px`;
        this._renderer.setStyle(this._notchLeading.nativeElement, 'width', notchLeadingWidth);
        this._renderer.setStyle(this._notchMiddle.nativeElement, 'width', notchMiddleWidth);
        this._renderer.setStyle(this._label.nativeElement, 'margin-left', `${this._labelMarginLeft}px`);
    }
    _updateLabelActiveState() {
        if (this._isLabelActive()) {
            this._renderer.addClass(this._input.nativeElement, 'active');
        }
        else {
            this._renderer.removeClass(this._input.nativeElement, 'active');
        }
    }
    _isLabelActive() {
        return this._formControl && this._formControl.labelActive;
    }
}
MdbFormControlComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-form-control',
                template: "<ng-content></ng-content>\n<div class=\"form-notch\">\n  <div #notchLeading class=\"form-notch-leading\"></div>\n  <div #notchMiddle class=\"form-notch-middle\"></div>\n  <div class=\"form-notch-trailing\"></div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush
            },] }
];
MdbFormControlComponent.ctorParameters = () => [
    { type: Renderer2 },
    { type: ContentObserver }
];
MdbFormControlComponent.propDecorators = {
    _notchLeading: [{ type: ViewChild, args: ['notchLeading', { static: true },] }],
    _notchMiddle: [{ type: ViewChild, args: ['notchMiddle', { static: true },] }],
    _input: [{ type: ContentChild, args: [MdbInputDirective, { static: true, read: ElementRef },] }],
    _formControl: [{ type: ContentChild, args: [MdbAbstractFormControl, { static: true },] }],
    _label: [{ type: ContentChild, args: [MdbLabelDirective, { static: true, read: ElementRef },] }],
    outline: [{ type: HostBinding, args: ['class.form-outline',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1jb250cm9sLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIuLi8uLi8uLi9wcm9qZWN0cy9tZGItYW5ndWxhci11aS1raXQvIiwic291cmNlcyI6WyJmb3Jtcy9mb3JtLWNvbnRyb2wuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsdUJBQXVCLEVBQ3ZCLFdBQVcsRUFDWCxTQUFTLEVBQ1QsWUFBWSxFQUNaLFVBQVUsRUFHVixTQUFTLEdBRVYsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDdEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDdEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBUTNDLE1BQU0sT0FBTyx1QkFBdUI7SUFTbEMsWUFBb0IsU0FBb0IsRUFBVSxnQkFBaUM7UUFBL0QsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFGaEQsWUFBTyxHQUFHLElBQUksQ0FBQztRQUl6QyxjQUFTLEdBQWtCLElBQUksT0FBTyxFQUFRLENBQUM7UUFFaEQsd0JBQW1CLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLHFCQUFnQixHQUFHLENBQUMsQ0FBQztRQUNyQixxQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDckIsZ0JBQVcsR0FBRyxHQUFHLENBQUM7SUFQNEQsQ0FBQztJQVN2RixlQUFlLEtBQVUsQ0FBQztJQUUxQixrQkFBa0I7UUFDaEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDekI7YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLG9CQUFvQixDQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUUvQixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsZ0JBQWdCO2lCQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUM7aUJBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUMvQixTQUFTLENBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDNUUsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7WUFDL0IsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNmLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2FBQ3pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRU8sY0FBYztRQUNwQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUMxRixDQUFDO0lBRU8sZ0JBQWdCO1FBQ3RCLE1BQU0saUJBQWlCLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixJQUFJLENBQUM7UUFDbEYsTUFBTSxnQkFBZ0IsR0FBRyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDO1FBRXRELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3RGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLGFBQWEsRUFBRSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLENBQUM7SUFDbEcsQ0FBQztJQUVPLHVCQUF1QjtRQUM3QixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRTtZQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUM5RDthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDakU7SUFDSCxDQUFDO0lBRU8sY0FBYztRQUNwQixPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7SUFDNUQsQ0FBQzs7O1lBL0VGLFNBQVMsU0FBQztnQkFDVCwrQ0FBK0M7Z0JBQy9DLFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLDBPQUE0QztnQkFDNUMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07YUFDaEQ7OztZQWZDLFNBQVM7WUFNRixlQUFlOzs7NEJBV3JCLFNBQVMsU0FBQyxjQUFjLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFOzJCQUMxQyxTQUFTLFNBQUMsYUFBYSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtxQkFDekMsWUFBWSxTQUFDLGlCQUFpQixFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFOzJCQUNsRSxZQUFZLFNBQUMsc0JBQXNCLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO3FCQUNyRCxZQUFZLFNBQUMsaUJBQWlCLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUU7c0JBRWxFLFdBQVcsU0FBQyxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxuICBIb3N0QmluZGluZyxcbiAgVmlld0NoaWxkLFxuICBDb250ZW50Q2hpbGQsXG4gIEVsZW1lbnRSZWYsXG4gIEFmdGVyVmlld0luaXQsXG4gIEFmdGVyQ29udGVudEluaXQsXG4gIFJlbmRlcmVyMixcbiAgT25EZXN0cm95LFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1kYkFic3RyYWN0Rm9ybUNvbnRyb2wgfSBmcm9tICcuL2Zvcm0tY29udHJvbCc7XG5pbXBvcnQgeyBNZGJJbnB1dERpcmVjdGl2ZSB9IGZyb20gJy4vaW5wdXQuZGlyZWN0aXZlJztcbmltcG9ydCB7IE1kYkxhYmVsRGlyZWN0aXZlIH0gZnJvbSAnLi9sYWJlbC5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgQ29udGVudE9ic2VydmVyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL29ic2VydmVycyc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBDb21wb25lbnQoe1xuICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGNvbXBvbmVudC1zZWxlY3RvclxuICBzZWxlY3RvcjogJ21kYi1mb3JtLWNvbnRyb2wnLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1jb250cm9sLmNvbXBvbmVudC5odG1sJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG59KVxuZXhwb3J0IGNsYXNzIE1kYkZvcm1Db250cm9sQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCwgQWZ0ZXJDb250ZW50SW5pdCwgT25EZXN0cm95IHtcbiAgQFZpZXdDaGlsZCgnbm90Y2hMZWFkaW5nJywgeyBzdGF0aWM6IHRydWUgfSkgX25vdGNoTGVhZGluZzogRWxlbWVudFJlZjtcbiAgQFZpZXdDaGlsZCgnbm90Y2hNaWRkbGUnLCB7IHN0YXRpYzogdHJ1ZSB9KSBfbm90Y2hNaWRkbGU6IEVsZW1lbnRSZWY7XG4gIEBDb250ZW50Q2hpbGQoTWRiSW5wdXREaXJlY3RpdmUsIHsgc3RhdGljOiB0cnVlLCByZWFkOiBFbGVtZW50UmVmIH0pIF9pbnB1dDogRWxlbWVudFJlZjtcbiAgQENvbnRlbnRDaGlsZChNZGJBYnN0cmFjdEZvcm1Db250cm9sLCB7IHN0YXRpYzogdHJ1ZSB9KSBfZm9ybUNvbnRyb2w6IE1kYkFic3RyYWN0Rm9ybUNvbnRyb2w8YW55PjtcbiAgQENvbnRlbnRDaGlsZChNZGJMYWJlbERpcmVjdGl2ZSwgeyBzdGF0aWM6IHRydWUsIHJlYWQ6IEVsZW1lbnRSZWYgfSkgX2xhYmVsOiBFbGVtZW50UmVmO1xuXG4gIEBIb3N0QmluZGluZygnY2xhc3MuZm9ybS1vdXRsaW5lJykgb3V0bGluZSA9IHRydWU7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMiwgcHJpdmF0ZSBfY29udGVudE9ic2VydmVyOiBDb250ZW50T2JzZXJ2ZXIpIHt9XG5cbiAgcmVhZG9ubHkgX2Rlc3Ryb3kkOiBTdWJqZWN0PHZvaWQ+ID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcblxuICBwcml2YXRlIF9ub3RjaExlYWRpbmdMZW5ndGggPSA5O1xuICBwcml2YXRlIF9sYWJlbE1hcmdpbkxlZnQgPSAwO1xuICBwcml2YXRlIF9sYWJlbEdhcFBhZGRpbmcgPSA4O1xuICBwcml2YXRlIF9sYWJlbFNjYWxlID0gMC44O1xuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHt9XG5cbiAgbmdBZnRlckNvbnRlbnRJbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLl9sYWJlbCkge1xuICAgICAgdGhpcy5fdXBkYXRlQm9yZGVyR2FwKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2lucHV0Lm5hdGl2ZUVsZW1lbnQsICdwbGFjZWhvbGRlci1hY3RpdmUnKTtcbiAgICB9XG4gICAgdGhpcy5fdXBkYXRlTGFiZWxBY3RpdmVTdGF0ZSgpO1xuXG4gICAgaWYgKHRoaXMuX2xhYmVsKSB7XG4gICAgICB0aGlzLl9jb250ZW50T2JzZXJ2ZXJcbiAgICAgICAgLm9ic2VydmUodGhpcy5fbGFiZWwubmF0aXZlRWxlbWVudClcbiAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuX2Rlc3Ryb3kkKSlcbiAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5fdXBkYXRlQm9yZGVyR2FwKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHRoaXMuX2Zvcm1Db250cm9sLnN0YXRlQ2hhbmdlcy5waXBlKHRha2VVbnRpbCh0aGlzLl9kZXN0cm95JCkpLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICB0aGlzLl91cGRhdGVMYWJlbEFjdGl2ZVN0YXRlKCk7XG4gICAgICBpZiAodGhpcy5fbGFiZWwpIHtcbiAgICAgICAgdGhpcy5fdXBkYXRlQm9yZGVyR2FwKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLl9kZXN0cm95JC5uZXh0KCk7XG4gICAgdGhpcy5fZGVzdHJveSQudW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIHByaXZhdGUgX2dldExhYmVsV2lkdGgoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5fbGFiZWwubmF0aXZlRWxlbWVudC5jbGllbnRXaWR0aCAqIHRoaXMuX2xhYmVsU2NhbGUgKyB0aGlzLl9sYWJlbEdhcFBhZGRpbmc7XG4gIH1cblxuICBwcml2YXRlIF91cGRhdGVCb3JkZXJHYXAoKTogdm9pZCB7XG4gICAgY29uc3Qgbm90Y2hMZWFkaW5nV2lkdGggPSBgJHt0aGlzLl9sYWJlbE1hcmdpbkxlZnQgKyB0aGlzLl9ub3RjaExlYWRpbmdMZW5ndGh9cHhgO1xuICAgIGNvbnN0IG5vdGNoTWlkZGxlV2lkdGggPSBgJHt0aGlzLl9nZXRMYWJlbFdpZHRoKCl9cHhgO1xuXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5fbm90Y2hMZWFkaW5nLm5hdGl2ZUVsZW1lbnQsICd3aWR0aCcsIG5vdGNoTGVhZGluZ1dpZHRoKTtcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLl9ub3RjaE1pZGRsZS5uYXRpdmVFbGVtZW50LCAnd2lkdGgnLCBub3RjaE1pZGRsZVdpZHRoKTtcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLl9sYWJlbC5uYXRpdmVFbGVtZW50LCAnbWFyZ2luLWxlZnQnLCBgJHt0aGlzLl9sYWJlbE1hcmdpbkxlZnR9cHhgKTtcbiAgfVxuXG4gIHByaXZhdGUgX3VwZGF0ZUxhYmVsQWN0aXZlU3RhdGUoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuX2lzTGFiZWxBY3RpdmUoKSkge1xuICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5faW5wdXQubmF0aXZlRWxlbWVudCwgJ2FjdGl2ZScpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9pbnB1dC5uYXRpdmVFbGVtZW50LCAnYWN0aXZlJyk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBfaXNMYWJlbEFjdGl2ZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5fZm9ybUNvbnRyb2wgJiYgdGhpcy5fZm9ybUNvbnRyb2wubGFiZWxBY3RpdmU7XG4gIH1cbn1cbiJdfQ==