(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('rxjs/operators'), require('@angular/common'), require('@angular/forms'), require('@angular/cdk/overlay'), require('@angular/cdk/portal'), require('@angular/animations'), require('@angular/cdk/observers'), require('@angular/cdk/a11y'), require('@angular/cdk/layout')) :
    typeof define === 'function' && define.amd ? define('mdb-angular-ui-kit', ['exports', '@angular/core', 'rxjs', 'rxjs/operators', '@angular/common', '@angular/forms', '@angular/cdk/overlay', '@angular/cdk/portal', '@angular/animations', '@angular/cdk/observers', '@angular/cdk/a11y', '@angular/cdk/layout'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['mdb-angular-ui-kit'] = {}, global.ng.core, global.rxjs, global.rxjs.operators, global.ng.common, global.ng.forms, global.ng.cdk.overlay, global.ng.cdk.portal, global.ng.animations, global.ng.cdk.observers, global.ng.cdk.a11y, global.ng.cdk.layout));
}(this, (function (exports, core, rxjs, operators, common, forms, overlay, portal, animations, observers, a11y, layout) { 'use strict';

    var TRANSITION_TIME = 350;
    // tslint:disable-next-line: component-class-suffix
    var MdbCollapseDirective = /** @class */ (function () {
        function MdbCollapseDirective(_elementRef, _renderer) {
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this.collapseClass = true;
            this.collapseShow = new core.EventEmitter();
            this.collapseShown = new core.EventEmitter();
            this.collapseHide = new core.EventEmitter();
            this.collapseHidden = new core.EventEmitter();
            this._collapsed = true;
            this._isTransitioning = false;
        }
        Object.defineProperty(MdbCollapseDirective.prototype, "collapsed", {
            get: function () {
                return this._collapsed;
            },
            set: function (collapsed) {
                if (collapsed !== this._collapsed) {
                    collapsed ? this.hide() : this.show();
                    this._collapsed = collapsed;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCollapseDirective.prototype, "host", {
            get: function () {
                return this._elementRef.nativeElement;
            },
            enumerable: false,
            configurable: true
        });
        MdbCollapseDirective.prototype.show = function () {
            var _this = this;
            if (this._isTransitioning || !this.collapsed) {
                return;
            }
            this.collapseShow.emit(this);
            this._renderer.removeClass(this.host, 'collapse');
            this._renderer.addClass(this.host, 'collapsing');
            this._renderer.setStyle(this.host, 'height', '0px');
            this._isTransitioning = true;
            var scrollHeight = this.host.scrollHeight;
            rxjs.fromEvent(this.host, 'transitionend')
                .pipe(operators.take(1))
                .subscribe(function () {
                _this._isTransitioning = false;
                _this.collapsed = false;
                _this._renderer.removeClass(_this.host, 'collapsing');
                _this._renderer.addClass(_this.host, 'collapse');
                _this._renderer.addClass(_this.host, 'show');
                _this._renderer.removeStyle(_this.host, 'height');
                _this.collapseShown.emit(_this);
            });
            this._emulateTransitionEnd(this.host, TRANSITION_TIME);
            this._renderer.setStyle(this.host, 'height', scrollHeight + "px");
        };
        MdbCollapseDirective.prototype.hide = function () {
            var _this = this;
            if (this._isTransitioning || this.collapsed) {
                return;
            }
            this.collapseHide.emit(this);
            var hostHeight = this.host.getBoundingClientRect().height;
            this._renderer.setStyle(this.host, 'height', hostHeight + "px");
            this._reflow(this.host);
            this._renderer.addClass(this.host, 'collapsing');
            this._renderer.removeClass(this.host, 'collapse');
            this._renderer.removeClass(this.host, 'show');
            this._isTransitioning = true;
            rxjs.fromEvent(this.host, 'transitionend')
                .pipe(operators.take(1))
                .subscribe(function () {
                _this._renderer.removeClass(_this.host, 'collapsing');
                _this._renderer.addClass(_this.host, 'collapse');
                _this._isTransitioning = false;
                _this.collapsed = true;
                _this.collapseHidden.emit(_this);
            });
            this._renderer.removeStyle(this.host, 'height');
            this._emulateTransitionEnd(this.host, TRANSITION_TIME);
        };
        MdbCollapseDirective.prototype.toggle = function () {
            this.collapsed = !this.collapsed;
            this.collapsed ? this.hide() : this.show();
        };
        MdbCollapseDirective.prototype._reflow = function (element) {
            return element.offsetHeight;
        };
        MdbCollapseDirective.prototype._emulateTransitionEnd = function (element, duration) {
            var eventEmitted = false;
            var durationPadding = 5;
            var emulatedDuration = duration + durationPadding;
            rxjs.fromEvent(element, 'transitionend')
                .pipe(operators.take(1))
                .subscribe(function () {
                eventEmitted = true;
            });
            setTimeout(function () {
                if (!eventEmitted) {
                    element.dispatchEvent(new Event('transitionend'));
                }
            }, emulatedDuration);
        };
        return MdbCollapseDirective;
    }());
    MdbCollapseDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbCollapse]',
                    exportAs: 'mdbCollapse',
                },] }
    ];
    MdbCollapseDirective.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 }
    ]; };
    MdbCollapseDirective.propDecorators = {
        collapseClass: [{ type: core.HostBinding, args: ['class.collapse',] }],
        collapseShow: [{ type: core.Output }],
        collapseShown: [{ type: core.Output }],
        collapseHide: [{ type: core.Output }],
        collapseHidden: [{ type: core.Output }],
        collapsed: [{ type: core.Input }]
    };

    var MdbCollapseModule = /** @class */ (function () {
        function MdbCollapseModule() {
        }
        return MdbCollapseModule;
    }());
    MdbCollapseModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [MdbCollapseDirective],
                    exports: [MdbCollapseDirective],
                },] }
    ];

    var MDB_CHECKBOX_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        // tslint:disable-next-line: no-use-before-declare
        useExisting: core.forwardRef(function () { return MdbCheckboxDirective; }),
        multi: true,
    };
    var MdbCheckboxChange = /** @class */ (function () {
        function MdbCheckboxChange() {
        }
        return MdbCheckboxChange;
    }());
    var MdbCheckboxDirective = /** @class */ (function () {
        function MdbCheckboxDirective() {
            this._checked = false;
            this._value = null;
            this._disabled = false;
            this.checkboxChange = new core.EventEmitter();
            // Control Value Accessor Methods
            this.onChange = function (_) { };
            this.onTouched = function () { };
        }
        Object.defineProperty(MdbCheckboxDirective.prototype, "checked", {
            get: function () {
                return this._checked;
            },
            set: function (value) {
                this._checked = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCheckboxDirective.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (value) {
                this._value = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCheckboxDirective.prototype, "disabled", {
            get: function () {
                return this._disabled;
            },
            set: function (value) {
                this._disabled = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCheckboxDirective.prototype, "isDisabled", {
            get: function () {
                return this._disabled;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCheckboxDirective.prototype, "isChecked", {
            get: function () {
                return this._checked;
            },
            enumerable: false,
            configurable: true
        });
        MdbCheckboxDirective.prototype.onCheckboxClick = function () {
            this.toggle();
        };
        MdbCheckboxDirective.prototype.onBlur = function () {
            this.onTouched();
        };
        Object.defineProperty(MdbCheckboxDirective.prototype, "changeEvent", {
            get: function () {
                var newChangeEvent = new MdbCheckboxChange();
                newChangeEvent.element = this;
                newChangeEvent.checked = this.checked;
                return newChangeEvent;
            },
            enumerable: false,
            configurable: true
        });
        MdbCheckboxDirective.prototype.toggle = function () {
            if (this.disabled) {
                return;
            }
            this._checked = !this._checked;
            this.onChange(this.checked);
            this.onCheckboxChange();
        };
        MdbCheckboxDirective.prototype.onCheckboxChange = function () {
            this.checkboxChange.emit(this.changeEvent);
        };
        MdbCheckboxDirective.prototype.writeValue = function (value) {
            this.value = value;
            this.checked = !!value;
        };
        MdbCheckboxDirective.prototype.registerOnChange = function (fn) {
            this.onChange = fn;
        };
        MdbCheckboxDirective.prototype.registerOnTouched = function (fn) {
            this.onTouched = fn;
        };
        MdbCheckboxDirective.prototype.setDisabledState = function (isDisabled) {
            this.disabled = isDisabled;
        };
        return MdbCheckboxDirective;
    }());
    MdbCheckboxDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbCheckbox]',
                    providers: [MDB_CHECKBOX_VALUE_ACCESSOR],
                },] }
    ];
    MdbCheckboxDirective.ctorParameters = function () { return []; };
    MdbCheckboxDirective.propDecorators = {
        checked: [{ type: core.Input, args: ['checked',] }],
        value: [{ type: core.Input, args: ['value',] }],
        disabled: [{ type: core.Input, args: ['disabled',] }],
        checkboxChange: [{ type: core.Output }],
        isDisabled: [{ type: core.HostBinding, args: ['disabled',] }],
        isChecked: [{ type: core.HostBinding, args: ['checked',] }],
        onCheckboxClick: [{ type: core.HostListener, args: ['click',] }],
        onBlur: [{ type: core.HostListener, args: ['blur',] }]
    };

    var MdbCheckboxModule = /** @class */ (function () {
        function MdbCheckboxModule() {
        }
        return MdbCheckboxModule;
    }());
    MdbCheckboxModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [MdbCheckboxDirective],
                    exports: [MdbCheckboxDirective],
                    imports: [common.CommonModule, forms.FormsModule],
                },] }
    ];

    var MdbRadioDirective = /** @class */ (function () {
        function MdbRadioDirective() {
            this._checked = false;
            this._value = null;
            this._disabled = false;
        }
        Object.defineProperty(MdbRadioDirective.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (value) {
                this._name = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioDirective.prototype, "checked", {
            get: function () {
                return this._checked;
            },
            set: function (value) {
                this._checked = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioDirective.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (value) {
                this._value = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioDirective.prototype, "disabled", {
            get: function () {
                return this._disabled;
            },
            set: function (value) {
                this._disabled = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioDirective.prototype, "isDisabled", {
            get: function () {
                return this._disabled;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioDirective.prototype, "isChecked", {
            get: function () {
                return this._checked;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioDirective.prototype, "nameAttr", {
            get: function () {
                return this.name;
            },
            enumerable: false,
            configurable: true
        });
        MdbRadioDirective.prototype._updateName = function (value) {
            this._name = value;
        };
        MdbRadioDirective.prototype._updateChecked = function (value) {
            this._checked = value;
        };
        MdbRadioDirective.prototype._updateDisabledState = function (value) {
            this._disabled = value;
        };
        return MdbRadioDirective;
    }());
    MdbRadioDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbRadio]',
                },] }
    ];
    MdbRadioDirective.ctorParameters = function () { return []; };
    MdbRadioDirective.propDecorators = {
        name: [{ type: core.Input }],
        checked: [{ type: core.Input, args: ['checked',] }],
        value: [{ type: core.Input, args: ['value',] }],
        disabled: [{ type: core.Input, args: ['disabled',] }],
        isDisabled: [{ type: core.HostBinding, args: ['disabled',] }],
        isChecked: [{ type: core.HostBinding, args: ['checked',] }],
        nameAttr: [{ type: core.HostBinding, args: ['attr.name',] }]
    };

    var MDB_RADIO_GROUP_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        // tslint:disable-next-line: no-use-before-declare
        useExisting: core.forwardRef(function () { return MdbRadioGroupDirective; }),
        multi: true,
    };
    var MdbRadioGroupDirective = /** @class */ (function () {
        function MdbRadioGroupDirective() {
            this._disabled = false;
            this._destroy$ = new rxjs.Subject();
            this.onChange = function (_) { };
            this.onTouched = function () { };
        }
        Object.defineProperty(MdbRadioGroupDirective.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (value) {
                this._value = value;
                if (this.radios) {
                    this._updateChecked();
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioGroupDirective.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (name) {
                this._name = name;
                if (this.radios) {
                    this._updateNames();
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbRadioGroupDirective.prototype, "disabled", {
            get: function () {
                return this._disabled;
            },
            set: function (disabled) {
                this._disabled = disabled;
                if (this.radios) {
                    this._updateDisabled();
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbRadioGroupDirective.prototype.ngAfterContentInit = function () {
            var _this = this;
            this._updateNames();
            this._updateDisabled();
            this.radios.changes
                .pipe(operators.startWith(this.radios), operators.switchMap(function (radios) { return rxjs.from(Promise.resolve(radios)); }), operators.takeUntil(this._destroy$))
                .subscribe(function () { return _this._updateRadiosState(); });
        };
        MdbRadioGroupDirective.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
        };
        MdbRadioGroupDirective.prototype._updateRadiosState = function () {
            this._updateNames();
            this._updateChecked();
            this._updateDisabled();
        };
        MdbRadioGroupDirective.prototype._updateNames = function () {
            var _this = this;
            this.radios.forEach(function (radio) { return radio._updateName(_this.name); });
        };
        MdbRadioGroupDirective.prototype._updateChecked = function () {
            var _this = this;
            this.radios.forEach(function (radio) {
                var isChecked = radio.value === _this._value;
                radio._updateChecked(isChecked);
            });
        };
        MdbRadioGroupDirective.prototype._updateDisabled = function () {
            var _this = this;
            this.radios.forEach(function (radio) { return radio._updateDisabledState(_this._disabled); });
        };
        // Control value accessor methods
        MdbRadioGroupDirective.prototype.registerOnChange = function (fn) {
            this.onChange = fn;
        };
        MdbRadioGroupDirective.prototype.registerOnTouched = function (fn) {
            this.onTouched = fn;
        };
        MdbRadioGroupDirective.prototype.setDisabledState = function (isDisabled) {
            this._disabled = isDisabled;
            this._updateDisabled();
        };
        MdbRadioGroupDirective.prototype.writeValue = function (value) {
            this.value = value;
        };
        return MdbRadioGroupDirective;
    }());
    MdbRadioGroupDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbRadioGroup]',
                    providers: [MDB_RADIO_GROUP_VALUE_ACCESSOR],
                },] }
    ];
    MdbRadioGroupDirective.propDecorators = {
        radios: [{ type: core.ContentChildren, args: [MdbRadioDirective, { descendants: true },] }],
        value: [{ type: core.Input }],
        name: [{ type: core.Input }],
        disabled: [{ type: core.Input }]
    };

    var MdbRadioModule = /** @class */ (function () {
        function MdbRadioModule() {
        }
        return MdbRadioModule;
    }());
    MdbRadioModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [MdbRadioDirective, MdbRadioGroupDirective],
                    exports: [MdbRadioDirective, MdbRadioGroupDirective],
                    imports: [common.CommonModule, forms.FormsModule],
                },] }
    ];

    var MdbTooltipComponent = /** @class */ (function () {
        function MdbTooltipComponent(_cdRef) {
            this._cdRef = _cdRef;
            this._hidden = new rxjs.Subject();
            this.animationState = 'hidden';
        }
        MdbTooltipComponent.prototype.ngOnInit = function () { };
        MdbTooltipComponent.prototype.markForCheck = function () {
            this._cdRef.markForCheck();
        };
        MdbTooltipComponent.prototype.onAnimationEnd = function (event) {
            if (event.toState === 'hidden') {
                this._hidden.next();
            }
        };
        return MdbTooltipComponent;
    }());
    MdbTooltipComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-tooltip',
                    template: "<div\n  *ngIf=\"html\"\n  [@fade]=\"animationState\"\n  (@fade.done)=\"onAnimationEnd($event)\"\n  [@.disabled]=\"!animation\"\n  [innerHTML]=\"title\"\n  class=\"tooltip-inner\"\n></div>\n<div\n  *ngIf=\"!html\"\n  [@fade]=\"animationState\"\n  (@fade.done)=\"onAnimationEnd($event)\"\n  [@.disabled]=\"!animation\"\n  class=\"tooltip-inner\"\n>\n  {{ title }}\n</div>\n",
                    changeDetection: core.ChangeDetectionStrategy.OnPush,
                    animations: [
                        animations.trigger('fade', [
                            animations.state('visible', animations.style({ opacity: 1 })),
                            animations.state('hidden', animations.style({ opacity: 0 })),
                            animations.transition('visible => hidden', animations.animate('150ms linear')),
                            animations.transition(':enter', animations.animate('150ms linear')),
                        ]),
                    ]
                },] }
    ];
    MdbTooltipComponent.ctorParameters = function () { return [
        { type: core.ChangeDetectorRef }
    ]; };
    MdbTooltipComponent.propDecorators = {
        title: [{ type: core.Input }],
        html: [{ type: core.Input }],
        animation: [{ type: core.Input }]
    };

    // tslint:disable-next-line:component-class-suffix
    var MdbTooltipDirective = /** @class */ (function () {
        function MdbTooltipDirective(_overlay, _overlayPositionBuilder, _elementRef, _renderer) {
            this._overlay = _overlay;
            this._overlayPositionBuilder = _overlayPositionBuilder;
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this.mdbTooltip = '';
            this.tooltipDisabled = false;
            this.placement = 'top';
            this.html = false;
            this.animation = true;
            this.trigger = 'hover focus';
            this.delayShow = 0;
            this.delayHide = 0;
            this.offset = 4;
            this.tooltipShow = new core.EventEmitter();
            this.tooltipShown = new core.EventEmitter();
            this.tooltipHide = new core.EventEmitter();
            this.tooltipHidden = new core.EventEmitter();
            this._open = false;
            this._showTimeout = 0;
            this._hideTimeout = 0;
            this._destroy$ = new rxjs.Subject();
        }
        MdbTooltipDirective.prototype.ngOnInit = function () {
            if (this.tooltipDisabled) {
                return;
            }
            this._bindTriggerEvents();
            this._createOverlay();
        };
        MdbTooltipDirective.prototype.ngOnDestroy = function () {
            if (this._open) {
                this.hide();
            }
            this._destroy$.next();
            this._destroy$.complete();
        };
        MdbTooltipDirective.prototype._bindTriggerEvents = function () {
            var _this = this;
            var triggers = this.trigger.split(' ');
            triggers.forEach(function (trigger) {
                if (trigger === 'click') {
                    rxjs.fromEvent(_this._elementRef.nativeElement, trigger)
                        .pipe(operators.takeUntil(_this._destroy$))
                        .subscribe(function () { return _this.toggle(); });
                }
                else if (trigger !== 'manual') {
                    var evIn = trigger === 'hover' ? 'mouseenter' : 'focusin';
                    var evOut = trigger === 'hover' ? 'mouseleave' : 'focusout';
                    rxjs.fromEvent(_this._elementRef.nativeElement, evIn)
                        .pipe(operators.takeUntil(_this._destroy$))
                        .subscribe(function () { return _this.show(); });
                    rxjs.fromEvent(_this._elementRef.nativeElement, evOut)
                        .pipe(operators.takeUntil(_this._destroy$))
                        .subscribe(function () { return _this.hide(); });
                }
            });
        };
        MdbTooltipDirective.prototype._createOverlayConfig = function () {
            var positionStrategy = this._overlayPositionBuilder
                .flexibleConnectedTo(this._elementRef)
                .withPositions(this._getPosition());
            var overlayConfig = new overlay.OverlayConfig({
                hasBackdrop: false,
                scrollStrategy: this._overlay.scrollStrategies.reposition(),
                positionStrategy: positionStrategy,
            });
            return overlayConfig;
        };
        MdbTooltipDirective.prototype._createOverlay = function () {
            this._overlayRef = this._overlay.create(this._createOverlayConfig());
        };
        MdbTooltipDirective.prototype._getPosition = function () {
            var position;
            var positionTop = {
                originX: 'center',
                originY: 'top',
                overlayX: 'center',
                overlayY: 'bottom',
                offsetY: -this.offset,
            };
            var positionBottom = {
                originX: 'center',
                originY: 'bottom',
                overlayX: 'center',
                overlayY: 'top',
                offsetY: this.offset,
            };
            var positionRight = {
                originX: 'end',
                originY: 'center',
                overlayX: 'start',
                overlayY: 'center',
                offsetX: this.offset,
            };
            var positionLeft = {
                originX: 'start',
                originY: 'center',
                overlayX: 'end',
                overlayY: 'center',
                offsetX: -this.offset,
            };
            switch (this.placement) {
                case 'top':
                    position = [positionTop, positionBottom];
                    break;
                case 'bottom':
                    position = [positionBottom, positionTop];
                    break;
                case 'left':
                    position = [positionLeft, positionRight];
                    break;
                case 'right':
                    position = [positionRight, positionLeft];
                    break;
                default:
                    break;
            }
            return position;
        };
        MdbTooltipDirective.prototype.show = function () {
            var _this = this;
            if (this._open) {
                this._overlayRef.detach();
            }
            if (this._hideTimeout) {
                clearTimeout(this._hideTimeout);
                this._hideTimeout = null;
            }
            this._showTimeout = setTimeout(function () {
                var tooltipPortal = new portal.ComponentPortal(MdbTooltipComponent);
                _this.tooltipShow.emit(_this);
                _this._open = true;
                _this._tooltipRef = _this._overlayRef.attach(tooltipPortal);
                _this._tooltipRef.instance.title = _this.mdbTooltip;
                _this._tooltipRef.instance.html = _this.html;
                _this._tooltipRef.instance.animation = _this.animation;
                _this._tooltipRef.instance.animationState = 'visible';
                _this._tooltipRef.instance.markForCheck();
                _this.tooltipShown.emit(_this);
            }, this.delayShow);
        };
        MdbTooltipDirective.prototype.hide = function () {
            var _this = this;
            if (!this._open) {
                return;
            }
            if (this._showTimeout) {
                clearTimeout(this._showTimeout);
                this._showTimeout = null;
            }
            this._hideTimeout = setTimeout(function () {
                _this.tooltipHide.emit(_this);
                _this._tooltipRef.instance._hidden.pipe(operators.first()).subscribe(function () {
                    _this._overlayRef.detach();
                    _this._open = false;
                    _this.tooltipShown.emit(_this);
                });
                _this._tooltipRef.instance.animationState = 'hidden';
                _this._tooltipRef.instance.markForCheck();
            }, this.delayHide);
        };
        MdbTooltipDirective.prototype.toggle = function () {
            if (this._open) {
                this.hide();
            }
            else {
                this.show();
            }
        };
        return MdbTooltipDirective;
    }());
    MdbTooltipDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbTooltip]',
                    exportAs: 'mdbTooltip',
                },] }
    ];
    MdbTooltipDirective.ctorParameters = function () { return [
        { type: overlay.Overlay },
        { type: overlay.OverlayPositionBuilder },
        { type: core.ElementRef },
        { type: core.Renderer2 }
    ]; };
    MdbTooltipDirective.propDecorators = {
        mdbTooltip: [{ type: core.Input }],
        tooltipDisabled: [{ type: core.Input }],
        placement: [{ type: core.Input }],
        html: [{ type: core.Input }],
        animation: [{ type: core.Input }],
        trigger: [{ type: core.Input }],
        delayShow: [{ type: core.Input }],
        delayHide: [{ type: core.Input }],
        offset: [{ type: core.Input }],
        tooltipShow: [{ type: core.Output }],
        tooltipShown: [{ type: core.Output }],
        tooltipHide: [{ type: core.Output }],
        tooltipHidden: [{ type: core.Output }]
    };

    var MdbTooltipModule = /** @class */ (function () {
        function MdbTooltipModule() {
        }
        return MdbTooltipModule;
    }());
    MdbTooltipModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [common.CommonModule, overlay.OverlayModule],
                    declarations: [MdbTooltipDirective, MdbTooltipComponent],
                    exports: [MdbTooltipDirective, MdbTooltipComponent],
                },] }
    ];

    var MdbPopoverComponent = /** @class */ (function () {
        function MdbPopoverComponent(_cdRef) {
            this._cdRef = _cdRef;
            this._hidden = new rxjs.Subject();
            this.animationState = 'hidden';
        }
        MdbPopoverComponent.prototype.ngOnInit = function () { };
        MdbPopoverComponent.prototype.markForCheck = function () {
            this._cdRef.markForCheck();
        };
        MdbPopoverComponent.prototype.onAnimationEnd = function (event) {
            if (event.toState === 'hidden') {
                this._hidden.next();
            }
        };
        return MdbPopoverComponent;
    }());
    MdbPopoverComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-popover',
                    template: "<div\n  class=\"popover\"\n  [@fade]=\"animationState\"\n  (@fade.done)=\"onAnimationEnd($event)\"\n  [@.disabled]=\"!animation\"\n>\n  <div *ngIf=\"title\" class=\"popover-header\">\n    {{ title }}\n  </div>\n  <div *ngIf=\"template\" [innerHTML]=\"content\" class=\"popover-body\"></div>\n  <div *ngIf=\"!template\" class=\"popover-body\">\n    {{ content }}\n  </div>\n</div>\n",
                    changeDetection: core.ChangeDetectionStrategy.OnPush,
                    animations: [
                        animations.trigger('fade', [
                            animations.state('visible', animations.style({ opacity: 1 })),
                            animations.state('hidden', animations.style({ opacity: 0 })),
                            animations.transition('visible <=> hidden', animations.animate('150ms linear')),
                            animations.transition(':enter', animations.animate('150ms linear')),
                        ]),
                    ]
                },] }
    ];
    MdbPopoverComponent.ctorParameters = function () { return [
        { type: core.ChangeDetectorRef }
    ]; };
    MdbPopoverComponent.propDecorators = {
        title: [{ type: core.Input }],
        content: [{ type: core.Input }],
        template: [{ type: core.Input }],
        animation: [{ type: core.Input }]
    };

    // tslint:disable-next-line:component-class-suffix
    var MdbPopoverDirective = /** @class */ (function () {
        function MdbPopoverDirective(_overlay, _overlayPositionBuilder, _elementRef) {
            this._overlay = _overlay;
            this._overlayPositionBuilder = _overlayPositionBuilder;
            this._elementRef = _elementRef;
            this.mdbPopover = '';
            this.mdbPopoverTitle = '';
            this.popoverDisabled = false;
            this.placement = 'top';
            this.template = false;
            this.animation = true;
            this.trigger = 'click';
            this.delayShow = 0;
            this.delayHide = 0;
            this.offset = 4;
            this.popoverShow = new core.EventEmitter();
            this.popoverShown = new core.EventEmitter();
            this.popoverHide = new core.EventEmitter();
            this.popoverHidden = new core.EventEmitter();
            this._open = false;
            this._showTimeout = 0;
            this._hideTimeout = 0;
            this._destroy$ = new rxjs.Subject();
        }
        MdbPopoverDirective.prototype.ngOnInit = function () {
            if (this.popoverDisabled) {
                return;
            }
            this._bindTriggerEvents();
            this._createOverlay();
        };
        MdbPopoverDirective.prototype.ngOnDestroy = function () {
            if (this._open) {
                this.hide();
            }
            this._destroy$.next();
            this._destroy$.complete();
        };
        MdbPopoverDirective.prototype._bindTriggerEvents = function () {
            var _this = this;
            var triggers = this.trigger.split(' ');
            triggers.forEach(function (trigger) {
                if (trigger === 'click') {
                    rxjs.fromEvent(_this._elementRef.nativeElement, trigger)
                        .pipe(operators.takeUntil(_this._destroy$))
                        .subscribe(function () { return _this.toggle(); });
                }
                else if (trigger !== 'manual') {
                    var evIn = trigger === 'hover' ? 'mouseenter' : 'focusin';
                    var evOut = trigger === 'hover' ? 'mouseleave' : 'focusout';
                    rxjs.fromEvent(_this._elementRef.nativeElement, evIn)
                        .pipe(operators.takeUntil(_this._destroy$))
                        .subscribe(function () { return _this.show(); });
                    rxjs.fromEvent(_this._elementRef.nativeElement, evOut)
                        .pipe(operators.takeUntil(_this._destroy$))
                        .subscribe(function () { return _this.hide(); });
                }
            });
        };
        MdbPopoverDirective.prototype._createOverlayConfig = function () {
            var positionStrategy = this._overlayPositionBuilder
                .flexibleConnectedTo(this._elementRef)
                .withPositions(this._getPosition());
            var overlayConfig = new overlay.OverlayConfig({
                hasBackdrop: false,
                scrollStrategy: this._overlay.scrollStrategies.reposition(),
                positionStrategy: positionStrategy,
            });
            return overlayConfig;
        };
        MdbPopoverDirective.prototype._createOverlay = function () {
            this._overlayRef = this._overlay.create(this._createOverlayConfig());
        };
        MdbPopoverDirective.prototype._getPosition = function () {
            var position;
            var positionTop = {
                originX: 'center',
                originY: 'top',
                overlayX: 'center',
                overlayY: 'bottom',
                offsetY: -this.offset,
            };
            var positionBottom = {
                originX: 'center',
                originY: 'bottom',
                overlayX: 'center',
                overlayY: 'top',
                offsetY: this.offset,
            };
            var positionRight = {
                originX: 'end',
                originY: 'center',
                overlayX: 'start',
                overlayY: 'center',
                offsetX: this.offset,
            };
            var positionLeft = {
                originX: 'start',
                originY: 'center',
                overlayX: 'end',
                overlayY: 'center',
                offsetX: -this.offset,
            };
            switch (this.placement) {
                case 'top':
                    position = [positionTop, positionBottom];
                    break;
                case 'bottom':
                    position = [positionBottom, positionTop];
                    break;
                case 'left':
                    position = [positionLeft, positionRight, positionTop, positionBottom];
                    break;
                case 'right':
                    position = [positionRight, positionLeft, positionTop, positionBottom];
                    break;
                default:
                    break;
            }
            return position;
        };
        MdbPopoverDirective.prototype.show = function () {
            var _this = this;
            if (this._open) {
                this._overlayRef.detach();
            }
            if (this._hideTimeout) {
                clearTimeout(this._hideTimeout);
                this._hideTimeout = null;
            }
            this._showTimeout = setTimeout(function () {
                var tooltipPortal = new portal.ComponentPortal(MdbPopoverComponent);
                _this.popoverShow.emit(_this);
                _this._open = true;
                _this._tooltipRef = _this._overlayRef.attach(tooltipPortal);
                _this._tooltipRef.instance.content = _this.mdbPopover;
                _this._tooltipRef.instance.title = _this.mdbPopoverTitle;
                _this._tooltipRef.instance.template = _this.template;
                _this._tooltipRef.instance.animation = _this.animation;
                _this._tooltipRef.instance.animationState = 'visible';
                _this._tooltipRef.instance.markForCheck();
                _this.popoverShown.emit(_this);
            }, this.delayShow);
        };
        MdbPopoverDirective.prototype.hide = function () {
            var _this = this;
            if (!this._open) {
                return;
            }
            if (this._showTimeout) {
                clearTimeout(this._showTimeout);
                this._showTimeout = null;
            }
            this._hideTimeout = setTimeout(function () {
                _this.popoverHide.emit(_this);
                _this._tooltipRef.instance._hidden.pipe(operators.first()).subscribe(function () {
                    _this._overlayRef.detach();
                    _this._open = false;
                    _this.popoverShown.emit(_this);
                });
                _this._tooltipRef.instance.animationState = 'hidden';
                _this._tooltipRef.instance.markForCheck();
            }, this.delayHide);
        };
        MdbPopoverDirective.prototype.toggle = function () {
            if (this._open) {
                this.hide();
            }
            else {
                this.show();
            }
        };
        return MdbPopoverDirective;
    }());
    MdbPopoverDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbPopover]',
                    exportAs: 'mdbPopover',
                },] }
    ];
    MdbPopoverDirective.ctorParameters = function () { return [
        { type: overlay.Overlay },
        { type: overlay.OverlayPositionBuilder },
        { type: core.ElementRef }
    ]; };
    MdbPopoverDirective.propDecorators = {
        mdbPopover: [{ type: core.Input }],
        mdbPopoverTitle: [{ type: core.Input }],
        popoverDisabled: [{ type: core.Input }],
        placement: [{ type: core.Input }],
        template: [{ type: core.Input }],
        animation: [{ type: core.Input }],
        trigger: [{ type: core.Input }],
        delayShow: [{ type: core.Input }],
        delayHide: [{ type: core.Input }],
        offset: [{ type: core.Input }],
        popoverShow: [{ type: core.Output }],
        popoverShown: [{ type: core.Output }],
        popoverHide: [{ type: core.Output }],
        popoverHidden: [{ type: core.Output }]
    };

    var MdbPopoverModule = /** @class */ (function () {
        function MdbPopoverModule() {
        }
        return MdbPopoverModule;
    }());
    MdbPopoverModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [common.CommonModule, overlay.OverlayModule],
                    declarations: [MdbPopoverDirective, MdbPopoverComponent],
                    exports: [MdbPopoverDirective, MdbPopoverComponent],
                },] }
    ];

    // tslint:disable-next-line: directive-class-suffix
    var MdbAbstractFormControl = /** @class */ (function () {
        function MdbAbstractFormControl() {
        }
        return MdbAbstractFormControl;
    }());
    MdbAbstractFormControl.decorators = [
        { type: core.Directive }
    ];

    // tslint:disable-next-line: component-class-suffix
    var MdbInputDirective = /** @class */ (function () {
        function MdbInputDirective(_elementRef, _renderer) {
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this.stateChanges = new rxjs.Subject();
            this._focused = false;
            this._disabled = false;
            this._readonly = false;
        }
        Object.defineProperty(MdbInputDirective.prototype, "disabled", {
            get: function () {
                return this._disabled;
            },
            set: function (value) {
                this._disabled = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbInputDirective.prototype, "readonly", {
            get: function () {
                return this._readonly;
            },
            set: function (value) {
                if (value) {
                    this._renderer.setAttribute(this._elementRef.nativeElement, 'readonly', '');
                }
                else {
                    this._renderer.removeAttribute(this._elementRef.nativeElement, 'readonly');
                }
                this._readonly = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbInputDirective.prototype, "value", {
            get: function () {
                return this._elementRef.nativeElement.value;
            },
            set: function (value) {
                if (value !== this.value) {
                    this._elementRef.nativeElement.value = value;
                    this.stateChanges.next();
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbInputDirective.prototype._onFocus = function () {
            this._focused = true;
            this.stateChanges.next();
        };
        MdbInputDirective.prototype._onBlur = function () {
            this._focused = false;
            this.stateChanges.next();
        };
        Object.defineProperty(MdbInputDirective.prototype, "hasValue", {
            get: function () {
                return this._elementRef.nativeElement.value !== '';
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbInputDirective.prototype, "focused", {
            get: function () {
                return this._focused;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbInputDirective.prototype, "labelActive", {
            get: function () {
                return this.focused || this.hasValue;
            },
            enumerable: false,
            configurable: true
        });
        return MdbInputDirective;
    }());
    MdbInputDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbInput]',
                    exportAs: 'mdbInput',
                    providers: [{ provide: MdbAbstractFormControl, useExisting: MdbInputDirective }],
                },] }
    ];
    MdbInputDirective.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 }
    ]; };
    MdbInputDirective.propDecorators = {
        disabled: [{ type: core.HostBinding, args: ['disabled',] }, { type: core.Input, args: ['disabled',] }],
        readonly: [{ type: core.Input, args: ['readonly',] }],
        value: [{ type: core.Input }],
        _onFocus: [{ type: core.HostListener, args: ['focus',] }],
        _onBlur: [{ type: core.HostListener, args: ['blur',] }]
    };

    // tslint:disable-next-line: component-class-suffix
    var MdbLabelDirective = /** @class */ (function () {
        function MdbLabelDirective() {
        }
        return MdbLabelDirective;
    }());
    MdbLabelDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbLabel]',
                    exportAs: 'mdbLabel',
                },] }
    ];
    MdbLabelDirective.ctorParameters = function () { return []; };

    var MdbFormControlComponent = /** @class */ (function () {
        function MdbFormControlComponent(_renderer, _contentObserver) {
            this._renderer = _renderer;
            this._contentObserver = _contentObserver;
            this.outline = true;
            this._destroy$ = new rxjs.Subject();
            this._notchLeadingLength = 9;
            this._labelMarginLeft = 0;
            this._labelGapPadding = 8;
            this._labelScale = 0.8;
        }
        MdbFormControlComponent.prototype.ngAfterViewInit = function () { };
        MdbFormControlComponent.prototype.ngAfterContentInit = function () {
            var _this = this;
            if (this._label) {
                this._updateBorderGap();
            }
            else {
                this._renderer.addClass(this._input.nativeElement, 'placeholder-active');
            }
            this._updateLabelActiveState();
            if (this._label) {
                this._contentObserver
                    .observe(this._label.nativeElement)
                    .pipe(operators.takeUntil(this._destroy$))
                    .subscribe(function () {
                    _this._updateBorderGap();
                });
            }
            this._formControl.stateChanges.pipe(operators.takeUntil(this._destroy$)).subscribe(function () {
                _this._updateLabelActiveState();
                if (_this._label) {
                    _this._updateBorderGap();
                }
            });
        };
        MdbFormControlComponent.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.unsubscribe();
        };
        MdbFormControlComponent.prototype._getLabelWidth = function () {
            return this._label.nativeElement.clientWidth * this._labelScale + this._labelGapPadding;
        };
        MdbFormControlComponent.prototype._updateBorderGap = function () {
            var notchLeadingWidth = this._labelMarginLeft + this._notchLeadingLength + "px";
            var notchMiddleWidth = this._getLabelWidth() + "px";
            this._renderer.setStyle(this._notchLeading.nativeElement, 'width', notchLeadingWidth);
            this._renderer.setStyle(this._notchMiddle.nativeElement, 'width', notchMiddleWidth);
            this._renderer.setStyle(this._label.nativeElement, 'margin-left', this._labelMarginLeft + "px");
        };
        MdbFormControlComponent.prototype._updateLabelActiveState = function () {
            if (this._isLabelActive()) {
                this._renderer.addClass(this._input.nativeElement, 'active');
            }
            else {
                this._renderer.removeClass(this._input.nativeElement, 'active');
            }
        };
        MdbFormControlComponent.prototype._isLabelActive = function () {
            return this._formControl && this._formControl.labelActive;
        };
        return MdbFormControlComponent;
    }());
    MdbFormControlComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-form-control',
                    template: "<ng-content></ng-content>\n<div class=\"form-notch\">\n  <div #notchLeading class=\"form-notch-leading\"></div>\n  <div #notchMiddle class=\"form-notch-middle\"></div>\n  <div class=\"form-notch-trailing\"></div>\n</div>\n",
                    changeDetection: core.ChangeDetectionStrategy.OnPush
                },] }
    ];
    MdbFormControlComponent.ctorParameters = function () { return [
        { type: core.Renderer2 },
        { type: observers.ContentObserver }
    ]; };
    MdbFormControlComponent.propDecorators = {
        _notchLeading: [{ type: core.ViewChild, args: ['notchLeading', { static: true },] }],
        _notchMiddle: [{ type: core.ViewChild, args: ['notchMiddle', { static: true },] }],
        _input: [{ type: core.ContentChild, args: [MdbInputDirective, { static: true, read: core.ElementRef },] }],
        _formControl: [{ type: core.ContentChild, args: [MdbAbstractFormControl, { static: true },] }],
        _label: [{ type: core.ContentChild, args: [MdbLabelDirective, { static: true, read: core.ElementRef },] }],
        outline: [{ type: core.HostBinding, args: ['class.form-outline',] }]
    };

    var MdbFormsModule = /** @class */ (function () {
        function MdbFormsModule() {
        }
        return MdbFormsModule;
    }());
    MdbFormsModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [MdbFormControlComponent, MdbInputDirective, MdbLabelDirective],
                    exports: [MdbFormControlComponent, MdbInputDirective, MdbLabelDirective],
                    imports: [common.CommonModule, forms.FormsModule],
                },] }
    ];

    var MdbModalContainerComponent = /** @class */ (function () {
        function MdbModalContainerComponent(_document, _elementRef, _renderer, _focusTrapFactory) {
            this._document = _document;
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this._focusTrapFactory = _focusTrapFactory;
            this._destroy$ = new rxjs.Subject();
            this.backdropClick$ = new rxjs.Subject();
            this.BACKDROP_TRANSITION = 150;
            this.MODAL_TRANSITION = 200;
            this.modal = true;
        }
        Object.defineProperty(MdbModalContainerComponent.prototype, "hasAnimation", {
            get: function () {
                return this._config.animation;
            },
            enumerable: false,
            configurable: true
        });
        MdbModalContainerComponent.prototype.ngOnInit = function () {
            var _this = this;
            this._updateContainerClass();
            this._renderer.addClass(this._document.body, 'modal-open');
            this._renderer.setStyle(this._document.body, 'padding-right', '15px');
            this._renderer.setStyle(this._elementRef.nativeElement, 'display', 'block');
            this._previouslyFocusedElement = this._document.activeElement;
            this._focusTrap = this._focusTrapFactory.create(this._elementRef.nativeElement);
            if (this._config.animation) {
                setTimeout(function () {
                    _this._renderer.addClass(_this._elementRef.nativeElement, 'show');
                    setTimeout(function () {
                        _this._focusTrap.focusInitialElementWhenReady();
                    }, _this.MODAL_TRANSITION);
                }, this.BACKDROP_TRANSITION);
            }
            else {
                this._focusTrap.focusInitialElementWhenReady();
            }
        };
        MdbModalContainerComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            if (!this._config.ignoreBackdropClick) {
                rxjs.fromEvent(this._elementRef.nativeElement, 'click')
                    .pipe(operators.filter(function (event) {
                    var target = event.target;
                    var dialog = _this.modalDialog.nativeElement;
                    var notDialog = target !== dialog;
                    var notDialogContent = !dialog.contains(target);
                    return notDialog && notDialogContent;
                }), operators.takeUntil(this._destroy$))
                    .subscribe(function (event) {
                    _this.backdropClick$.next(event);
                });
            }
        };
        MdbModalContainerComponent.prototype.ngOnDestroy = function () {
            this._previouslyFocusedElement.focus();
            this._focusTrap.destroy();
            this._destroy$.next();
            this._destroy$.complete();
        };
        MdbModalContainerComponent.prototype._updateContainerClass = function () {
            var _this = this;
            if (this._config.containerClass === '' ||
                (this._config.containerClass.length && this._config.containerClass.length === 0)) {
                return;
            }
            var containerClasses = this._config.containerClass.split(' ');
            containerClasses.forEach(function (containerClass) {
                _this._renderer.addClass(_this._elementRef.nativeElement, containerClass);
            });
        };
        MdbModalContainerComponent.prototype._close = function () {
            var _this = this;
            if (this._config.animation) {
                this._renderer.removeClass(this._elementRef.nativeElement, 'show');
            }
            // Pause iframe/video when closing modal
            var iframeElements = Array.from(this._elementRef.nativeElement.querySelectorAll('iframe'));
            var videoElements = Array.from(this._elementRef.nativeElement.querySelectorAll('video'));
            iframeElements.forEach(function (iframe) {
                var srcAttribute = iframe.getAttribute('src');
                _this._renderer.setAttribute(iframe, 'src', srcAttribute);
            });
            videoElements.forEach(function (video) {
                video.pause();
            });
        };
        MdbModalContainerComponent.prototype._restoreScrollbar = function () {
            this._renderer.removeClass(this._document.body, 'modal-open');
            this._renderer.removeStyle(this._document.body, 'padding-right');
        };
        MdbModalContainerComponent.prototype.attachComponentPortal = function (portal) {
            return this._portalOutlet.attachComponentPortal(portal);
        };
        MdbModalContainerComponent.prototype.attachTemplatePortal = function (portal) {
            return this._portalOutlet.attachTemplatePortal(portal);
        };
        return MdbModalContainerComponent;
    }());
    MdbModalContainerComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-modal-container',
                    template: "<div #dialog [class]=\"'modal-dialog' + (_config.modalClass ? ' ' + _config.modalClass : '')\">\n  <div class=\"modal-content\">\n    <ng-template cdkPortalOutlet></ng-template>\n  </div>\n</div>\n",
                    changeDetection: core.ChangeDetectionStrategy.Default
                },] }
    ];
    MdbModalContainerComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
        { type: core.ElementRef },
        { type: core.Renderer2 },
        { type: a11y.ConfigurableFocusTrapFactory }
    ]; };
    MdbModalContainerComponent.propDecorators = {
        _portalOutlet: [{ type: core.ViewChild, args: [portal.CdkPortalOutlet, { static: true },] }],
        modalDialog: [{ type: core.ViewChild, args: ['dialog', { static: true },] }],
        modal: [{ type: core.HostBinding, args: ['class.modal',] }],
        hasAnimation: [{ type: core.HostBinding, args: ['class.fade',] }]
    };

    // tslint:disable: no-inferrable-types
    var MdbModalConfig = /** @class */ (function () {
        function MdbModalConfig() {
            this.animation = true;
            this.backdrop = true;
            this.ignoreBackdropClick = false;
            this.keyboard = true;
            this.modalClass = '';
            this.containerClass = '';
            this.data = null;
        }
        return MdbModalConfig;
    }());

    var MdbModalRef = /** @class */ (function () {
        function MdbModalRef(_overlayRef, _container) {
            this._overlayRef = _overlayRef;
            this._container = _container;
            this.onClose$ = new rxjs.Subject();
            this.onClose = this.onClose$.asObservable();
        }
        MdbModalRef.prototype.close = function (message) {
            var _this = this;
            this._container._close();
            setTimeout(function () {
                _this._container._restoreScrollbar();
                _this.onClose$.next(message);
                _this.onClose$.complete();
                _this._overlayRef.detach();
                _this._overlayRef.dispose();
            }, this._container.MODAL_TRANSITION);
        };
        return MdbModalRef;
    }());

    var MdbModalService = /** @class */ (function () {
        function MdbModalService(_document, _overlay, _injector, _cfr) {
            this._document = _document;
            this._overlay = _overlay;
            this._injector = _injector;
            this._cfr = _cfr;
        }
        MdbModalService.prototype.open = function (componentOrTemplateRef, config) {
            var defaultConfig = new MdbModalConfig();
            config = config ? Object.assign(defaultConfig, config) : defaultConfig;
            var overlayRef = this._createOverlay(config);
            var container = this._createContainer(overlayRef, config);
            var modalRef = this._createContent(componentOrTemplateRef, container, overlayRef, config);
            this._registerListeners(modalRef, config, container);
            return modalRef;
        };
        MdbModalService.prototype._createOverlay = function (config) {
            var overlayConfig = this._getOverlayConfig(config);
            return this._overlay.create(overlayConfig);
        };
        MdbModalService.prototype._getOverlayConfig = function (modalConfig) {
            var config = new overlay.OverlayConfig({
                positionStrategy: this._overlay.position().global(),
                scrollStrategy: this._overlay.scrollStrategies.noop(),
                hasBackdrop: modalConfig.backdrop,
                backdropClass: 'mdb-backdrop',
            });
            return config;
        };
        MdbModalService.prototype._createContainer = function (overlayRef, config) {
            var portal$1 = new portal.ComponentPortal(MdbModalContainerComponent, null, this._injector, this._cfr);
            var containerRef = overlayRef.attach(portal$1);
            containerRef.instance._config = config;
            return containerRef.instance;
        };
        MdbModalService.prototype._createContent = function (componentOrTemplate, container, overlayRef, config) {
            var modalRef = new MdbModalRef(overlayRef, container);
            if (componentOrTemplate instanceof core.TemplateRef) {
                container.attachTemplatePortal(new portal.TemplatePortal(componentOrTemplate, null, {
                    $implicit: config.data,
                    modalRef: modalRef,
                }));
            }
            else {
                var injector = this._createInjector(config, modalRef, container);
                var contentRef = container.attachComponentPortal(new portal.ComponentPortal(componentOrTemplate, config.viewContainerRef, injector));
                if (config.data) {
                    Object.assign(contentRef.instance, Object.assign({}, config.data));
                }
            }
            return modalRef;
        };
        MdbModalService.prototype._createInjector = function (config, modalRef, container) {
            var userInjector = config && config.viewContainerRef && config.viewContainerRef.injector;
            // The dialog container should be provided as the dialog container and the dialog's
            // content are created out of the same `ViewContainerRef` and as such, are siblings
            // for injector purposes. To allow the hierarchy that is expected, the dialog
            // container is explicitly provided in the injector.
            var providers = [
                { provide: MdbModalContainerComponent, useValue: container },
                { provide: MdbModalRef, useValue: modalRef },
            ];
            return core.Injector.create({ parent: userInjector || this._injector, providers: providers });
        };
        MdbModalService.prototype._registerListeners = function (modalRef, config, container) {
            container.backdropClick$.pipe(operators.take(1)).subscribe(function () {
                modalRef.close();
            });
            if (config.keyboard) {
                rxjs.fromEvent(container._elementRef.nativeElement, 'keydown')
                    .pipe(operators.filter(function (event) {
                    return event.key === 'Escape';
                }), operators.take(1))
                    .subscribe(function () {
                    modalRef.close();
                });
            }
        };
        return MdbModalService;
    }());
    MdbModalService.decorators = [
        { type: core.Injectable }
    ];
    MdbModalService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
        { type: overlay.Overlay },
        { type: core.Injector },
        { type: core.ComponentFactoryResolver }
    ]; };

    var MdbModalModule = /** @class */ (function () {
        function MdbModalModule() {
        }
        return MdbModalModule;
    }());
    MdbModalModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [overlay.OverlayModule, portal.PortalModule],
                    exports: [MdbModalContainerComponent],
                    declarations: [MdbModalContainerComponent],
                    providers: [MdbModalService],
                    entryComponents: [MdbModalContainerComponent],
                },] }
    ];

    // tslint:disable-next-line:component-class-suffix
    var MdbDropdownToggleDirective = /** @class */ (function () {
        function MdbDropdownToggleDirective() {
        }
        MdbDropdownToggleDirective.prototype.ngOnInit = function () { };
        return MdbDropdownToggleDirective;
    }());
    MdbDropdownToggleDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbDropdownToggle]',
                    exportAs: 'mdbDropdownToggle',
                },] }
    ];
    MdbDropdownToggleDirective.ctorParameters = function () { return []; };

    // tslint:disable-next-line:component-class-suffix
    var MdbDropdownMenuDirective = /** @class */ (function () {
        function MdbDropdownMenuDirective() {
        }
        MdbDropdownMenuDirective.prototype.ngOnInit = function () { };
        return MdbDropdownMenuDirective;
    }());
    MdbDropdownMenuDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbDropdownMenu]',
                    exportAs: 'mdbDropdownMenu',
                },] }
    ];
    MdbDropdownMenuDirective.ctorParameters = function () { return []; };

    // tslint:disable-next-line:component-class-suffix
    var MdbDropdownDirective = /** @class */ (function () {
        function MdbDropdownDirective(_overlay, _overlayPositionBuilder, _elementRef, _vcr, _breakpointObserver, _cdRef) {
            this._overlay = _overlay;
            this._overlayPositionBuilder = _overlayPositionBuilder;
            this._elementRef = _elementRef;
            this._vcr = _vcr;
            this._breakpointObserver = _breakpointObserver;
            this._cdRef = _cdRef;
            this.animation = true;
            this.offset = 0;
            this.dropdownShow = new core.EventEmitter();
            this.dropdownShown = new core.EventEmitter();
            this.dropdownHide = new core.EventEmitter();
            this.dropdownHidden = new core.EventEmitter();
            this._open = false;
            this._breakpoints = {
                isSm: this._breakpointObserver.isMatched('(min-width: 576px)'),
                isMd: this._breakpointObserver.isMatched('(min-width: 768px)'),
                isLg: this._breakpointObserver.isMatched('(min-width: 992px)'),
                isXl: this._breakpointObserver.isMatched('(min-width: 1200px)'),
                isXxl: this._breakpointObserver.isMatched('(min-width: 1400px)'),
            };
            this._destroy$ = new rxjs.Subject();
            this._animationState = 'hidden';
        }
        MdbDropdownDirective.prototype.ngOnInit = function () { };
        MdbDropdownDirective.prototype.ngAfterContentInit = function () {
            this._bindDropdownToggleClick();
        };
        MdbDropdownDirective.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
        };
        MdbDropdownDirective.prototype._bindDropdownToggleClick = function () {
            var _this = this;
            rxjs.fromEvent(this._dropdownToggle.nativeElement, 'click')
                .pipe(operators.takeUntil(this._destroy$))
                .subscribe(function () { return _this.toggle(); });
        };
        MdbDropdownDirective.prototype._createOverlayConfig = function () {
            return new overlay.OverlayConfig({
                hasBackdrop: false,
                scrollStrategy: this._overlay.scrollStrategies.reposition(),
                positionStrategy: this._createPositionStrategy(),
            });
        };
        MdbDropdownDirective.prototype._createOverlay = function () {
            this._overlayRef = this._overlay.create(this._createOverlayConfig());
        };
        MdbDropdownDirective.prototype._createPositionStrategy = function () {
            var positionStrategy = this._overlayPositionBuilder
                .flexibleConnectedTo(this._dropdownToggle)
                .withPositions(this._getPosition())
                .withFlexibleDimensions(false);
            return positionStrategy;
        };
        MdbDropdownDirective.prototype._getPosition = function () {
            this._isDropUp = this._elementRef.nativeElement.classList.contains('dropup');
            this._isDropStart = this._elementRef.nativeElement.classList.contains('dropstart');
            this._isDropEnd = this._elementRef.nativeElement.classList.contains('dropend');
            this._isDropdownMenuEnd = this._dropdownMenu.nativeElement.classList.contains('dropdown-menu-end');
            this._xPosition = this._isDropdownMenuEnd ? 'end' : 'start';
            var regex = new RegExp(/dropdown-menu-(sm|md|lg|xl|xxl)-(start|end)/, 'g');
            var responsiveClass = this._dropdownMenu.nativeElement.className.match(regex);
            if (responsiveClass) {
                this._subscribeBrakpoints();
                var positionRegex = new RegExp(/start|end/, 'g');
                var breakpointRegex = new RegExp(/(sm|md|lg|xl|xxl)/, 'g');
                var dropdownPosition = positionRegex.exec(responsiveClass)[0];
                var breakpoint = breakpointRegex.exec(responsiveClass)[0];
                switch (true) {
                    case breakpoint === 'xxl' && this._breakpoints.isXxl:
                        this._xPosition = dropdownPosition;
                        break;
                    case breakpoint === 'xl' && this._breakpoints.isXl:
                        this._xPosition = dropdownPosition;
                        break;
                    case breakpoint === 'lg' && this._breakpoints.isLg:
                        this._xPosition = dropdownPosition;
                        break;
                    case breakpoint === 'md' && this._breakpoints.isMd:
                        this._xPosition = dropdownPosition;
                        break;
                    case breakpoint === 'sm' && this._breakpoints.isSm:
                        this._xPosition = dropdownPosition;
                        break;
                    default:
                        break;
                }
            }
            var position;
            var positionDropup = {
                originX: this._xPosition,
                originY: 'top',
                overlayX: this._xPosition,
                overlayY: 'bottom',
                offsetY: -this.offset,
            };
            var positionDropdown = {
                originX: this._xPosition,
                originY: 'bottom',
                overlayX: this._xPosition,
                overlayY: 'top',
                offsetY: this.offset,
            };
            var positionDropstart = {
                originX: 'start',
                originY: 'top',
                overlayX: 'end',
                overlayY: 'top',
                offsetX: this.offset,
            };
            var positionDropend = {
                originX: 'end',
                originY: 'top',
                overlayX: 'start',
                overlayY: 'top',
                offsetX: -this.offset,
            };
            switch (true) {
                case this._isDropEnd:
                    position = [positionDropend, positionDropstart];
                    break;
                case this._isDropStart:
                    position = [positionDropstart, positionDropend];
                    break;
                case this._isDropUp:
                    position = [positionDropup, positionDropdown];
                    break;
                default:
                    position = [positionDropdown, positionDropup];
                    break;
            }
            return position;
        };
        MdbDropdownDirective.prototype._listenToOutSideCick = function (overlayRef, origin) {
            var _this = this;
            return rxjs.fromEvent(document, 'click').pipe(operators.filter(function (event) {
                var target = event.target;
                var notOrigin = target !== origin;
                var notValue = !_this._dropdownMenu.nativeElement.contains(target);
                var notOverlay = !!overlayRef && overlayRef.overlayElement.contains(target) === false;
                return notOrigin && notValue && notOverlay;
            }), operators.takeUntil(overlayRef.detachments()));
        };
        MdbDropdownDirective.prototype.onAnimationEnd = function (event) {
            if (event.fromState === 'visible' && event.toState === 'hidden') {
                this._overlayRef.detach();
                this._open = false;
                this.dropdownHidden.emit(this);
            }
            if (event.fromState === 'hidden' && event.toState === 'visible') {
                this.dropdownShown.emit(this);
            }
        };
        MdbDropdownDirective.prototype._subscribeBrakpoints = function () {
            var _this = this;
            var brakpoints = [
                '(min-width: 576px)',
                '(min-width: 768px)',
                '(min-width: 992px)',
                '(min-width: 1200px)',
                '(min-width: 1400px)',
            ];
            this._breakpointSubscription = this._breakpointObserver
                .observe(brakpoints)
                .pipe(operators.takeUntil(this._destroy$))
                .subscribe(function (result) {
                Object.keys(_this._breakpoints).forEach(function (key, index) {
                    var brakpointValue = brakpoints[index];
                    var newBreakpoint = result.breakpoints[brakpointValue];
                    var isBreakpointChanged = newBreakpoint !== _this._breakpoints[key];
                    if (!isBreakpointChanged) {
                        return;
                    }
                    _this._breakpoints[key] = newBreakpoint;
                    if (_this._open) {
                        _this._overlayRef.updatePositionStrategy(_this._createPositionStrategy());
                    }
                });
            });
        };
        MdbDropdownDirective.prototype.show = function () {
            var _this = this;
            this._cdRef.markForCheck();
            if (this._open) {
                return;
            }
            if (!this._overlayRef) {
                this._createOverlay();
            }
            this._portal = new portal.TemplatePortal(this._template, this._vcr);
            this.dropdownShow.emit(this);
            this._open = true;
            this._overlayRef.attach(this._portal);
            this._listenToOutSideCick(this._overlayRef, this._dropdownToggle.nativeElement).subscribe(function () { return _this.hide(); });
            this._animationState = 'visible';
        };
        MdbDropdownDirective.prototype.hide = function () {
            this._cdRef.markForCheck();
            if (!this._open) {
                return;
            }
            this.dropdownHide.emit(this);
            this._animationState = 'hidden';
        };
        MdbDropdownDirective.prototype.toggle = function () {
            this._cdRef.markForCheck();
            if (this._open) {
                this.hide();
            }
            else {
                this.show();
            }
        };
        return MdbDropdownDirective;
    }());
    MdbDropdownDirective.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: '[mdbDropdown]',
                    template: "<ng-content></ng-content>\n<ng-content select=\".dropdown-toggle\"></ng-content>\n<ng-template #dropdownTemplate>\n  <div [@fade]=\"_animationState\" (@fade.done)=\"onAnimationEnd($event)\" [@.disabled]=\"!animation\">\n    <ng-content select=\".dropdown-menu\"></ng-content>\n  </div>\n</ng-template>\n",
                    changeDetection: core.ChangeDetectionStrategy.OnPush,
                    animations: [
                        animations.trigger('fade', [
                            animations.state('visible', animations.style({ opacity: 1 })),
                            animations.state('hidden', animations.style({ opacity: 0 })),
                            animations.transition('visible => hidden', animations.animate('150ms linear')),
                            animations.transition('hidden => visible', [animations.style({ opacity: 0 }), animations.animate('150ms linear')]),
                        ]),
                    ]
                },] }
    ];
    MdbDropdownDirective.ctorParameters = function () { return [
        { type: overlay.Overlay },
        { type: overlay.OverlayPositionBuilder },
        { type: core.ElementRef },
        { type: core.ViewContainerRef },
        { type: layout.BreakpointObserver },
        { type: core.ChangeDetectorRef }
    ]; };
    MdbDropdownDirective.propDecorators = {
        _template: [{ type: core.ViewChild, args: ['dropdownTemplate',] }],
        _dropdownToggle: [{ type: core.ContentChild, args: [MdbDropdownToggleDirective, { read: core.ElementRef },] }],
        _dropdownMenu: [{ type: core.ContentChild, args: [MdbDropdownMenuDirective, { read: core.ElementRef },] }],
        animation: [{ type: core.Input }],
        offset: [{ type: core.Input }],
        dropdownShow: [{ type: core.Output }],
        dropdownShown: [{ type: core.Output }],
        dropdownHide: [{ type: core.Output }],
        dropdownHidden: [{ type: core.Output }]
    };

    var MdbDropdownModule = /** @class */ (function () {
        function MdbDropdownModule() {
        }
        return MdbDropdownModule;
    }());
    MdbDropdownModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [common.CommonModule, overlay.OverlayModule],
                    declarations: [MdbDropdownDirective, MdbDropdownToggleDirective, MdbDropdownMenuDirective],
                    exports: [MdbDropdownDirective, MdbDropdownToggleDirective, MdbDropdownMenuDirective],
                },] }
    ];

    var TRANSITION_BREAK_OPACITY = 0.5;
    var GRADIENT = 'rgba({{color}}, 0.2) 0, rgba({{color}}, 0.3) 40%, rgba({{color}}, 0.4) 50%, rgba({{color}}, 0.5) 60%, rgba({{color}}, 0) 70%';
    var DEFAULT_RIPPLE_COLOR = [0, 0, 0];
    var BOOTSTRAP_COLORS = [
        'primary',
        'secondary',
        'success',
        'danger',
        'warning',
        'info',
        'light',
        'dark',
    ];
    var MdbRippleDirective = /** @class */ (function () {
        function MdbRippleDirective(_elementRef, _renderer) {
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this.rippleCentered = false;
            this.rippleColor = '';
            this.rippleDuration = '500ms';
            this.rippleRadius = 0;
            this.rippleUnbound = false;
            this.ripple = true;
        }
        Object.defineProperty(MdbRippleDirective.prototype, "host", {
            get: function () {
                return this._elementRef.nativeElement;
            },
            enumerable: false,
            configurable: true
        });
        MdbRippleDirective.prototype._createRipple = function (event) {
            var layerX = event.layerX, layerY = event.layerY;
            var offsetX = layerX;
            var offsetY = layerY;
            var height = this.host.offsetHeight;
            var width = this.host.offsetWidth;
            var duration = this._durationToMsNumber(this.rippleDuration);
            var diameterOptions = {
                offsetX: this.rippleCentered ? height / 2 : offsetX,
                offsetY: this.rippleCentered ? width / 2 : offsetY,
                height: height,
                width: width,
            };
            var diameter = this._getDiameter(diameterOptions);
            var radiusValue = this.rippleRadius || diameter / 2;
            var opacity = {
                delay: duration * TRANSITION_BREAK_OPACITY,
                duration: duration - duration * TRANSITION_BREAK_OPACITY,
            };
            var styles = {
                left: this.rippleCentered ? width / 2 - radiusValue + "px" : offsetX - radiusValue + "px",
                top: this.rippleCentered ? height / 2 - radiusValue + "px" : offsetY - radiusValue + "px",
                height: (this.rippleRadius * 2 || diameter) + "px",
                width: (this.rippleRadius * 2 || diameter) + "px",
                transitionDelay: "0s, " + opacity.delay + "ms",
                transitionDuration: duration + "ms, " + opacity.duration + "ms",
            };
            var rippleHTML = this._renderer.createElement('div');
            this._createHTMLRipple(this.host, rippleHTML, styles);
            this._removeHTMLRipple(rippleHTML, duration);
        };
        MdbRippleDirective.prototype._createHTMLRipple = function (wrapper, ripple, styles) {
            Object.keys(styles).forEach(function (property) { return (ripple.style[property] = styles[property]); });
            this._renderer.addClass(ripple, 'ripple-wave');
            if (this.rippleColor !== '') {
                this._removeOldColorClasses(wrapper);
                this._addColor(ripple, wrapper);
            }
            this._toggleUnbound(wrapper);
            this._appendRipple(ripple, wrapper);
        };
        MdbRippleDirective.prototype._removeHTMLRipple = function (ripple, duration) {
            setTimeout(function () {
                if (ripple) {
                    ripple.remove();
                }
            }, duration);
        };
        MdbRippleDirective.prototype._durationToMsNumber = function (time) {
            return Number(time.replace('ms', '').replace('s', '000'));
        };
        MdbRippleDirective.prototype._getDiameter = function (_a) {
            var offsetX = _a.offsetX, offsetY = _a.offsetY, height = _a.height, width = _a.width;
            var top = offsetY <= height / 2;
            var left = offsetX <= width / 2;
            var pythagorean = function (sideA, sideB) { return Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2)); };
            var positionCenter = offsetY === height / 2 && offsetX === width / 2;
            // mouse position on the quadrants of the coordinate system
            var quadrant = {
                first: top === true && left === false,
                second: top === true && left === true,
                third: top === false && left === true,
                fourth: top === false && left === false,
            };
            var getCorner = {
                topLeft: pythagorean(offsetX, offsetY),
                topRight: pythagorean(width - offsetX, offsetY),
                bottomLeft: pythagorean(offsetX, height - offsetY),
                bottomRight: pythagorean(width - offsetX, height - offsetY),
            };
            var diameter = 0;
            if (positionCenter || quadrant.fourth) {
                diameter = getCorner.topLeft;
            }
            else if (quadrant.third) {
                diameter = getCorner.topRight;
            }
            else if (quadrant.second) {
                diameter = getCorner.bottomRight;
            }
            else if (quadrant.first) {
                diameter = getCorner.bottomLeft;
            }
            return diameter * 2;
        };
        MdbRippleDirective.prototype._appendRipple = function (target, parent) {
            var _this = this;
            var FIX_ADD_RIPPLE_EFFECT = 50; // delay for active animations
            this._renderer.appendChild(parent, target);
            setTimeout(function () {
                _this._renderer.addClass(target, 'active');
            }, FIX_ADD_RIPPLE_EFFECT);
        };
        MdbRippleDirective.prototype._toggleUnbound = function (target) {
            if (this.rippleUnbound) {
                this._renderer.addClass(target, 'ripple-surface-unbound');
            }
            else {
                this._renderer.removeClass(target, 'ripple-surface-unbound');
            }
        };
        MdbRippleDirective.prototype._addColor = function (target, parent) {
            var _this = this;
            var isBootstrapColor = BOOTSTRAP_COLORS.find(function (color) { return color === _this.rippleColor.toLowerCase(); });
            if (isBootstrapColor) {
                this._renderer.addClass(parent, 'ripple-surface' + "-" + this.rippleColor.toLowerCase());
            }
            else {
                var rgbValue = this._colorToRGB(this.rippleColor).join(',');
                var gradientImage = GRADIENT.split('{{color}}').join("" + rgbValue);
                target.style.backgroundImage = "radial-gradient(circle, " + gradientImage + ")";
            }
        };
        MdbRippleDirective.prototype._removeOldColorClasses = function (target) {
            var _this = this;
            var REGEXP_CLASS_COLOR = new RegExp('ripple-surface' + "-[a-z]+", 'gi');
            var PARENT_CLASSS_COLOR = target.classList.value.match(REGEXP_CLASS_COLOR) || [];
            PARENT_CLASSS_COLOR.forEach(function (className) {
                _this._renderer.removeClass(target, className);
            });
        };
        MdbRippleDirective.prototype._colorToRGB = function (color) {
            // tslint:disable-next-line: no-shadowed-variable
            function hexToRgb(color) {
                var HEX_COLOR_LENGTH = 7;
                var IS_SHORT_HEX = color.length < HEX_COLOR_LENGTH;
                if (IS_SHORT_HEX) {
                    color = "#" + color[1] + color[1] + color[2] + color[2] + color[3] + color[3];
                }
                return [
                    parseInt(color.substr(1, 2), 16),
                    parseInt(color.substr(3, 2), 16),
                    parseInt(color.substr(5, 2), 16),
                ];
            }
            // tslint:disable-next-line: no-shadowed-variable
            function namedColorsToRgba(color) {
                var tempElem = document.body.appendChild(document.createElement('fictum'));
                var flag = 'rgb(1, 2, 3)';
                tempElem.style.color = flag;
                if (tempElem.style.color !== flag) {
                    return DEFAULT_RIPPLE_COLOR;
                }
                tempElem.style.color = color;
                if (tempElem.style.color === flag || tempElem.style.color === '') {
                    return DEFAULT_RIPPLE_COLOR;
                } // color parse failed
                color = getComputedStyle(tempElem).color;
                document.body.removeChild(tempElem);
                return color;
            }
            // tslint:disable-next-line: no-shadowed-variable
            function rgbaToRgb(color) {
                color = color.match(/[.\d]+/g).map(function (a) { return +Number(a); });
                color.length = 3;
                return color;
            }
            if (color.toLowerCase() === 'transparent') {
                return DEFAULT_RIPPLE_COLOR;
            }
            if (color[0] === '#') {
                return hexToRgb(color);
            }
            if (color.indexOf('rgb') === -1) {
                color = namedColorsToRgba(color);
            }
            if (color.indexOf('rgb') === 0) {
                return rgbaToRgb(color);
            }
            return DEFAULT_RIPPLE_COLOR;
        };
        return MdbRippleDirective;
    }());
    MdbRippleDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbRipple]',
                    exportAs: 'mdbRipple',
                },] }
    ];
    MdbRippleDirective.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 }
    ]; };
    MdbRippleDirective.propDecorators = {
        rippleCentered: [{ type: core.Input }],
        rippleColor: [{ type: core.Input }],
        rippleDuration: [{ type: core.Input }],
        rippleRadius: [{ type: core.Input }],
        rippleUnbound: [{ type: core.Input }],
        ripple: [{ type: core.HostBinding, args: ['class.ripple-surface',] }],
        _createRipple: [{ type: core.HostListener, args: ['click', ['$event'],] }]
    };

    var MdbRippleModule = /** @class */ (function () {
        function MdbRippleModule() {
        }
        return MdbRippleModule;
    }());
    MdbRippleModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [MdbRippleDirective],
                    imports: [],
                    exports: [MdbRippleDirective],
                },] }
    ];

    var defaultIdNumber = 0;
    // tslint:disable-next-line:component-class-suffix
    var MdbErrorDirective = /** @class */ (function () {
        function MdbErrorDirective(_elementRef, renderer) {
            this._elementRef = _elementRef;
            this.renderer = renderer;
            this.id = "mdb-error-" + defaultIdNumber++;
            this.errorMsg = true;
            this.messageId = this.id;
            this._destroy$ = new rxjs.Subject();
        }
        MdbErrorDirective.prototype._getClosestEl = function (el, selector) {
            for (; el && el !== document; el = el.parentNode) {
                if (el.matches && el.matches(selector)) {
                    return el;
                }
            }
            return null;
        };
        MdbErrorDirective.prototype.ngOnInit = function () {
            var _this = this;
            var textarea = this._getClosestEl(this._elementRef.nativeElement, 'textarea');
            if (textarea) {
                var height_1 = textarea.offsetHeight + 4 + 'px';
                this.renderer.setStyle(this._elementRef.nativeElement, 'top', height_1);
                rxjs.fromEvent(textarea, 'keyup')
                    .pipe(operators.takeUntil(this._destroy$))
                    .subscribe(function () {
                    height_1 = textarea.offsetHeight + 4 + 'px';
                    _this.renderer.setStyle(_this._elementRef.nativeElement, 'top', height_1);
                });
            }
        };
        MdbErrorDirective.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
        };
        return MdbErrorDirective;
    }());
    MdbErrorDirective.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-error',
                    template: '<ng-content></ng-content>'
                },] }
    ];
    MdbErrorDirective.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 }
    ]; };
    MdbErrorDirective.propDecorators = {
        id: [{ type: core.Input }],
        errorMsg: [{ type: core.HostBinding, args: ['class.error-message',] }],
        messageId: [{ type: core.HostBinding, args: ['attr.id',] }]
    };

    var defaultIdNumber$1 = 0;
    // tslint:disable-next-line:component-class-suffix
    var MdbSuccessDirective = /** @class */ (function () {
        function MdbSuccessDirective(_elementRef, renderer) {
            this._elementRef = _elementRef;
            this.renderer = renderer;
            this.id = "mdb-success-" + defaultIdNumber$1++;
            this.successMsg = true;
            this.messageId = this.id;
            this._destroy$ = new rxjs.Subject();
        }
        MdbSuccessDirective.prototype._getClosestEl = function (el, selector) {
            for (; el && el !== document; el = el.parentNode) {
                if (el.matches && el.matches(selector)) {
                    return el;
                }
            }
            return null;
        };
        MdbSuccessDirective.prototype.ngOnInit = function () {
            var _this = this;
            var textarea = this._getClosestEl(this._elementRef.nativeElement, 'textarea');
            if (textarea) {
                var height_1 = textarea.offsetHeight + 4 + 'px';
                this.renderer.setStyle(this._elementRef.nativeElement, 'top', height_1);
                rxjs.fromEvent(textarea, 'keyup')
                    .pipe(operators.takeUntil(this._destroy$))
                    .subscribe(function () {
                    height_1 = textarea.offsetHeight + 4 + 'px';
                    _this.renderer.setStyle(_this._elementRef.nativeElement, 'top', height_1);
                });
            }
        };
        MdbSuccessDirective.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
        };
        return MdbSuccessDirective;
    }());
    MdbSuccessDirective.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-success',
                    template: '<ng-content></ng-content>'
                },] }
    ];
    MdbSuccessDirective.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 }
    ]; };
    MdbSuccessDirective.propDecorators = {
        id: [{ type: core.Input }],
        successMsg: [{ type: core.HostBinding, args: ['class.success-message',] }],
        messageId: [{ type: core.HostBinding, args: ['attr.id',] }]
    };

    var MdbValidateDirective = /** @class */ (function () {
        function MdbValidateDirective(renderer, _elementRef) {
            this.renderer = renderer;
            this._elementRef = _elementRef;
            this._validate = true;
            this._validateSuccess = true;
            this._validateError = true;
        }
        Object.defineProperty(MdbValidateDirective.prototype, "validate", {
            get: function () {
                return this._validate;
            },
            set: function (value) {
                this._validate = value;
                this.updateErrorClass();
                this.updateSuccessClass();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbValidateDirective.prototype, "validateSuccess", {
            get: function () {
                return this._validateSuccess;
            },
            set: function (value) {
                this._validateSuccess = value;
                this.updateSuccessClass();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbValidateDirective.prototype, "validateError", {
            get: function () {
                return this._validateError;
            },
            set: function (value) {
                this._validateError = value;
                this.updateErrorClass();
                this.updateSuccessClass();
            },
            enumerable: false,
            configurable: true
        });
        MdbValidateDirective.prototype.updateSuccessClass = function () {
            if (this.validate && this.validateSuccess) {
                this.renderer.addClass(this._elementRef.nativeElement, 'validate-success');
            }
            else {
                this.renderer.removeClass(this._elementRef.nativeElement, 'validate-success');
            }
        };
        MdbValidateDirective.prototype.updateErrorClass = function () {
            if (this.validate && this.validateError) {
                this.renderer.addClass(this._elementRef.nativeElement, 'validate-error');
            }
            else {
                this.renderer.removeClass(this._elementRef.nativeElement, 'validate-error');
            }
        };
        MdbValidateDirective.prototype.ngOnInit = function () {
            this.updateSuccessClass();
            this.updateErrorClass();
        };
        return MdbValidateDirective;
    }());
    MdbValidateDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbValidate]',
                },] }
    ];
    MdbValidateDirective.ctorParameters = function () { return [
        { type: core.Renderer2 },
        { type: core.ElementRef }
    ]; };
    MdbValidateDirective.propDecorators = {
        mdbValidate: [{ type: core.Input }],
        validate: [{ type: core.Input }],
        validateSuccess: [{ type: core.Input }],
        validateError: [{ type: core.Input }]
    };

    var MdbValidationModule = /** @class */ (function () {
        function MdbValidationModule() {
        }
        return MdbValidationModule;
    }());
    MdbValidationModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [common.CommonModule],
                    declarations: [MdbErrorDirective, MdbSuccessDirective, MdbValidateDirective],
                    exports: [MdbErrorDirective, MdbSuccessDirective, MdbValidateDirective],
                },] }
    ];

    var MdbScrollspyLinkDirective = /** @class */ (function () {
        function MdbScrollspyLinkDirective(cdRef, document) {
            this.cdRef = cdRef;
            this.document = document;
            this._scrollIntoView = true;
            this.scrollspyLink = true;
            this.active = false;
        }
        Object.defineProperty(MdbScrollspyLinkDirective.prototype, "scrollIntoView", {
            get: function () {
                return this._scrollIntoView;
            },
            set: function (value) {
                this._scrollIntoView = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbScrollspyLinkDirective.prototype, "section", {
            get: function () {
                return this._section;
            },
            set: function (value) {
                if (value) {
                    this._section = value;
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbScrollspyLinkDirective.prototype, "id", {
            get: function () {
                return this._id;
            },
            set: function (newId) {
                if (newId) {
                    this._id = newId;
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbScrollspyLinkDirective.prototype.onClick = function () {
            if (this.section && this.scrollIntoView === true) {
                this.section.scrollIntoView();
            }
        };
        MdbScrollspyLinkDirective.prototype.detectChanges = function () {
            this.cdRef.detectChanges();
        };
        MdbScrollspyLinkDirective.prototype.assignSectionToId = function () {
            this.section = this.document.documentElement.querySelector("#" + this.id);
        };
        MdbScrollspyLinkDirective.prototype.ngOnInit = function () {
            this.assignSectionToId();
        };
        return MdbScrollspyLinkDirective;
    }());
    MdbScrollspyLinkDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbScrollspyLink]',
                },] }
    ];
    MdbScrollspyLinkDirective.ctorParameters = function () { return [
        { type: core.ChangeDetectorRef },
        { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
    ]; };
    MdbScrollspyLinkDirective.propDecorators = {
        scrollIntoView: [{ type: core.Input }],
        id: [{ type: core.Input, args: ['mdbScrollspyLink',] }],
        scrollspyLink: [{ type: core.HostBinding, args: ['class.scrollspy-link',] }],
        active: [{ type: core.HostBinding, args: ['class.active',] }],
        onClick: [{ type: core.HostListener, args: ['click', [],] }]
    };

    var MdbScrollspyService = /** @class */ (function () {
        function MdbScrollspyService() {
            this.scrollSpys = [];
            this.activeSubject = new rxjs.Subject();
            this.active$ = this.activeSubject;
        }
        MdbScrollspyService.prototype.addScrollspy = function (scrollSpy) {
            this.scrollSpys.push(scrollSpy);
        };
        MdbScrollspyService.prototype.removeScrollspy = function (scrollSpyId) {
            var scrollSpyIndex = this.scrollSpys.findIndex(function (spy) {
                return spy.id === scrollSpyId;
            });
            this.scrollSpys.splice(scrollSpyIndex, 1);
        };
        MdbScrollspyService.prototype.updateActiveState = function (scrollSpyId, activeLinkId) {
            var scrollSpy = this.scrollSpys.find(function (spy) {
                return spy.id === scrollSpyId;
            });
            if (!scrollSpy) {
                return;
            }
            var activeLink = scrollSpy.links.find(function (link) {
                return link.id === activeLinkId;
            });
            this.setActiveLink(activeLink);
        };
        MdbScrollspyService.prototype.removeActiveState = function (scrollSpyId, activeLinkId) {
            var scrollSpy = this.scrollSpys.find(function (spy) {
                return spy.id === scrollSpyId;
            });
            if (!scrollSpy) {
                return;
            }
            var activeLink = scrollSpy.links.find(function (link) {
                return link.id === activeLinkId;
            });
            if (!activeLink) {
                return;
            }
            activeLink.active = false;
            activeLink.detectChanges();
        };
        MdbScrollspyService.prototype.setActiveLink = function (activeLink) {
            if (activeLink) {
                activeLink.active = true;
                activeLink.detectChanges();
                this.activeSubject.next(activeLink);
            }
        };
        MdbScrollspyService.prototype.removeActiveLinks = function (scrollSpyId) {
            var scrollSpy = this.scrollSpys.find(function (spy) {
                return spy.id === scrollSpyId;
            });
            if (!scrollSpy) {
                return;
            }
            scrollSpy.links.forEach(function (link) {
                link.active = false;
                link.detectChanges();
            });
        };
        return MdbScrollspyService;
    }());
    MdbScrollspyService.decorators = [
        { type: core.Injectable }
    ];

    // tslint:disable-next-line:component-class-suffix
    var MdbScrollspyDirective = /** @class */ (function () {
        function MdbScrollspyDirective(scrollSpyService) {
            this.scrollSpyService = scrollSpyService;
            this._destroy$ = new rxjs.Subject();
            this.activeLinkChange = new core.EventEmitter();
        }
        Object.defineProperty(MdbScrollspyDirective.prototype, "id", {
            get: function () {
                return this._id;
            },
            set: function (newId) {
                if (newId) {
                    this._id = newId;
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbScrollspyDirective.prototype.ngOnInit = function () {
            var _this = this;
            this.activeSub = this.scrollSpyService.active$
                .pipe(operators.takeUntil(this._destroy$), operators.distinctUntilChanged())
                .subscribe(function (activeLink) {
                _this.activeLinkChange.emit(activeLink);
            });
        };
        MdbScrollspyDirective.prototype.ngAfterContentInit = function () {
            this.scrollSpyService.addScrollspy({ id: this.id, links: this.links });
        };
        MdbScrollspyDirective.prototype.ngOnDestroy = function () {
            this.scrollSpyService.removeScrollspy(this.id);
            this._destroy$.next();
            this._destroy$.complete();
        };
        return MdbScrollspyDirective;
    }());
    MdbScrollspyDirective.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line:component-selector
                    selector: '[mdbScrollspy]',
                    template: '<ng-content></ng-content>'
                },] }
    ];
    MdbScrollspyDirective.ctorParameters = function () { return [
        { type: MdbScrollspyService }
    ]; };
    MdbScrollspyDirective.propDecorators = {
        links: [{ type: core.ContentChildren, args: [MdbScrollspyLinkDirective, { descendants: true },] }],
        id: [{ type: core.Input, args: ['mdbScrollspy',] }],
        activeLinkChange: [{ type: core.Output }]
    };

    // tslint:disable-next-line: directive-class-suffix
    var MdbScrollspyElementDirective = /** @class */ (function () {
        function MdbScrollspyElementDirective(_elementRef, renderer, ngZone, scrollSpyService) {
            this._elementRef = _elementRef;
            this.renderer = renderer;
            this.ngZone = ngZone;
            this.scrollSpyService = scrollSpyService;
            this.offset = 0;
        }
        Object.defineProperty(MdbScrollspyElementDirective.prototype, "host", {
            get: function () {
                return this._elementRef.nativeElement;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbScrollspyElementDirective.prototype, "scrollSpyId", {
            get: function () {
                return this._scrollSpyId;
            },
            set: function (newId) {
                if (newId) {
                    this._scrollSpyId = newId;
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbScrollspyElementDirective.prototype.isElementInViewport = function () {
            var scrollTop = this.container.scrollTop;
            var elTop = this.host.offsetTop - this.offset;
            var elHeight = this.host.offsetHeight;
            return scrollTop >= elTop && scrollTop < elTop + elHeight;
        };
        MdbScrollspyElementDirective.prototype.updateActiveState = function (scrollSpyId, id) {
            if (this.isElementInViewport()) {
                this.scrollSpyService.removeActiveLinks(scrollSpyId);
                this.scrollSpyService.updateActiveState(scrollSpyId, id);
            }
        };
        MdbScrollspyElementDirective.prototype.onScroll = function () {
            this.updateActiveState(this.scrollSpyId, this.id);
        };
        MdbScrollspyElementDirective.prototype.listenToScroll = function () {
            var _this = this;
            this.renderer.listen(this.container, 'scroll', function () {
                _this.onScroll();
            });
        };
        MdbScrollspyElementDirective.prototype.ngOnInit = function () {
            this.id = this.host.id;
            if (!this.container) {
                this.container = this._getClosestEl(this.host, '.scrollspy-container');
            }
            this.renderer.setStyle(this.container, 'position', 'relative');
            this.ngZone.runOutsideAngular(this.listenToScroll.bind(this));
        };
        MdbScrollspyElementDirective.prototype.ngAfterViewInit = function () {
            var _this = this;
            setTimeout(function () {
                _this.updateActiveState(_this.scrollSpyId, _this.id);
            }, 0);
        };
        MdbScrollspyElementDirective.prototype._getClosestEl = function (el, selector) {
            for (; el && el !== document; el = el.parentNode) {
                if (el.matches && el.matches(selector)) {
                    return el;
                }
            }
            return null;
        };
        return MdbScrollspyElementDirective;
    }());
    MdbScrollspyElementDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbScrollspyElement]',
                },] }
    ];
    MdbScrollspyElementDirective.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 },
        { type: core.NgZone },
        { type: MdbScrollspyService }
    ]; };
    MdbScrollspyElementDirective.propDecorators = {
        container: [{ type: core.Input }],
        scrollSpyId: [{ type: core.Input, args: ['mdbScrollspyElement',] }],
        offset: [{ type: core.Input }]
    };

    var MdbScrollspyWindowDirective = /** @class */ (function () {
        function MdbScrollspyWindowDirective(document, el, renderer, ngZone, scrollSpyService) {
            this.document = document;
            this.el = el;
            this.renderer = renderer;
            this.ngZone = ngZone;
            this.scrollSpyService = scrollSpyService;
            this.offset = 0;
        }
        Object.defineProperty(MdbScrollspyWindowDirective.prototype, "scrollSpyId", {
            get: function () {
                return this._scrollSpyId;
            },
            set: function (newId) {
                if (newId) {
                    this._scrollSpyId = newId;
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbScrollspyWindowDirective.prototype.isElementInViewport = function () {
            var scrollTop = this.document.documentElement.scrollTop || this.document.body.scrollTop;
            var elHeight = this.el.nativeElement.offsetHeight;
            var elTop = this.el.nativeElement.offsetTop - this.offset;
            var elBottom = elTop + elHeight;
            return scrollTop >= elTop && scrollTop <= elBottom;
        };
        MdbScrollspyWindowDirective.prototype.updateActiveState = function (scrollSpyId, id) {
            if (this.isElementInViewport()) {
                this.scrollSpyService.updateActiveState(scrollSpyId, id);
            }
            else {
                this.scrollSpyService.removeActiveState(scrollSpyId, id);
            }
        };
        MdbScrollspyWindowDirective.prototype.onScroll = function () {
            this.updateActiveState(this.scrollSpyId, this.id);
        };
        MdbScrollspyWindowDirective.prototype.listenToScroll = function () {
            var _this = this;
            this.renderer.listen(window, 'scroll', function () {
                _this.onScroll();
            });
        };
        MdbScrollspyWindowDirective.prototype.ngOnInit = function () {
            this.id = this.el.nativeElement.id;
            this.ngZone.runOutsideAngular(this.listenToScroll.bind(this));
        };
        MdbScrollspyWindowDirective.prototype.ngAfterViewInit = function () {
            var _this = this;
            setTimeout(function () {
                _this.updateActiveState(_this.scrollSpyId, _this.id);
            }, 0);
        };
        return MdbScrollspyWindowDirective;
    }());
    MdbScrollspyWindowDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbScrollspyWindow]',
                },] }
    ];
    MdbScrollspyWindowDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
        { type: core.ElementRef },
        { type: core.Renderer2 },
        { type: core.NgZone },
        { type: MdbScrollspyService }
    ]; };
    MdbScrollspyWindowDirective.propDecorators = {
        scrollSpyId: [{ type: core.Input, args: ['mdbScrollspyWindow',] }],
        offset: [{ type: core.Input }]
    };

    var MdbScrollspyModule = /** @class */ (function () {
        function MdbScrollspyModule() {
        }
        return MdbScrollspyModule;
    }());
    MdbScrollspyModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [
                        MdbScrollspyDirective,
                        MdbScrollspyLinkDirective,
                        MdbScrollspyElementDirective,
                        MdbScrollspyWindowDirective,
                    ],
                    exports: [
                        MdbScrollspyDirective,
                        MdbScrollspyLinkDirective,
                        MdbScrollspyElementDirective,
                        MdbScrollspyWindowDirective,
                    ],
                    providers: [MdbScrollspyService],
                },] }
    ];

    var RANGE_VALUE_ACCESOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        // tslint:disable-next-line: no-use-before-declare
        useExisting: core.forwardRef(function () { return MdbRangeComponent; }),
        multi: true,
    };
    var MdbRangeComponent = /** @class */ (function () {
        function MdbRangeComponent(_cdRef) {
            this._cdRef = _cdRef;
            this.min = 0;
            this.max = 100;
            this.rangeValueChange = new core.EventEmitter();
            this.visibility = false;
            // Control Value Accessor Methods
            this.onChange = function (_) { };
            this.onTouched = function () { };
        }
        MdbRangeComponent.prototype.onchange = function (event) {
            this.onChange(event.target.value);
        };
        MdbRangeComponent.prototype.onInput = function () {
            this.rangeValueChange.emit({ value: this.value });
            this.focusRangeInput();
        };
        MdbRangeComponent.prototype.ngAfterViewInit = function () {
            this.thumbPositionUpdate();
        };
        MdbRangeComponent.prototype.focusRangeInput = function () {
            this.input.nativeElement.focus();
            this.visibility = true;
        };
        MdbRangeComponent.prototype.blurRangeInput = function () {
            this.input.nativeElement.blur();
            this.visibility = false;
        };
        MdbRangeComponent.prototype.thumbPositionUpdate = function () {
            var rangeInput = this.input.nativeElement;
            var inputValue = rangeInput.value;
            var minValue = rangeInput.min ? rangeInput.min : 0;
            var maxValue = rangeInput.max ? rangeInput.max : 100;
            var newValue = Number(((inputValue - minValue) * 100) / (maxValue - minValue));
            this.value = inputValue;
            this.thumbStyle = { left: "calc(" + newValue + "% + (" + (8 - newValue * 0.15) + "px))" };
        };
        MdbRangeComponent.prototype.writeValue = function (value) {
            var _this = this;
            this.value = value;
            this._cdRef.markForCheck();
            setTimeout(function () {
                _this.thumbPositionUpdate();
            }, 0);
        };
        MdbRangeComponent.prototype.registerOnChange = function (fn) {
            this.onChange = fn;
        };
        MdbRangeComponent.prototype.registerOnTouched = function (fn) {
            this.onTouched = fn;
        };
        MdbRangeComponent.prototype.setDisabledState = function (isDisabled) {
            this.disabled = isDisabled;
        };
        return MdbRangeComponent;
    }());
    MdbRangeComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-range',
                    template: "<label for=\"customRange2\" class=\"form-label\">Example range</label>\n<div class=\"range\">\n  <input\n    #input\n    [name]=\"name\"\n    type=\"range\"\n    [disabled]=\"disabled\"\n    [id]=\"id\"\n    [min]=\"min\"\n    [max]=\"max\"\n    [step]=\"step\"\n    [value]=\"value\"\n    [(ngModel)]=\"value\"\n    class=\"form-range\"\n    min=\"0\"\n    max=\"5\"\n    [id]=\"id\"\n    (input)=\"thumbPositionUpdate()\"\n    (blur)=\"blurRangeInput()\"\n    (mousedown)=\"focusRangeInput()\"\n    (mouseup)=\"blurRangeInput()\"\n    (touchstart)=\"focusRangeInput()\"\n    (touchend)=\"blurRangeInput()\"\n  />\n  <span #thumb class=\"thumb\" [ngStyle]=\"thumbStyle\" [ngClass]=\"{ 'thumb-active': this.visibility }\">\n    <span #thumbValue class=\"thumb-value\">{{ value }}</span>\n  </span>\n</div>\n",
                    changeDetection: core.ChangeDetectionStrategy.OnPush,
                    providers: [RANGE_VALUE_ACCESOR]
                },] }
    ];
    MdbRangeComponent.ctorParameters = function () { return [
        { type: core.ChangeDetectorRef }
    ]; };
    MdbRangeComponent.propDecorators = {
        input: [{ type: core.ViewChild, args: ['input',] }],
        thumb: [{ type: core.ViewChild, args: ['thumb',] }],
        thumbValue: [{ type: core.ViewChild, args: ['thumbValue',] }],
        id: [{ type: core.Input }],
        required: [{ type: core.Input }],
        name: [{ type: core.Input }],
        value: [{ type: core.Input }],
        disabled: [{ type: core.Input }],
        min: [{ type: core.Input }],
        max: [{ type: core.Input }],
        step: [{ type: core.Input }],
        default: [{ type: core.Input }],
        defaultRangeCounterClass: [{ type: core.Input }],
        rangeValueChange: [{ type: core.Output }],
        onchange: [{ type: core.HostListener, args: ['change', ['$event'],] }],
        onInput: [{ type: core.HostListener, args: ['input',] }]
    };

    var MdbRangeModule = /** @class */ (function () {
        function MdbRangeModule() {
        }
        return MdbRangeModule;
    }());
    MdbRangeModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [common.CommonModule, forms.FormsModule],
                    declarations: [MdbRangeComponent],
                    exports: [MdbRangeComponent],
                },] }
    ];

    var MDB_TAB_CONTENT = new core.InjectionToken('MdbTabContentDirective');
    var MdbTabContentDirective = /** @class */ (function () {
        function MdbTabContentDirective(template) {
            this.template = template;
        }
        return MdbTabContentDirective;
    }());
    MdbTabContentDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbTabContent]',
                    providers: [{ provide: MDB_TAB_CONTENT, useExisting: MdbTabContentDirective }],
                },] }
    ];
    MdbTabContentDirective.ctorParameters = function () { return [
        { type: core.TemplateRef }
    ]; };

    var MDB_TAB_TITLE = new core.InjectionToken('MdbTabTitleDirective');
    var MdbTabTitleDirective = /** @class */ (function () {
        function MdbTabTitleDirective(template) {
            this.template = template;
        }
        return MdbTabTitleDirective;
    }());
    MdbTabTitleDirective.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbTabTitle]',
                    providers: [{ provide: MDB_TAB_TITLE, useExisting: MdbTabTitleDirective }],
                },] }
    ];
    MdbTabTitleDirective.ctorParameters = function () { return [
        { type: core.TemplateRef }
    ]; };

    var MdbTabComponent = /** @class */ (function () {
        function MdbTabComponent(_elementRef, _renderer, _vcr) {
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this._vcr = _vcr;
            this.activeStateChange$ = new rxjs.Subject();
            this.disabled = false;
            this._contentPortal = null;
            this._titlePortal = null;
            this._active = false;
        }
        Object.defineProperty(MdbTabComponent.prototype, "active", {
            get: function () {
                return this._active;
            },
            // tslint:disable-next-line: adjacent-overload-signatures
            set: function (value) {
                if (value) {
                    this._renderer.addClass(this._elementRef.nativeElement, 'show');
                    this._renderer.addClass(this._elementRef.nativeElement, 'active');
                }
                else {
                    this._renderer.removeClass(this._elementRef.nativeElement, 'show');
                    this._renderer.removeClass(this._elementRef.nativeElement, 'active');
                }
                this._active = value;
                this.activeStateChange$.next(value);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbTabComponent.prototype, "content", {
            get: function () {
                return this._contentPortal;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbTabComponent.prototype, "titleContent", {
            get: function () {
                return this._titlePortal;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbTabComponent.prototype, "shouldAttach", {
            get: function () {
                return this._lazyContent === undefined;
            },
            enumerable: false,
            configurable: true
        });
        MdbTabComponent.prototype.ngOnInit = function () {
            this._createContentPortal();
            if (this._titleContent) {
                this._createTitlePortal();
            }
        };
        MdbTabComponent.prototype._createContentPortal = function () {
            var content = this._lazyContent || this._content;
            this._contentPortal = new portal.TemplatePortal(content, this._vcr);
        };
        MdbTabComponent.prototype._createTitlePortal = function () {
            this._titlePortal = new portal.TemplatePortal(this._titleContent, this._vcr);
        };
        return MdbTabComponent;
    }());
    MdbTabComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-tab',
                    template: "<ng-template><ng-content></ng-content></ng-template>\n"
                },] }
    ];
    MdbTabComponent.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.Renderer2 },
        { type: core.ViewContainerRef }
    ]; };
    MdbTabComponent.propDecorators = {
        _lazyContent: [{ type: core.ContentChild, args: [MDB_TAB_CONTENT, { read: core.TemplateRef, static: true },] }],
        _titleContent: [{ type: core.ContentChild, args: [MDB_TAB_TITLE, { read: core.TemplateRef, static: true },] }],
        _content: [{ type: core.ViewChild, args: [core.TemplateRef, { static: true },] }],
        disabled: [{ type: core.Input }],
        title: [{ type: core.Input }]
    };

    var MdbTabChange = /** @class */ (function () {
        function MdbTabChange() {
        }
        return MdbTabChange;
    }());
    var MdbTabsComponent = /** @class */ (function () {
        function MdbTabsComponent() {
            this._destroy$ = new rxjs.Subject();
            this.fill = false;
            this.justified = false;
            this.pills = false;
            this.vertical = false;
            this.activeTabChange = new core.EventEmitter();
        }
        MdbTabsComponent.prototype.ngAfterContentInit = function () {
            var _this = this;
            var firstActiveTabIndex = this.tabs.toArray().findIndex(function (tab) { return !tab.disabled; });
            this.setActiveTab(firstActiveTabIndex);
            // tslint:disable-next-line: deprecation
            this.tabs.changes.pipe(operators.takeUntil(this._destroy$)).subscribe(function () {
                var hasActiveTab = _this.tabs.find(function (tab) { return tab.active; });
                if (!hasActiveTab) {
                    var closestTabIndex = _this._getClosestTabIndex(_this._selectedIndex);
                    if (closestTabIndex !== -1) {
                        _this.setActiveTab(closestTabIndex);
                    }
                }
            });
        };
        MdbTabsComponent.prototype.setActiveTab = function (index) {
            var activeTab = this.tabs.toArray()[index];
            if (!activeTab || (activeTab && activeTab.disabled)) {
                return;
            }
            this.tabs.forEach(function (tab) { return (tab.active = tab === activeTab); });
            this._selectedIndex = index;
            var tabChangeEvent = this._getTabChangeEvent(index, activeTab);
            this.activeTabChange.emit(tabChangeEvent);
        };
        MdbTabsComponent.prototype._getTabChangeEvent = function (index, tab) {
            var event = new MdbTabChange();
            event.index = index;
            event.tab = tab;
            return event;
        };
        MdbTabsComponent.prototype._getClosestTabIndex = function (index) {
            var tabs = this.tabs.toArray();
            var tabsLength = tabs.length;
            if (!tabsLength) {
                return -1;
            }
            for (var i = 1; i <= tabsLength; i += 1) {
                var prevIndex = index - i;
                var nextIndex = index + i;
                if (tabs[prevIndex] && !tabs[prevIndex].disabled) {
                    return prevIndex;
                }
                if (tabs[nextIndex] && !tabs[nextIndex].disabled) {
                    return nextIndex;
                }
            }
            return -1;
        };
        MdbTabsComponent.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
        };
        return MdbTabsComponent;
    }());
    MdbTabsComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-tabs',
                    template: "<ul\n  class=\"nav mb-3 col-3 flex-column\"\n  [ngClass]=\"{\n    'nav-pills': pills,\n    'nav-tabs': !pills,\n    'nav-fill': fill,\n    'nav-justified': justified,\n    'flex-column': vertical,\n    'col-3': vertical,\n    'text-center': vertical\n  }\"\n  role=\"tablist\"\n>\n  <li\n    *ngFor=\"let tab of tabs; let i = index\"\n    (click)=\"setActiveTab(i)\"\n    class=\"nav-item\"\n    role=\"presentation\"\n  >\n    <a\n      href=\"javascript:void(0)\"\n      class=\"nav-link\"\n      [class.active]=\"tab.active\"\n      [class.disabled]=\"tab.disabled\"\n      role=\"tab\"\n    >\n      <ng-template [ngIf]=\"tab.titleContent\">\n        <ng-template [cdkPortalOutlet]=\"tab.titleContent\"></ng-template>\n      </ng-template>\n\n      <ng-template [ngIf]=\"!tab.titleContent\">{{ tab.title }}</ng-template>\n    </a>\n  </li>\n</ul>\n\n<div\n  class=\"tab-content\"\n  [ngClass]=\"{\n    'col-9': vertical\n  }\"\n>\n  <!-- <ng-content select=\"mdb-tab\"></ng-content> -->\n  <ng-container *ngFor=\"let tab of tabs\">\n    <div\n      class=\"tab-pane fade\"\n      [ngClass]=\"{\n        show: tab.active,\n        active: tab.active\n      }\"\n    >\n      <ng-template mdbTabPortalOutlet [tab]=\"tab\"></ng-template>\n    </div>\n  </ng-container>\n</div>\n"
                },] }
    ];
    MdbTabsComponent.ctorParameters = function () { return []; };
    MdbTabsComponent.propDecorators = {
        tabs: [{ type: core.ContentChildren, args: [MdbTabComponent,] }],
        fill: [{ type: core.Input }],
        justified: [{ type: core.Input }],
        pills: [{ type: core.Input }],
        vertical: [{ type: core.HostBinding, args: ['class.row',] }, { type: core.Input }],
        activeTabChange: [{ type: core.Output }]
    };

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (Object.prototype.hasOwnProperty.call(b, p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, o) {
        for (var p in m)
            if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p))
                __createBinding(o, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    ;
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }
    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    // tslint:disable-next-line: directive-class-suffix
    var MdbTabPortalOutlet = /** @class */ (function (_super) {
        __extends(MdbTabPortalOutlet, _super);
        function MdbTabPortalOutlet(_cfr, _vcr, _document) {
            var _this = _super.call(this, _cfr, _vcr, _document) || this;
            _this._destroy$ = new rxjs.Subject();
            return _this;
        }
        MdbTabPortalOutlet.prototype.ngOnInit = function () {
            var _this = this;
            _super.prototype.ngOnInit.call(this);
            if ((this.tab.shouldAttach || this.tab.active) && !this.hasAttached()) {
                this.attach(this.tab.content);
            }
            else {
                // tslint:disable-next-line: deprecation
                this.tab.activeStateChange$.pipe(operators.takeUntil(this._destroy$)).subscribe(function (isActive) {
                    if (isActive && !_this.hasAttached()) {
                        _this.attach(_this.tab.content);
                    }
                });
            }
        };
        MdbTabPortalOutlet.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
            _super.prototype.ngOnDestroy.call(this);
        };
        return MdbTabPortalOutlet;
    }(portal.CdkPortalOutlet));
    MdbTabPortalOutlet.decorators = [
        { type: core.Directive, args: [{
                    // tslint:disable-next-line: directive-selector
                    selector: '[mdbTabPortalOutlet]',
                },] }
    ];
    MdbTabPortalOutlet.ctorParameters = function () { return [
        { type: core.ComponentFactoryResolver },
        { type: core.ViewContainerRef },
        { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
    ]; };
    MdbTabPortalOutlet.propDecorators = {
        tab: [{ type: core.Input }]
    };

    var MdbTabsModule = /** @class */ (function () {
        function MdbTabsModule() {
        }
        return MdbTabsModule;
    }());
    MdbTabsModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [
                        MdbTabComponent,
                        MdbTabContentDirective,
                        MdbTabTitleDirective,
                        MdbTabPortalOutlet,
                        MdbTabsComponent,
                    ],
                    imports: [common.CommonModule, portal.PortalModule],
                    exports: [
                        MdbTabComponent,
                        MdbTabContentDirective,
                        MdbTabTitleDirective,
                        MdbTabPortalOutlet,
                        MdbTabsComponent,
                    ],
                },] }
    ];

    var MdbCarouselItemComponent = /** @class */ (function () {
        function MdbCarouselItemComponent(_elementRef) {
            this._elementRef = _elementRef;
            this.interval = null;
            this.carouselItem = true;
            this.active = false;
            this.next = false;
            this.prev = false;
            this.start = false;
            this.end = false;
        }
        Object.defineProperty(MdbCarouselItemComponent.prototype, "host", {
            get: function () {
                return this._elementRef.nativeElement;
            },
            enumerable: false,
            configurable: true
        });
        MdbCarouselItemComponent.prototype.ngOnInit = function () { };
        return MdbCarouselItemComponent;
    }());
    MdbCarouselItemComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-carousel-item',
                    template: '<ng-content></ng-content>'
                },] }
    ];
    MdbCarouselItemComponent.ctorParameters = function () { return [
        { type: core.ElementRef }
    ]; };
    MdbCarouselItemComponent.propDecorators = {
        interval: [{ type: core.Input }],
        carouselItem: [{ type: core.HostBinding, args: ['class.carousel-item',] }],
        active: [{ type: core.HostBinding, args: ['class.active',] }],
        next: [{ type: core.HostBinding, args: ['class.carousel-item-next',] }],
        prev: [{ type: core.HostBinding, args: ['class.carousel-item-prev',] }],
        start: [{ type: core.HostBinding, args: ['class.carousel-item-start',] }],
        end: [{ type: core.HostBinding, args: ['class.carousel-item-end',] }]
    };

    var Direction;
    (function (Direction) {
        Direction[Direction["UNKNOWN"] = 0] = "UNKNOWN";
        Direction[Direction["NEXT"] = 1] = "NEXT";
        Direction[Direction["PREV"] = 2] = "PREV";
    })(Direction || (Direction = {}));
    var MdbCarouselComponent = /** @class */ (function () {
        function MdbCarouselComponent(_elementRef, _cdRef) {
            this._elementRef = _elementRef;
            this._cdRef = _cdRef;
            this.animation = 'slide';
            this.controls = false;
            this.dark = false;
            this.indicators = false;
            this.ride = true;
            this._interval = 5000;
            this.keyboard = true;
            this.pause = true;
            this.wrap = true;
            this.slide = new core.EventEmitter();
            this.slideChange = new core.EventEmitter();
            this._activeSlide = 0;
            this._isPlaying = false;
            this._isSliding = false;
            this._destroy$ = new rxjs.Subject();
        }
        Object.defineProperty(MdbCarouselComponent.prototype, "items", {
            get: function () {
                return this._items && this._items.toArray();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCarouselComponent.prototype, "interval", {
            get: function () {
                return this._interval;
            },
            set: function (value) {
                this._interval = value;
                if (this.items) {
                    this._restartInterval();
                }
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(MdbCarouselComponent.prototype, "activeSlide", {
            get: function () {
                return this._activeSlide;
            },
            set: function (index) {
                if (this.items.length && this._activeSlide !== index) {
                    this._activeSlide = index;
                    this._restartInterval();
                }
            },
            enumerable: false,
            configurable: true
        });
        MdbCarouselComponent.prototype.onMouseEnter = function () {
            if (this.pause && this._isPlaying) {
                this.stop();
            }
        };
        MdbCarouselComponent.prototype.onMouseLeave = function () {
            if (this.pause && !this._isPlaying) {
                this.play();
            }
        };
        MdbCarouselComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            Promise.resolve().then(function () {
                _this._setActiveSlide(_this._activeSlide);
                if (_this.interval > 0 && _this.ride) {
                    _this.play();
                }
            });
            if (this.keyboard) {
                rxjs.fromEvent(this._elementRef.nativeElement, 'keydown')
                    .pipe(operators.takeUntil(this._destroy$))
                    // tslint:disable-next-line: deprecation
                    .subscribe(function (event) {
                    if (event.key === 'ArrowRight') {
                        _this.next();
                    }
                    else if (event.key === 'ArrowLeft') {
                        _this.prev();
                    }
                });
            }
        };
        MdbCarouselComponent.prototype.ngOnDestroy = function () {
            this._destroy$.next();
            this._destroy$.complete();
        };
        MdbCarouselComponent.prototype._setActiveSlide = function (index) {
            var currentSlide = this.items[this._activeSlide];
            currentSlide.active = false;
            var newSlide = this.items[index];
            newSlide.active = true;
            this._activeSlide = index;
        };
        MdbCarouselComponent.prototype._restartInterval = function () {
            var _this = this;
            this._resetInterval();
            var activeElement = this.items[this.activeSlide];
            var interval = activeElement.interval ? activeElement.interval : this.interval;
            if (!isNaN(interval) && interval > 0) {
                this._lastInterval = setInterval(function () {
                    var nInterval = +interval;
                    if (_this._isPlaying && !isNaN(nInterval) && nInterval > 0) {
                        _this.next();
                    }
                    else {
                        _this.stop();
                    }
                }, interval);
            }
        };
        MdbCarouselComponent.prototype._resetInterval = function () {
            if (this._lastInterval) {
                clearInterval(this._lastInterval);
                this._lastInterval = null;
            }
        };
        MdbCarouselComponent.prototype.play = function () {
            if (!this._isPlaying) {
                this._isPlaying = true;
                this._restartInterval();
            }
        };
        MdbCarouselComponent.prototype.stop = function () {
            if (this._isPlaying) {
                this._isPlaying = false;
                this._resetInterval();
            }
        };
        MdbCarouselComponent.prototype.to = function (index) {
            if (index > this.items.length - 1 || index < 0) {
                return;
            }
            if (this.activeSlide === index) {
                this.stop();
                this.play();
                return;
            }
            var direction = index > this.activeSlide ? Direction.NEXT : Direction.PREV;
            this._animateSlides(direction, this.activeSlide, index);
            this.activeSlide = index;
            this._cdRef.markForCheck();
        };
        MdbCarouselComponent.prototype.next = function () {
            if (!this._isSliding) {
                this._slide(Direction.NEXT);
            }
            this._cdRef.markForCheck();
        };
        MdbCarouselComponent.prototype.prev = function () {
            if (!this._isSliding) {
                this._slide(Direction.PREV);
            }
            this._cdRef.markForCheck();
        };
        MdbCarouselComponent.prototype._slide = function (direction) {
            var isFirst = this._activeSlide === 0;
            var isLast = this._activeSlide === this.items.length - 1;
            if (!this.wrap) {
                if ((direction === Direction.NEXT && isLast) || (direction === Direction.PREV && isFirst)) {
                    return;
                }
            }
            var newSlideIndex = this._getNewSlideIndex(direction);
            this._animateSlides(direction, this.activeSlide, newSlideIndex);
            this.activeSlide = newSlideIndex;
            this.slide.emit();
        };
        MdbCarouselComponent.prototype._animateSlides = function (direction, currentIndex, nextIndex) {
            var _this = this;
            var currentItem = this.items[currentIndex];
            var nextItem = this.items[nextIndex];
            var currentEl = currentItem.host;
            var nextEl = nextItem.host;
            this._isSliding = true;
            if (this._isPlaying) {
                this.stop();
            }
            if (direction === Direction.NEXT) {
                nextItem.next = true;
                setTimeout(function () {
                    _this._reflow(nextEl);
                    currentItem.start = true;
                    nextItem.start = true;
                }, 0);
                var transitionDuration = 600;
                rxjs.fromEvent(currentEl, 'transitionend')
                    .pipe(operators.take(1))
                    // tslint:disable-next-line: deprecation
                    .subscribe(function () {
                    nextItem.next = false;
                    nextItem.start = false;
                    nextItem.active = true;
                    currentItem.active = false;
                    currentItem.start = false;
                    currentItem.next = false;
                    _this.slideChange.emit();
                    _this._isSliding = false;
                });
                this._emulateTransitionEnd(currentEl, transitionDuration);
            }
            else if (direction === Direction.PREV) {
                nextItem.prev = true;
                setTimeout(function () {
                    _this._reflow(nextEl);
                    currentItem.end = true;
                    nextItem.end = true;
                }, 0);
                var transitionDuration = 600;
                rxjs.fromEvent(currentEl, 'transitionend')
                    .pipe(operators.take(1))
                    // tslint:disable-next-line: deprecation
                    .subscribe(function () {
                    nextItem.prev = false;
                    nextItem.end = false;
                    nextItem.active = true;
                    currentItem.active = false;
                    currentItem.end = false;
                    currentItem.prev = false;
                    _this.slideChange.emit();
                    _this._isSliding = false;
                });
                this._emulateTransitionEnd(currentEl, transitionDuration);
            }
            if (!this._isPlaying && this.interval > 0) {
                this.play();
            }
        };
        MdbCarouselComponent.prototype._reflow = function (element) {
            return element.offsetHeight;
        };
        MdbCarouselComponent.prototype._emulateTransitionEnd = function (element, duration) {
            var eventEmitted = false;
            var durationPadding = 5;
            var emulatedDuration = duration + durationPadding;
            rxjs.fromEvent(element, 'transitionend')
                .pipe(operators.take(1))
                // tslint:disable-next-line: deprecation
                .subscribe(function () {
                eventEmitted = true;
            });
            setTimeout(function () {
                if (!eventEmitted) {
                    element.dispatchEvent(new Event('transitionend'));
                }
            }, emulatedDuration);
        };
        MdbCarouselComponent.prototype._getNewSlideIndex = function (direction) {
            var newSlideIndex;
            if (direction === Direction.NEXT) {
                newSlideIndex = this._getNextSlideIndex();
            }
            if (direction === Direction.PREV) {
                newSlideIndex = this._getPrevSlideIndex();
            }
            return newSlideIndex;
        };
        MdbCarouselComponent.prototype._getNextSlideIndex = function () {
            var isLast = this._activeSlide === this.items.length - 1;
            if (!isLast) {
                return this._activeSlide + 1;
            }
            else if (this.wrap && isLast) {
                return 0;
            }
            else {
                return this._activeSlide;
            }
        };
        MdbCarouselComponent.prototype._getPrevSlideIndex = function () {
            var isFirst = this._activeSlide === 0;
            if (!isFirst) {
                return this._activeSlide - 1;
            }
            else if (this.wrap && isFirst) {
                return this.items.length - 1;
            }
            else {
                return this._activeSlide;
            }
        };
        return MdbCarouselComponent;
    }());
    MdbCarouselComponent.decorators = [
        { type: core.Component, args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mdb-carousel',
                    template: "<div\n  class=\"carousel slide\"\n  [class.carousel-fade]=\"animation === 'fade'\"\n  [class.carousel-dark]=\"dark\"\n>\n  <div class=\"carousel-indicators\" *ngIf=\"indicators\">\n    <button\n      *ngFor=\"let item of items; let i = index\"\n      type=\"button\"\n      [class.active]=\"i === activeSlide\"\n      [attr.aria-current]=\"i === activeSlide\"\n      (click)=\"to(i)\"\n    ></button>\n  </div>\n\n  <div class=\"carousel-inner\">\n    <ng-content></ng-content>\n  </div>\n\n  <button *ngIf=\"controls\" class=\"carousel-control-prev\" type=\"button\" (click)=\"prev()\">\n    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n    <span class=\"visually-hidden\">Previous</span>\n  </button>\n  <button *ngIf=\"controls\" class=\"carousel-control-next\" type=\"button\" (click)=\"next()\">\n    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n    <span class=\"visually-hidden\">Next</span>\n  </button>\n</div>\n",
                    changeDetection: core.ChangeDetectionStrategy.OnPush
                },] }
    ];
    MdbCarouselComponent.ctorParameters = function () { return [
        { type: core.ElementRef },
        { type: core.ChangeDetectorRef }
    ]; };
    MdbCarouselComponent.propDecorators = {
        _items: [{ type: core.ContentChildren, args: [MdbCarouselItemComponent,] }],
        animation: [{ type: core.Input }],
        controls: [{ type: core.Input }],
        dark: [{ type: core.Input }],
        indicators: [{ type: core.Input }],
        ride: [{ type: core.Input }],
        interval: [{ type: core.Input }],
        keyboard: [{ type: core.Input }],
        pause: [{ type: core.Input }],
        wrap: [{ type: core.Input }],
        slide: [{ type: core.Output }],
        slideChange: [{ type: core.Output }],
        onMouseEnter: [{ type: core.HostListener, args: ['mouseenter',] }],
        onMouseLeave: [{ type: core.HostListener, args: ['mouseleave',] }]
    };

    var MdbCarouselModule = /** @class */ (function () {
        function MdbCarouselModule() {
        }
        return MdbCarouselModule;
    }());
    MdbCarouselModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [MdbCarouselComponent, MdbCarouselItemComponent],
                    exports: [MdbCarouselComponent, MdbCarouselItemComponent],
                    imports: [common.CommonModule],
                },] }
    ];

    var MDB_MODULES = [
        MdbCollapseModule,
        MdbCheckboxModule,
        MdbRadioModule,
        MdbTooltipModule,
        MdbPopoverModule,
        MdbFormsModule,
        MdbModalModule,
        MdbDropdownModule,
        MdbRippleModule,
        MdbValidationModule,
        MdbScrollspyModule,
        MdbRangeModule,
        MdbTabsModule,
        MdbCarouselModule,
    ];
    var MdbModule = /** @class */ (function () {
        function MdbModule() {
        }
        return MdbModule;
    }());
    MdbModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [],
                    imports: [MDB_MODULES],
                    exports: [MDB_MODULES],
                },] }
    ];

    /**
     * Generated bundle index. Do not edit.
     */

    exports.MDB_CHECKBOX_VALUE_ACCESSOR = MDB_CHECKBOX_VALUE_ACCESSOR;
    exports.MDB_RADIO_GROUP_VALUE_ACCESSOR = MDB_RADIO_GROUP_VALUE_ACCESSOR;
    exports.MdbCarouselComponent = MdbCarouselComponent;
    exports.MdbCarouselItemComponent = MdbCarouselItemComponent;
    exports.MdbCarouselModule = MdbCarouselModule;
    exports.MdbCheckboxChange = MdbCheckboxChange;
    exports.MdbCheckboxDirective = MdbCheckboxDirective;
    exports.MdbCheckboxModule = MdbCheckboxModule;
    exports.MdbCollapseDirective = MdbCollapseDirective;
    exports.MdbCollapseModule = MdbCollapseModule;
    exports.MdbDropdownDirective = MdbDropdownDirective;
    exports.MdbDropdownMenuDirective = MdbDropdownMenuDirective;
    exports.MdbDropdownModule = MdbDropdownModule;
    exports.MdbDropdownToggleDirective = MdbDropdownToggleDirective;
    exports.MdbErrorDirective = MdbErrorDirective;
    exports.MdbFormControlComponent = MdbFormControlComponent;
    exports.MdbFormsModule = MdbFormsModule;
    exports.MdbInputDirective = MdbInputDirective;
    exports.MdbLabelDirective = MdbLabelDirective;
    exports.MdbModalConfig = MdbModalConfig;
    exports.MdbModalContainerComponent = MdbModalContainerComponent;
    exports.MdbModalModule = MdbModalModule;
    exports.MdbModalRef = MdbModalRef;
    exports.MdbModalService = MdbModalService;
    exports.MdbModule = MdbModule;
    exports.MdbPopoverDirective = MdbPopoverDirective;
    exports.MdbPopoverModule = MdbPopoverModule;
    exports.MdbRadioDirective = MdbRadioDirective;
    exports.MdbRadioGroupDirective = MdbRadioGroupDirective;
    exports.MdbRadioModule = MdbRadioModule;
    exports.MdbRangeComponent = MdbRangeComponent;
    exports.MdbRangeModule = MdbRangeModule;
    exports.MdbRippleDirective = MdbRippleDirective;
    exports.MdbRippleModule = MdbRippleModule;
    exports.MdbScrollspyDirective = MdbScrollspyDirective;
    exports.MdbScrollspyElementDirective = MdbScrollspyElementDirective;
    exports.MdbScrollspyLinkDirective = MdbScrollspyLinkDirective;
    exports.MdbScrollspyModule = MdbScrollspyModule;
    exports.MdbScrollspyService = MdbScrollspyService;
    exports.MdbSuccessDirective = MdbSuccessDirective;
    exports.MdbTabComponent = MdbTabComponent;
    exports.MdbTabContentDirective = MdbTabContentDirective;
    exports.MdbTabTitleDirective = MdbTabTitleDirective;
    exports.MdbTabsComponent = MdbTabsComponent;
    exports.MdbTabsModule = MdbTabsModule;
    exports.MdbTooltipDirective = MdbTooltipDirective;
    exports.MdbTooltipModule = MdbTooltipModule;
    exports.MdbValidateDirective = MdbValidateDirective;
    exports.MdbValidationModule = MdbValidationModule;
    exports.ɵa = MdbScrollspyWindowDirective;
    exports.ɵb = RANGE_VALUE_ACCESOR;
    exports.ɵc = MDB_TAB_CONTENT;
    exports.ɵd = MDB_TAB_TITLE;
    exports.ɵe = MdbTooltipComponent;
    exports.ɵf = MdbPopoverComponent;
    exports.ɵg = MdbAbstractFormControl;
    exports.ɵh = MdbTabPortalOutlet;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=mdb-angular-ui-kit.umd.js.map
