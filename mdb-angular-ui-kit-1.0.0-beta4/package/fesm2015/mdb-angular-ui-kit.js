import { EventEmitter, Directive, ElementRef, Renderer2, HostBinding, Output, Input, NgModule, forwardRef, HostListener, ContentChildren, Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ContentChild, Inject, TemplateRef, Injector, Injectable, ComponentFactoryResolver, ViewContainerRef, NgZone, InjectionToken } from '@angular/core';
import { fromEvent, Subject, from } from 'rxjs';
import { take, startWith, switchMap, takeUntil, first, filter, distinctUntilChanged } from 'rxjs/operators';
import { CommonModule, DOCUMENT } from '@angular/common';
import { NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import { OverlayConfig, Overlay, OverlayPositionBuilder, OverlayModule } from '@angular/cdk/overlay';
import { ComponentPortal, CdkPortalOutlet, TemplatePortal, PortalModule } from '@angular/cdk/portal';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ContentObserver } from '@angular/cdk/observers';
import { ConfigurableFocusTrapFactory } from '@angular/cdk/a11y';
import { BreakpointObserver } from '@angular/cdk/layout';

const TRANSITION_TIME = 350;
// tslint:disable-next-line: component-class-suffix
class MdbCollapseDirective {
    constructor(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.collapseClass = true;
        this.collapseShow = new EventEmitter();
        this.collapseShown = new EventEmitter();
        this.collapseHide = new EventEmitter();
        this.collapseHidden = new EventEmitter();
        this._collapsed = true;
        this._isTransitioning = false;
    }
    set collapsed(collapsed) {
        if (collapsed !== this._collapsed) {
            collapsed ? this.hide() : this.show();
            this._collapsed = collapsed;
        }
    }
    get collapsed() {
        return this._collapsed;
    }
    get host() {
        return this._elementRef.nativeElement;
    }
    show() {
        if (this._isTransitioning || !this.collapsed) {
            return;
        }
        this.collapseShow.emit(this);
        this._renderer.removeClass(this.host, 'collapse');
        this._renderer.addClass(this.host, 'collapsing');
        this._renderer.setStyle(this.host, 'height', '0px');
        this._isTransitioning = true;
        const scrollHeight = this.host.scrollHeight;
        fromEvent(this.host, 'transitionend')
            .pipe(take(1))
            .subscribe(() => {
            this._isTransitioning = false;
            this.collapsed = false;
            this._renderer.removeClass(this.host, 'collapsing');
            this._renderer.addClass(this.host, 'collapse');
            this._renderer.addClass(this.host, 'show');
            this._renderer.removeStyle(this.host, 'height');
            this.collapseShown.emit(this);
        });
        this._emulateTransitionEnd(this.host, TRANSITION_TIME);
        this._renderer.setStyle(this.host, 'height', `${scrollHeight}px`);
    }
    hide() {
        if (this._isTransitioning || this.collapsed) {
            return;
        }
        this.collapseHide.emit(this);
        const hostHeight = this.host.getBoundingClientRect().height;
        this._renderer.setStyle(this.host, 'height', `${hostHeight}px`);
        this._reflow(this.host);
        this._renderer.addClass(this.host, 'collapsing');
        this._renderer.removeClass(this.host, 'collapse');
        this._renderer.removeClass(this.host, 'show');
        this._isTransitioning = true;
        fromEvent(this.host, 'transitionend')
            .pipe(take(1))
            .subscribe(() => {
            this._renderer.removeClass(this.host, 'collapsing');
            this._renderer.addClass(this.host, 'collapse');
            this._isTransitioning = false;
            this.collapsed = true;
            this.collapseHidden.emit(this);
        });
        this._renderer.removeStyle(this.host, 'height');
        this._emulateTransitionEnd(this.host, TRANSITION_TIME);
    }
    toggle() {
        this.collapsed = !this.collapsed;
        this.collapsed ? this.hide() : this.show();
    }
    _reflow(element) {
        return element.offsetHeight;
    }
    _emulateTransitionEnd(element, duration) {
        let eventEmitted = false;
        const durationPadding = 5;
        const emulatedDuration = duration + durationPadding;
        fromEvent(element, 'transitionend')
            .pipe(take(1))
            .subscribe(() => {
            eventEmitted = true;
        });
        setTimeout(() => {
            if (!eventEmitted) {
                element.dispatchEvent(new Event('transitionend'));
            }
        }, emulatedDuration);
    }
}
MdbCollapseDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbCollapse]',
                exportAs: 'mdbCollapse',
            },] }
];
MdbCollapseDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
MdbCollapseDirective.propDecorators = {
    collapseClass: [{ type: HostBinding, args: ['class.collapse',] }],
    collapseShow: [{ type: Output }],
    collapseShown: [{ type: Output }],
    collapseHide: [{ type: Output }],
    collapseHidden: [{ type: Output }],
    collapsed: [{ type: Input }]
};

class MdbCollapseModule {
}
MdbCollapseModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbCollapseDirective],
                exports: [MdbCollapseDirective],
            },] }
];

const MDB_CHECKBOX_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    // tslint:disable-next-line: no-use-before-declare
    useExisting: forwardRef(() => MdbCheckboxDirective),
    multi: true,
};
class MdbCheckboxChange {
}
class MdbCheckboxDirective {
    constructor() {
        this._checked = false;
        this._value = null;
        this._disabled = false;
        this.checkboxChange = new EventEmitter();
        // Control Value Accessor Methods
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    get checked() {
        return this._checked;
    }
    set checked(value) {
        this._checked = value;
    }
    get value() {
        return this._value;
    }
    set value(value) {
        this._value = value;
    }
    get disabled() {
        return this._disabled;
    }
    set disabled(value) {
        this._disabled = value;
    }
    get isDisabled() {
        return this._disabled;
    }
    get isChecked() {
        return this._checked;
    }
    onCheckboxClick() {
        this.toggle();
    }
    onBlur() {
        this.onTouched();
    }
    get changeEvent() {
        const newChangeEvent = new MdbCheckboxChange();
        newChangeEvent.element = this;
        newChangeEvent.checked = this.checked;
        return newChangeEvent;
    }
    toggle() {
        if (this.disabled) {
            return;
        }
        this._checked = !this._checked;
        this.onChange(this.checked);
        this.onCheckboxChange();
    }
    onCheckboxChange() {
        this.checkboxChange.emit(this.changeEvent);
    }
    writeValue(value) {
        this.value = value;
        this.checked = !!value;
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled) {
        this.disabled = isDisabled;
    }
}
MdbCheckboxDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbCheckbox]',
                providers: [MDB_CHECKBOX_VALUE_ACCESSOR],
            },] }
];
MdbCheckboxDirective.ctorParameters = () => [];
MdbCheckboxDirective.propDecorators = {
    checked: [{ type: Input, args: ['checked',] }],
    value: [{ type: Input, args: ['value',] }],
    disabled: [{ type: Input, args: ['disabled',] }],
    checkboxChange: [{ type: Output }],
    isDisabled: [{ type: HostBinding, args: ['disabled',] }],
    isChecked: [{ type: HostBinding, args: ['checked',] }],
    onCheckboxClick: [{ type: HostListener, args: ['click',] }],
    onBlur: [{ type: HostListener, args: ['blur',] }]
};

class MdbCheckboxModule {
}
MdbCheckboxModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbCheckboxDirective],
                exports: [MdbCheckboxDirective],
                imports: [CommonModule, FormsModule],
            },] }
];

class MdbRadioDirective {
    constructor() {
        this._checked = false;
        this._value = null;
        this._disabled = false;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get checked() {
        return this._checked;
    }
    set checked(value) {
        this._checked = value;
    }
    get value() {
        return this._value;
    }
    set value(value) {
        this._value = value;
    }
    get disabled() {
        return this._disabled;
    }
    set disabled(value) {
        this._disabled = value;
    }
    get isDisabled() {
        return this._disabled;
    }
    get isChecked() {
        return this._checked;
    }
    get nameAttr() {
        return this.name;
    }
    _updateName(value) {
        this._name = value;
    }
    _updateChecked(value) {
        this._checked = value;
    }
    _updateDisabledState(value) {
        this._disabled = value;
    }
}
MdbRadioDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbRadio]',
            },] }
];
MdbRadioDirective.ctorParameters = () => [];
MdbRadioDirective.propDecorators = {
    name: [{ type: Input }],
    checked: [{ type: Input, args: ['checked',] }],
    value: [{ type: Input, args: ['value',] }],
    disabled: [{ type: Input, args: ['disabled',] }],
    isDisabled: [{ type: HostBinding, args: ['disabled',] }],
    isChecked: [{ type: HostBinding, args: ['checked',] }],
    nameAttr: [{ type: HostBinding, args: ['attr.name',] }]
};

const MDB_RADIO_GROUP_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    // tslint:disable-next-line: no-use-before-declare
    useExisting: forwardRef(() => MdbRadioGroupDirective),
    multi: true,
};
class MdbRadioGroupDirective {
    constructor() {
        this._disabled = false;
        this._destroy$ = new Subject();
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    get value() {
        return this._value;
    }
    set value(value) {
        this._value = value;
        if (this.radios) {
            this._updateChecked();
        }
    }
    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
        if (this.radios) {
            this._updateNames();
        }
    }
    get disabled() {
        return this._disabled;
    }
    set disabled(disabled) {
        this._disabled = disabled;
        if (this.radios) {
            this._updateDisabled();
        }
    }
    ngAfterContentInit() {
        this._updateNames();
        this._updateDisabled();
        this.radios.changes
            .pipe(startWith(this.radios), switchMap((radios) => from(Promise.resolve(radios))), takeUntil(this._destroy$))
            .subscribe(() => this._updateRadiosState());
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
    _updateRadiosState() {
        this._updateNames();
        this._updateChecked();
        this._updateDisabled();
    }
    _updateNames() {
        this.radios.forEach((radio) => radio._updateName(this.name));
    }
    _updateChecked() {
        this.radios.forEach((radio) => {
            const isChecked = radio.value === this._value;
            radio._updateChecked(isChecked);
        });
    }
    _updateDisabled() {
        this.radios.forEach((radio) => radio._updateDisabledState(this._disabled));
    }
    // Control value accessor methods
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled) {
        this._disabled = isDisabled;
        this._updateDisabled();
    }
    writeValue(value) {
        this.value = value;
    }
}
MdbRadioGroupDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbRadioGroup]',
                providers: [MDB_RADIO_GROUP_VALUE_ACCESSOR],
            },] }
];
MdbRadioGroupDirective.propDecorators = {
    radios: [{ type: ContentChildren, args: [MdbRadioDirective, { descendants: true },] }],
    value: [{ type: Input }],
    name: [{ type: Input }],
    disabled: [{ type: Input }]
};

class MdbRadioModule {
}
MdbRadioModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbRadioDirective, MdbRadioGroupDirective],
                exports: [MdbRadioDirective, MdbRadioGroupDirective],
                imports: [CommonModule, FormsModule],
            },] }
];

class MdbTooltipComponent {
    constructor(_cdRef) {
        this._cdRef = _cdRef;
        this._hidden = new Subject();
        this.animationState = 'hidden';
    }
    ngOnInit() { }
    markForCheck() {
        this._cdRef.markForCheck();
    }
    onAnimationEnd(event) {
        if (event.toState === 'hidden') {
            this._hidden.next();
        }
    }
}
MdbTooltipComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-tooltip',
                template: "<div\n  *ngIf=\"html\"\n  [@fade]=\"animationState\"\n  (@fade.done)=\"onAnimationEnd($event)\"\n  [@.disabled]=\"!animation\"\n  [innerHTML]=\"title\"\n  class=\"tooltip-inner\"\n></div>\n<div\n  *ngIf=\"!html\"\n  [@fade]=\"animationState\"\n  (@fade.done)=\"onAnimationEnd($event)\"\n  [@.disabled]=\"!animation\"\n  class=\"tooltip-inner\"\n>\n  {{ title }}\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                animations: [
                    trigger('fade', [
                        state('visible', style({ opacity: 1 })),
                        state('hidden', style({ opacity: 0 })),
                        transition('visible => hidden', animate('150ms linear')),
                        transition(':enter', animate('150ms linear')),
                    ]),
                ]
            },] }
];
MdbTooltipComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
MdbTooltipComponent.propDecorators = {
    title: [{ type: Input }],
    html: [{ type: Input }],
    animation: [{ type: Input }]
};

// tslint:disable-next-line:component-class-suffix
class MdbTooltipDirective {
    constructor(_overlay, _overlayPositionBuilder, _elementRef, _renderer) {
        this._overlay = _overlay;
        this._overlayPositionBuilder = _overlayPositionBuilder;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.mdbTooltip = '';
        this.tooltipDisabled = false;
        this.placement = 'top';
        this.html = false;
        this.animation = true;
        this.trigger = 'hover focus';
        this.delayShow = 0;
        this.delayHide = 0;
        this.offset = 4;
        this.tooltipShow = new EventEmitter();
        this.tooltipShown = new EventEmitter();
        this.tooltipHide = new EventEmitter();
        this.tooltipHidden = new EventEmitter();
        this._open = false;
        this._showTimeout = 0;
        this._hideTimeout = 0;
        this._destroy$ = new Subject();
    }
    ngOnInit() {
        if (this.tooltipDisabled) {
            return;
        }
        this._bindTriggerEvents();
        this._createOverlay();
    }
    ngOnDestroy() {
        if (this._open) {
            this.hide();
        }
        this._destroy$.next();
        this._destroy$.complete();
    }
    _bindTriggerEvents() {
        const triggers = this.trigger.split(' ');
        triggers.forEach((trigger) => {
            if (trigger === 'click') {
                fromEvent(this._elementRef.nativeElement, trigger)
                    .pipe(takeUntil(this._destroy$))
                    .subscribe(() => this.toggle());
            }
            else if (trigger !== 'manual') {
                const evIn = trigger === 'hover' ? 'mouseenter' : 'focusin';
                const evOut = trigger === 'hover' ? 'mouseleave' : 'focusout';
                fromEvent(this._elementRef.nativeElement, evIn)
                    .pipe(takeUntil(this._destroy$))
                    .subscribe(() => this.show());
                fromEvent(this._elementRef.nativeElement, evOut)
                    .pipe(takeUntil(this._destroy$))
                    .subscribe(() => this.hide());
            }
        });
    }
    _createOverlayConfig() {
        const positionStrategy = this._overlayPositionBuilder
            .flexibleConnectedTo(this._elementRef)
            .withPositions(this._getPosition());
        const overlayConfig = new OverlayConfig({
            hasBackdrop: false,
            scrollStrategy: this._overlay.scrollStrategies.reposition(),
            positionStrategy,
        });
        return overlayConfig;
    }
    _createOverlay() {
        this._overlayRef = this._overlay.create(this._createOverlayConfig());
    }
    _getPosition() {
        let position;
        const positionTop = {
            originX: 'center',
            originY: 'top',
            overlayX: 'center',
            overlayY: 'bottom',
            offsetY: -this.offset,
        };
        const positionBottom = {
            originX: 'center',
            originY: 'bottom',
            overlayX: 'center',
            overlayY: 'top',
            offsetY: this.offset,
        };
        const positionRight = {
            originX: 'end',
            originY: 'center',
            overlayX: 'start',
            overlayY: 'center',
            offsetX: this.offset,
        };
        const positionLeft = {
            originX: 'start',
            originY: 'center',
            overlayX: 'end',
            overlayY: 'center',
            offsetX: -this.offset,
        };
        switch (this.placement) {
            case 'top':
                position = [positionTop, positionBottom];
                break;
            case 'bottom':
                position = [positionBottom, positionTop];
                break;
            case 'left':
                position = [positionLeft, positionRight];
                break;
            case 'right':
                position = [positionRight, positionLeft];
                break;
            default:
                break;
        }
        return position;
    }
    show() {
        if (this._open) {
            this._overlayRef.detach();
        }
        if (this._hideTimeout) {
            clearTimeout(this._hideTimeout);
            this._hideTimeout = null;
        }
        this._showTimeout = setTimeout(() => {
            const tooltipPortal = new ComponentPortal(MdbTooltipComponent);
            this.tooltipShow.emit(this);
            this._open = true;
            this._tooltipRef = this._overlayRef.attach(tooltipPortal);
            this._tooltipRef.instance.title = this.mdbTooltip;
            this._tooltipRef.instance.html = this.html;
            this._tooltipRef.instance.animation = this.animation;
            this._tooltipRef.instance.animationState = 'visible';
            this._tooltipRef.instance.markForCheck();
            this.tooltipShown.emit(this);
        }, this.delayShow);
    }
    hide() {
        if (!this._open) {
            return;
        }
        if (this._showTimeout) {
            clearTimeout(this._showTimeout);
            this._showTimeout = null;
        }
        this._hideTimeout = setTimeout(() => {
            this.tooltipHide.emit(this);
            this._tooltipRef.instance._hidden.pipe(first()).subscribe(() => {
                this._overlayRef.detach();
                this._open = false;
                this.tooltipShown.emit(this);
            });
            this._tooltipRef.instance.animationState = 'hidden';
            this._tooltipRef.instance.markForCheck();
        }, this.delayHide);
    }
    toggle() {
        if (this._open) {
            this.hide();
        }
        else {
            this.show();
        }
    }
}
MdbTooltipDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbTooltip]',
                exportAs: 'mdbTooltip',
            },] }
];
MdbTooltipDirective.ctorParameters = () => [
    { type: Overlay },
    { type: OverlayPositionBuilder },
    { type: ElementRef },
    { type: Renderer2 }
];
MdbTooltipDirective.propDecorators = {
    mdbTooltip: [{ type: Input }],
    tooltipDisabled: [{ type: Input }],
    placement: [{ type: Input }],
    html: [{ type: Input }],
    animation: [{ type: Input }],
    trigger: [{ type: Input }],
    delayShow: [{ type: Input }],
    delayHide: [{ type: Input }],
    offset: [{ type: Input }],
    tooltipShow: [{ type: Output }],
    tooltipShown: [{ type: Output }],
    tooltipHide: [{ type: Output }],
    tooltipHidden: [{ type: Output }]
};

class MdbTooltipModule {
}
MdbTooltipModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, OverlayModule],
                declarations: [MdbTooltipDirective, MdbTooltipComponent],
                exports: [MdbTooltipDirective, MdbTooltipComponent],
            },] }
];

class MdbPopoverComponent {
    constructor(_cdRef) {
        this._cdRef = _cdRef;
        this._hidden = new Subject();
        this.animationState = 'hidden';
    }
    ngOnInit() { }
    markForCheck() {
        this._cdRef.markForCheck();
    }
    onAnimationEnd(event) {
        if (event.toState === 'hidden') {
            this._hidden.next();
        }
    }
}
MdbPopoverComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-popover',
                template: "<div\n  class=\"popover\"\n  [@fade]=\"animationState\"\n  (@fade.done)=\"onAnimationEnd($event)\"\n  [@.disabled]=\"!animation\"\n>\n  <div *ngIf=\"title\" class=\"popover-header\">\n    {{ title }}\n  </div>\n  <div *ngIf=\"template\" [innerHTML]=\"content\" class=\"popover-body\"></div>\n  <div *ngIf=\"!template\" class=\"popover-body\">\n    {{ content }}\n  </div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                animations: [
                    trigger('fade', [
                        state('visible', style({ opacity: 1 })),
                        state('hidden', style({ opacity: 0 })),
                        transition('visible <=> hidden', animate('150ms linear')),
                        transition(':enter', animate('150ms linear')),
                    ]),
                ]
            },] }
];
MdbPopoverComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
MdbPopoverComponent.propDecorators = {
    title: [{ type: Input }],
    content: [{ type: Input }],
    template: [{ type: Input }],
    animation: [{ type: Input }]
};

// tslint:disable-next-line:component-class-suffix
class MdbPopoverDirective {
    constructor(_overlay, _overlayPositionBuilder, _elementRef) {
        this._overlay = _overlay;
        this._overlayPositionBuilder = _overlayPositionBuilder;
        this._elementRef = _elementRef;
        this.mdbPopover = '';
        this.mdbPopoverTitle = '';
        this.popoverDisabled = false;
        this.placement = 'top';
        this.template = false;
        this.animation = true;
        this.trigger = 'click';
        this.delayShow = 0;
        this.delayHide = 0;
        this.offset = 4;
        this.popoverShow = new EventEmitter();
        this.popoverShown = new EventEmitter();
        this.popoverHide = new EventEmitter();
        this.popoverHidden = new EventEmitter();
        this._open = false;
        this._showTimeout = 0;
        this._hideTimeout = 0;
        this._destroy$ = new Subject();
    }
    ngOnInit() {
        if (this.popoverDisabled) {
            return;
        }
        this._bindTriggerEvents();
        this._createOverlay();
    }
    ngOnDestroy() {
        if (this._open) {
            this.hide();
        }
        this._destroy$.next();
        this._destroy$.complete();
    }
    _bindTriggerEvents() {
        const triggers = this.trigger.split(' ');
        triggers.forEach((trigger) => {
            if (trigger === 'click') {
                fromEvent(this._elementRef.nativeElement, trigger)
                    .pipe(takeUntil(this._destroy$))
                    .subscribe(() => this.toggle());
            }
            else if (trigger !== 'manual') {
                const evIn = trigger === 'hover' ? 'mouseenter' : 'focusin';
                const evOut = trigger === 'hover' ? 'mouseleave' : 'focusout';
                fromEvent(this._elementRef.nativeElement, evIn)
                    .pipe(takeUntil(this._destroy$))
                    .subscribe(() => this.show());
                fromEvent(this._elementRef.nativeElement, evOut)
                    .pipe(takeUntil(this._destroy$))
                    .subscribe(() => this.hide());
            }
        });
    }
    _createOverlayConfig() {
        const positionStrategy = this._overlayPositionBuilder
            .flexibleConnectedTo(this._elementRef)
            .withPositions(this._getPosition());
        const overlayConfig = new OverlayConfig({
            hasBackdrop: false,
            scrollStrategy: this._overlay.scrollStrategies.reposition(),
            positionStrategy,
        });
        return overlayConfig;
    }
    _createOverlay() {
        this._overlayRef = this._overlay.create(this._createOverlayConfig());
    }
    _getPosition() {
        let position;
        const positionTop = {
            originX: 'center',
            originY: 'top',
            overlayX: 'center',
            overlayY: 'bottom',
            offsetY: -this.offset,
        };
        const positionBottom = {
            originX: 'center',
            originY: 'bottom',
            overlayX: 'center',
            overlayY: 'top',
            offsetY: this.offset,
        };
        const positionRight = {
            originX: 'end',
            originY: 'center',
            overlayX: 'start',
            overlayY: 'center',
            offsetX: this.offset,
        };
        const positionLeft = {
            originX: 'start',
            originY: 'center',
            overlayX: 'end',
            overlayY: 'center',
            offsetX: -this.offset,
        };
        switch (this.placement) {
            case 'top':
                position = [positionTop, positionBottom];
                break;
            case 'bottom':
                position = [positionBottom, positionTop];
                break;
            case 'left':
                position = [positionLeft, positionRight, positionTop, positionBottom];
                break;
            case 'right':
                position = [positionRight, positionLeft, positionTop, positionBottom];
                break;
            default:
                break;
        }
        return position;
    }
    show() {
        if (this._open) {
            this._overlayRef.detach();
        }
        if (this._hideTimeout) {
            clearTimeout(this._hideTimeout);
            this._hideTimeout = null;
        }
        this._showTimeout = setTimeout(() => {
            const tooltipPortal = new ComponentPortal(MdbPopoverComponent);
            this.popoverShow.emit(this);
            this._open = true;
            this._tooltipRef = this._overlayRef.attach(tooltipPortal);
            this._tooltipRef.instance.content = this.mdbPopover;
            this._tooltipRef.instance.title = this.mdbPopoverTitle;
            this._tooltipRef.instance.template = this.template;
            this._tooltipRef.instance.animation = this.animation;
            this._tooltipRef.instance.animationState = 'visible';
            this._tooltipRef.instance.markForCheck();
            this.popoverShown.emit(this);
        }, this.delayShow);
    }
    hide() {
        if (!this._open) {
            return;
        }
        if (this._showTimeout) {
            clearTimeout(this._showTimeout);
            this._showTimeout = null;
        }
        this._hideTimeout = setTimeout(() => {
            this.popoverHide.emit(this);
            this._tooltipRef.instance._hidden.pipe(first()).subscribe(() => {
                this._overlayRef.detach();
                this._open = false;
                this.popoverShown.emit(this);
            });
            this._tooltipRef.instance.animationState = 'hidden';
            this._tooltipRef.instance.markForCheck();
        }, this.delayHide);
    }
    toggle() {
        if (this._open) {
            this.hide();
        }
        else {
            this.show();
        }
    }
}
MdbPopoverDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbPopover]',
                exportAs: 'mdbPopover',
            },] }
];
MdbPopoverDirective.ctorParameters = () => [
    { type: Overlay },
    { type: OverlayPositionBuilder },
    { type: ElementRef }
];
MdbPopoverDirective.propDecorators = {
    mdbPopover: [{ type: Input }],
    mdbPopoverTitle: [{ type: Input }],
    popoverDisabled: [{ type: Input }],
    placement: [{ type: Input }],
    template: [{ type: Input }],
    animation: [{ type: Input }],
    trigger: [{ type: Input }],
    delayShow: [{ type: Input }],
    delayHide: [{ type: Input }],
    offset: [{ type: Input }],
    popoverShow: [{ type: Output }],
    popoverShown: [{ type: Output }],
    popoverHide: [{ type: Output }],
    popoverHidden: [{ type: Output }]
};

class MdbPopoverModule {
}
MdbPopoverModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, OverlayModule],
                declarations: [MdbPopoverDirective, MdbPopoverComponent],
                exports: [MdbPopoverDirective, MdbPopoverComponent],
            },] }
];

// tslint:disable-next-line: directive-class-suffix
class MdbAbstractFormControl {
}
MdbAbstractFormControl.decorators = [
    { type: Directive }
];

// tslint:disable-next-line: component-class-suffix
class MdbInputDirective {
    constructor(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.stateChanges = new Subject();
        this._focused = false;
        this._disabled = false;
        this._readonly = false;
    }
    get disabled() {
        return this._disabled;
    }
    set disabled(value) {
        this._disabled = value;
    }
    get readonly() {
        return this._readonly;
    }
    set readonly(value) {
        if (value) {
            this._renderer.setAttribute(this._elementRef.nativeElement, 'readonly', '');
        }
        else {
            this._renderer.removeAttribute(this._elementRef.nativeElement, 'readonly');
        }
        this._readonly = value;
    }
    get value() {
        return this._elementRef.nativeElement.value;
    }
    set value(value) {
        if (value !== this.value) {
            this._elementRef.nativeElement.value = value;
            this.stateChanges.next();
        }
    }
    _onFocus() {
        this._focused = true;
        this.stateChanges.next();
    }
    _onBlur() {
        this._focused = false;
        this.stateChanges.next();
    }
    get hasValue() {
        return this._elementRef.nativeElement.value !== '';
    }
    get focused() {
        return this._focused;
    }
    get labelActive() {
        return this.focused || this.hasValue;
    }
}
MdbInputDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbInput]',
                exportAs: 'mdbInput',
                providers: [{ provide: MdbAbstractFormControl, useExisting: MdbInputDirective }],
            },] }
];
MdbInputDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
MdbInputDirective.propDecorators = {
    disabled: [{ type: HostBinding, args: ['disabled',] }, { type: Input, args: ['disabled',] }],
    readonly: [{ type: Input, args: ['readonly',] }],
    value: [{ type: Input }],
    _onFocus: [{ type: HostListener, args: ['focus',] }],
    _onBlur: [{ type: HostListener, args: ['blur',] }]
};

// tslint:disable-next-line: component-class-suffix
class MdbLabelDirective {
    constructor() { }
}
MdbLabelDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbLabel]',
                exportAs: 'mdbLabel',
            },] }
];
MdbLabelDirective.ctorParameters = () => [];

class MdbFormControlComponent {
    constructor(_renderer, _contentObserver) {
        this._renderer = _renderer;
        this._contentObserver = _contentObserver;
        this.outline = true;
        this._destroy$ = new Subject();
        this._notchLeadingLength = 9;
        this._labelMarginLeft = 0;
        this._labelGapPadding = 8;
        this._labelScale = 0.8;
    }
    ngAfterViewInit() { }
    ngAfterContentInit() {
        if (this._label) {
            this._updateBorderGap();
        }
        else {
            this._renderer.addClass(this._input.nativeElement, 'placeholder-active');
        }
        this._updateLabelActiveState();
        if (this._label) {
            this._contentObserver
                .observe(this._label.nativeElement)
                .pipe(takeUntil(this._destroy$))
                .subscribe(() => {
                this._updateBorderGap();
            });
        }
        this._formControl.stateChanges.pipe(takeUntil(this._destroy$)).subscribe(() => {
            this._updateLabelActiveState();
            if (this._label) {
                this._updateBorderGap();
            }
        });
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.unsubscribe();
    }
    _getLabelWidth() {
        return this._label.nativeElement.clientWidth * this._labelScale + this._labelGapPadding;
    }
    _updateBorderGap() {
        const notchLeadingWidth = `${this._labelMarginLeft + this._notchLeadingLength}px`;
        const notchMiddleWidth = `${this._getLabelWidth()}px`;
        this._renderer.setStyle(this._notchLeading.nativeElement, 'width', notchLeadingWidth);
        this._renderer.setStyle(this._notchMiddle.nativeElement, 'width', notchMiddleWidth);
        this._renderer.setStyle(this._label.nativeElement, 'margin-left', `${this._labelMarginLeft}px`);
    }
    _updateLabelActiveState() {
        if (this._isLabelActive()) {
            this._renderer.addClass(this._input.nativeElement, 'active');
        }
        else {
            this._renderer.removeClass(this._input.nativeElement, 'active');
        }
    }
    _isLabelActive() {
        return this._formControl && this._formControl.labelActive;
    }
}
MdbFormControlComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-form-control',
                template: "<ng-content></ng-content>\n<div class=\"form-notch\">\n  <div #notchLeading class=\"form-notch-leading\"></div>\n  <div #notchMiddle class=\"form-notch-middle\"></div>\n  <div class=\"form-notch-trailing\"></div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush
            },] }
];
MdbFormControlComponent.ctorParameters = () => [
    { type: Renderer2 },
    { type: ContentObserver }
];
MdbFormControlComponent.propDecorators = {
    _notchLeading: [{ type: ViewChild, args: ['notchLeading', { static: true },] }],
    _notchMiddle: [{ type: ViewChild, args: ['notchMiddle', { static: true },] }],
    _input: [{ type: ContentChild, args: [MdbInputDirective, { static: true, read: ElementRef },] }],
    _formControl: [{ type: ContentChild, args: [MdbAbstractFormControl, { static: true },] }],
    _label: [{ type: ContentChild, args: [MdbLabelDirective, { static: true, read: ElementRef },] }],
    outline: [{ type: HostBinding, args: ['class.form-outline',] }]
};

class MdbFormsModule {
}
MdbFormsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbFormControlComponent, MdbInputDirective, MdbLabelDirective],
                exports: [MdbFormControlComponent, MdbInputDirective, MdbLabelDirective],
                imports: [CommonModule, FormsModule],
            },] }
];

class MdbModalContainerComponent {
    constructor(_document, _elementRef, _renderer, _focusTrapFactory) {
        this._document = _document;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._focusTrapFactory = _focusTrapFactory;
        this._destroy$ = new Subject();
        this.backdropClick$ = new Subject();
        this.BACKDROP_TRANSITION = 150;
        this.MODAL_TRANSITION = 200;
        this.modal = true;
    }
    get hasAnimation() {
        return this._config.animation;
    }
    ngOnInit() {
        this._updateContainerClass();
        this._renderer.addClass(this._document.body, 'modal-open');
        this._renderer.setStyle(this._document.body, 'padding-right', '15px');
        this._renderer.setStyle(this._elementRef.nativeElement, 'display', 'block');
        this._previouslyFocusedElement = this._document.activeElement;
        this._focusTrap = this._focusTrapFactory.create(this._elementRef.nativeElement);
        if (this._config.animation) {
            setTimeout(() => {
                this._renderer.addClass(this._elementRef.nativeElement, 'show');
                setTimeout(() => {
                    this._focusTrap.focusInitialElementWhenReady();
                }, this.MODAL_TRANSITION);
            }, this.BACKDROP_TRANSITION);
        }
        else {
            this._focusTrap.focusInitialElementWhenReady();
        }
    }
    ngAfterViewInit() {
        if (!this._config.ignoreBackdropClick) {
            fromEvent(this._elementRef.nativeElement, 'click')
                .pipe(filter((event) => {
                const target = event.target;
                const dialog = this.modalDialog.nativeElement;
                const notDialog = target !== dialog;
                const notDialogContent = !dialog.contains(target);
                return notDialog && notDialogContent;
            }), takeUntil(this._destroy$))
                .subscribe((event) => {
                this.backdropClick$.next(event);
            });
        }
    }
    ngOnDestroy() {
        this._previouslyFocusedElement.focus();
        this._focusTrap.destroy();
        this._destroy$.next();
        this._destroy$.complete();
    }
    _updateContainerClass() {
        if (this._config.containerClass === '' ||
            (this._config.containerClass.length && this._config.containerClass.length === 0)) {
            return;
        }
        const containerClasses = this._config.containerClass.split(' ');
        containerClasses.forEach((containerClass) => {
            this._renderer.addClass(this._elementRef.nativeElement, containerClass);
        });
    }
    _close() {
        if (this._config.animation) {
            this._renderer.removeClass(this._elementRef.nativeElement, 'show');
        }
        // Pause iframe/video when closing modal
        const iframeElements = Array.from(this._elementRef.nativeElement.querySelectorAll('iframe'));
        const videoElements = Array.from(this._elementRef.nativeElement.querySelectorAll('video'));
        iframeElements.forEach((iframe) => {
            const srcAttribute = iframe.getAttribute('src');
            this._renderer.setAttribute(iframe, 'src', srcAttribute);
        });
        videoElements.forEach((video) => {
            video.pause();
        });
    }
    _restoreScrollbar() {
        this._renderer.removeClass(this._document.body, 'modal-open');
        this._renderer.removeStyle(this._document.body, 'padding-right');
    }
    attachComponentPortal(portal) {
        return this._portalOutlet.attachComponentPortal(portal);
    }
    attachTemplatePortal(portal) {
        return this._portalOutlet.attachTemplatePortal(portal);
    }
}
MdbModalContainerComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-modal-container',
                template: "<div #dialog [class]=\"'modal-dialog' + (_config.modalClass ? ' ' + _config.modalClass : '')\">\n  <div class=\"modal-content\">\n    <ng-template cdkPortalOutlet></ng-template>\n  </div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.Default
            },] }
];
MdbModalContainerComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ElementRef },
    { type: Renderer2 },
    { type: ConfigurableFocusTrapFactory }
];
MdbModalContainerComponent.propDecorators = {
    _portalOutlet: [{ type: ViewChild, args: [CdkPortalOutlet, { static: true },] }],
    modalDialog: [{ type: ViewChild, args: ['dialog', { static: true },] }],
    modal: [{ type: HostBinding, args: ['class.modal',] }],
    hasAnimation: [{ type: HostBinding, args: ['class.fade',] }]
};

// tslint:disable: no-inferrable-types
class MdbModalConfig {
    constructor() {
        this.animation = true;
        this.backdrop = true;
        this.ignoreBackdropClick = false;
        this.keyboard = true;
        this.modalClass = '';
        this.containerClass = '';
        this.data = null;
    }
}

class MdbModalRef {
    constructor(_overlayRef, _container) {
        this._overlayRef = _overlayRef;
        this._container = _container;
        this.onClose$ = new Subject();
        this.onClose = this.onClose$.asObservable();
    }
    close(message) {
        this._container._close();
        setTimeout(() => {
            this._container._restoreScrollbar();
            this.onClose$.next(message);
            this.onClose$.complete();
            this._overlayRef.detach();
            this._overlayRef.dispose();
        }, this._container.MODAL_TRANSITION);
    }
}

class MdbModalService {
    constructor(_document, _overlay, _injector, _cfr) {
        this._document = _document;
        this._overlay = _overlay;
        this._injector = _injector;
        this._cfr = _cfr;
    }
    open(componentOrTemplateRef, config) {
        const defaultConfig = new MdbModalConfig();
        config = config ? Object.assign(defaultConfig, config) : defaultConfig;
        const overlayRef = this._createOverlay(config);
        const container = this._createContainer(overlayRef, config);
        const modalRef = this._createContent(componentOrTemplateRef, container, overlayRef, config);
        this._registerListeners(modalRef, config, container);
        return modalRef;
    }
    _createOverlay(config) {
        const overlayConfig = this._getOverlayConfig(config);
        return this._overlay.create(overlayConfig);
    }
    _getOverlayConfig(modalConfig) {
        const config = new OverlayConfig({
            positionStrategy: this._overlay.position().global(),
            scrollStrategy: this._overlay.scrollStrategies.noop(),
            hasBackdrop: modalConfig.backdrop,
            backdropClass: 'mdb-backdrop',
        });
        return config;
    }
    _createContainer(overlayRef, config) {
        const portal = new ComponentPortal(MdbModalContainerComponent, null, this._injector, this._cfr);
        const containerRef = overlayRef.attach(portal);
        containerRef.instance._config = config;
        return containerRef.instance;
    }
    _createContent(componentOrTemplate, container, overlayRef, config) {
        const modalRef = new MdbModalRef(overlayRef, container);
        if (componentOrTemplate instanceof TemplateRef) {
            container.attachTemplatePortal(new TemplatePortal(componentOrTemplate, null, {
                $implicit: config.data,
                modalRef,
            }));
        }
        else {
            const injector = this._createInjector(config, modalRef, container);
            const contentRef = container.attachComponentPortal(new ComponentPortal(componentOrTemplate, config.viewContainerRef, injector));
            if (config.data) {
                Object.assign(contentRef.instance, Object.assign({}, config.data));
            }
        }
        return modalRef;
    }
    _createInjector(config, modalRef, container) {
        const userInjector = config && config.viewContainerRef && config.viewContainerRef.injector;
        // The dialog container should be provided as the dialog container and the dialog's
        // content are created out of the same `ViewContainerRef` and as such, are siblings
        // for injector purposes. To allow the hierarchy that is expected, the dialog
        // container is explicitly provided in the injector.
        const providers = [
            { provide: MdbModalContainerComponent, useValue: container },
            { provide: MdbModalRef, useValue: modalRef },
        ];
        return Injector.create({ parent: userInjector || this._injector, providers });
    }
    _registerListeners(modalRef, config, container) {
        container.backdropClick$.pipe(take(1)).subscribe(() => {
            modalRef.close();
        });
        if (config.keyboard) {
            fromEvent(container._elementRef.nativeElement, 'keydown')
                .pipe(filter((event) => {
                return event.key === 'Escape';
            }), take(1))
                .subscribe(() => {
                modalRef.close();
            });
        }
    }
}
MdbModalService.decorators = [
    { type: Injectable }
];
MdbModalService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: Overlay },
    { type: Injector },
    { type: ComponentFactoryResolver }
];

class MdbModalModule {
}
MdbModalModule.decorators = [
    { type: NgModule, args: [{
                imports: [OverlayModule, PortalModule],
                exports: [MdbModalContainerComponent],
                declarations: [MdbModalContainerComponent],
                providers: [MdbModalService],
                entryComponents: [MdbModalContainerComponent],
            },] }
];

// tslint:disable-next-line:component-class-suffix
class MdbDropdownToggleDirective {
    constructor() { }
    ngOnInit() { }
}
MdbDropdownToggleDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbDropdownToggle]',
                exportAs: 'mdbDropdownToggle',
            },] }
];
MdbDropdownToggleDirective.ctorParameters = () => [];

// tslint:disable-next-line:component-class-suffix
class MdbDropdownMenuDirective {
    constructor() { }
    ngOnInit() { }
}
MdbDropdownMenuDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbDropdownMenu]',
                exportAs: 'mdbDropdownMenu',
            },] }
];
MdbDropdownMenuDirective.ctorParameters = () => [];

// tslint:disable-next-line:component-class-suffix
class MdbDropdownDirective {
    constructor(_overlay, _overlayPositionBuilder, _elementRef, _vcr, _breakpointObserver, _cdRef) {
        this._overlay = _overlay;
        this._overlayPositionBuilder = _overlayPositionBuilder;
        this._elementRef = _elementRef;
        this._vcr = _vcr;
        this._breakpointObserver = _breakpointObserver;
        this._cdRef = _cdRef;
        this.animation = true;
        this.offset = 0;
        this.dropdownShow = new EventEmitter();
        this.dropdownShown = new EventEmitter();
        this.dropdownHide = new EventEmitter();
        this.dropdownHidden = new EventEmitter();
        this._open = false;
        this._breakpoints = {
            isSm: this._breakpointObserver.isMatched('(min-width: 576px)'),
            isMd: this._breakpointObserver.isMatched('(min-width: 768px)'),
            isLg: this._breakpointObserver.isMatched('(min-width: 992px)'),
            isXl: this._breakpointObserver.isMatched('(min-width: 1200px)'),
            isXxl: this._breakpointObserver.isMatched('(min-width: 1400px)'),
        };
        this._destroy$ = new Subject();
        this._animationState = 'hidden';
    }
    ngOnInit() { }
    ngAfterContentInit() {
        this._bindDropdownToggleClick();
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
    _bindDropdownToggleClick() {
        fromEvent(this._dropdownToggle.nativeElement, 'click')
            .pipe(takeUntil(this._destroy$))
            .subscribe(() => this.toggle());
    }
    _createOverlayConfig() {
        return new OverlayConfig({
            hasBackdrop: false,
            scrollStrategy: this._overlay.scrollStrategies.reposition(),
            positionStrategy: this._createPositionStrategy(),
        });
    }
    _createOverlay() {
        this._overlayRef = this._overlay.create(this._createOverlayConfig());
    }
    _createPositionStrategy() {
        const positionStrategy = this._overlayPositionBuilder
            .flexibleConnectedTo(this._dropdownToggle)
            .withPositions(this._getPosition())
            .withFlexibleDimensions(false);
        return positionStrategy;
    }
    _getPosition() {
        this._isDropUp = this._elementRef.nativeElement.classList.contains('dropup');
        this._isDropStart = this._elementRef.nativeElement.classList.contains('dropstart');
        this._isDropEnd = this._elementRef.nativeElement.classList.contains('dropend');
        this._isDropdownMenuEnd = this._dropdownMenu.nativeElement.classList.contains('dropdown-menu-end');
        this._xPosition = this._isDropdownMenuEnd ? 'end' : 'start';
        const regex = new RegExp(/dropdown-menu-(sm|md|lg|xl|xxl)-(start|end)/, 'g');
        const responsiveClass = this._dropdownMenu.nativeElement.className.match(regex);
        if (responsiveClass) {
            this._subscribeBrakpoints();
            const positionRegex = new RegExp(/start|end/, 'g');
            const breakpointRegex = new RegExp(/(sm|md|lg|xl|xxl)/, 'g');
            const dropdownPosition = positionRegex.exec(responsiveClass)[0];
            const breakpoint = breakpointRegex.exec(responsiveClass)[0];
            switch (true) {
                case breakpoint === 'xxl' && this._breakpoints.isXxl:
                    this._xPosition = dropdownPosition;
                    break;
                case breakpoint === 'xl' && this._breakpoints.isXl:
                    this._xPosition = dropdownPosition;
                    break;
                case breakpoint === 'lg' && this._breakpoints.isLg:
                    this._xPosition = dropdownPosition;
                    break;
                case breakpoint === 'md' && this._breakpoints.isMd:
                    this._xPosition = dropdownPosition;
                    break;
                case breakpoint === 'sm' && this._breakpoints.isSm:
                    this._xPosition = dropdownPosition;
                    break;
                default:
                    break;
            }
        }
        let position;
        const positionDropup = {
            originX: this._xPosition,
            originY: 'top',
            overlayX: this._xPosition,
            overlayY: 'bottom',
            offsetY: -this.offset,
        };
        const positionDropdown = {
            originX: this._xPosition,
            originY: 'bottom',
            overlayX: this._xPosition,
            overlayY: 'top',
            offsetY: this.offset,
        };
        const positionDropstart = {
            originX: 'start',
            originY: 'top',
            overlayX: 'end',
            overlayY: 'top',
            offsetX: this.offset,
        };
        const positionDropend = {
            originX: 'end',
            originY: 'top',
            overlayX: 'start',
            overlayY: 'top',
            offsetX: -this.offset,
        };
        switch (true) {
            case this._isDropEnd:
                position = [positionDropend, positionDropstart];
                break;
            case this._isDropStart:
                position = [positionDropstart, positionDropend];
                break;
            case this._isDropUp:
                position = [positionDropup, positionDropdown];
                break;
            default:
                position = [positionDropdown, positionDropup];
                break;
        }
        return position;
    }
    _listenToOutSideCick(overlayRef, origin) {
        return fromEvent(document, 'click').pipe(filter((event) => {
            const target = event.target;
            const notOrigin = target !== origin;
            const notValue = !this._dropdownMenu.nativeElement.contains(target);
            const notOverlay = !!overlayRef && overlayRef.overlayElement.contains(target) === false;
            return notOrigin && notValue && notOverlay;
        }), takeUntil(overlayRef.detachments()));
    }
    onAnimationEnd(event) {
        if (event.fromState === 'visible' && event.toState === 'hidden') {
            this._overlayRef.detach();
            this._open = false;
            this.dropdownHidden.emit(this);
        }
        if (event.fromState === 'hidden' && event.toState === 'visible') {
            this.dropdownShown.emit(this);
        }
    }
    _subscribeBrakpoints() {
        const brakpoints = [
            '(min-width: 576px)',
            '(min-width: 768px)',
            '(min-width: 992px)',
            '(min-width: 1200px)',
            '(min-width: 1400px)',
        ];
        this._breakpointSubscription = this._breakpointObserver
            .observe(brakpoints)
            .pipe(takeUntil(this._destroy$))
            .subscribe((result) => {
            Object.keys(this._breakpoints).forEach((key, index) => {
                const brakpointValue = brakpoints[index];
                const newBreakpoint = result.breakpoints[brakpointValue];
                const isBreakpointChanged = newBreakpoint !== this._breakpoints[key];
                if (!isBreakpointChanged) {
                    return;
                }
                this._breakpoints[key] = newBreakpoint;
                if (this._open) {
                    this._overlayRef.updatePositionStrategy(this._createPositionStrategy());
                }
            });
        });
    }
    show() {
        this._cdRef.markForCheck();
        if (this._open) {
            return;
        }
        if (!this._overlayRef) {
            this._createOverlay();
        }
        this._portal = new TemplatePortal(this._template, this._vcr);
        this.dropdownShow.emit(this);
        this._open = true;
        this._overlayRef.attach(this._portal);
        this._listenToOutSideCick(this._overlayRef, this._dropdownToggle.nativeElement).subscribe(() => this.hide());
        this._animationState = 'visible';
    }
    hide() {
        this._cdRef.markForCheck();
        if (!this._open) {
            return;
        }
        this.dropdownHide.emit(this);
        this._animationState = 'hidden';
    }
    toggle() {
        this._cdRef.markForCheck();
        if (this._open) {
            this.hide();
        }
        else {
            this.show();
        }
    }
}
MdbDropdownDirective.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: '[mdbDropdown]',
                template: "<ng-content></ng-content>\n<ng-content select=\".dropdown-toggle\"></ng-content>\n<ng-template #dropdownTemplate>\n  <div [@fade]=\"_animationState\" (@fade.done)=\"onAnimationEnd($event)\" [@.disabled]=\"!animation\">\n    <ng-content select=\".dropdown-menu\"></ng-content>\n  </div>\n</ng-template>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                animations: [
                    trigger('fade', [
                        state('visible', style({ opacity: 1 })),
                        state('hidden', style({ opacity: 0 })),
                        transition('visible => hidden', animate('150ms linear')),
                        transition('hidden => visible', [style({ opacity: 0 }), animate('150ms linear')]),
                    ]),
                ]
            },] }
];
MdbDropdownDirective.ctorParameters = () => [
    { type: Overlay },
    { type: OverlayPositionBuilder },
    { type: ElementRef },
    { type: ViewContainerRef },
    { type: BreakpointObserver },
    { type: ChangeDetectorRef }
];
MdbDropdownDirective.propDecorators = {
    _template: [{ type: ViewChild, args: ['dropdownTemplate',] }],
    _dropdownToggle: [{ type: ContentChild, args: [MdbDropdownToggleDirective, { read: ElementRef },] }],
    _dropdownMenu: [{ type: ContentChild, args: [MdbDropdownMenuDirective, { read: ElementRef },] }],
    animation: [{ type: Input }],
    offset: [{ type: Input }],
    dropdownShow: [{ type: Output }],
    dropdownShown: [{ type: Output }],
    dropdownHide: [{ type: Output }],
    dropdownHidden: [{ type: Output }]
};

class MdbDropdownModule {
}
MdbDropdownModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, OverlayModule],
                declarations: [MdbDropdownDirective, MdbDropdownToggleDirective, MdbDropdownMenuDirective],
                exports: [MdbDropdownDirective, MdbDropdownToggleDirective, MdbDropdownMenuDirective],
            },] }
];

const TRANSITION_BREAK_OPACITY = 0.5;
const GRADIENT = 'rgba({{color}}, 0.2) 0, rgba({{color}}, 0.3) 40%, rgba({{color}}, 0.4) 50%, rgba({{color}}, 0.5) 60%, rgba({{color}}, 0) 70%';
const DEFAULT_RIPPLE_COLOR = [0, 0, 0];
const BOOTSTRAP_COLORS = [
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
    'light',
    'dark',
];
class MdbRippleDirective {
    constructor(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.rippleCentered = false;
        this.rippleColor = '';
        this.rippleDuration = '500ms';
        this.rippleRadius = 0;
        this.rippleUnbound = false;
        this.ripple = true;
    }
    get host() {
        return this._elementRef.nativeElement;
    }
    _createRipple(event) {
        const { layerX, layerY } = event;
        const offsetX = layerX;
        const offsetY = layerY;
        const height = this.host.offsetHeight;
        const width = this.host.offsetWidth;
        const duration = this._durationToMsNumber(this.rippleDuration);
        const diameterOptions = {
            offsetX: this.rippleCentered ? height / 2 : offsetX,
            offsetY: this.rippleCentered ? width / 2 : offsetY,
            height,
            width,
        };
        const diameter = this._getDiameter(diameterOptions);
        const radiusValue = this.rippleRadius || diameter / 2;
        const opacity = {
            delay: duration * TRANSITION_BREAK_OPACITY,
            duration: duration - duration * TRANSITION_BREAK_OPACITY,
        };
        const styles = {
            left: this.rippleCentered ? `${width / 2 - radiusValue}px` : `${offsetX - radiusValue}px`,
            top: this.rippleCentered ? `${height / 2 - radiusValue}px` : `${offsetY - radiusValue}px`,
            height: `${this.rippleRadius * 2 || diameter}px`,
            width: `${this.rippleRadius * 2 || diameter}px`,
            transitionDelay: `0s, ${opacity.delay}ms`,
            transitionDuration: `${duration}ms, ${opacity.duration}ms`,
        };
        const rippleHTML = this._renderer.createElement('div');
        this._createHTMLRipple(this.host, rippleHTML, styles);
        this._removeHTMLRipple(rippleHTML, duration);
    }
    _createHTMLRipple(wrapper, ripple, styles) {
        Object.keys(styles).forEach((property) => (ripple.style[property] = styles[property]));
        this._renderer.addClass(ripple, 'ripple-wave');
        if (this.rippleColor !== '') {
            this._removeOldColorClasses(wrapper);
            this._addColor(ripple, wrapper);
        }
        this._toggleUnbound(wrapper);
        this._appendRipple(ripple, wrapper);
    }
    _removeHTMLRipple(ripple, duration) {
        setTimeout(() => {
            if (ripple) {
                ripple.remove();
            }
        }, duration);
    }
    _durationToMsNumber(time) {
        return Number(time.replace('ms', '').replace('s', '000'));
    }
    _getDiameter({ offsetX, offsetY, height, width }) {
        const top = offsetY <= height / 2;
        const left = offsetX <= width / 2;
        const pythagorean = (sideA, sideB) => Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
        const positionCenter = offsetY === height / 2 && offsetX === width / 2;
        // mouse position on the quadrants of the coordinate system
        const quadrant = {
            first: top === true && left === false,
            second: top === true && left === true,
            third: top === false && left === true,
            fourth: top === false && left === false,
        };
        const getCorner = {
            topLeft: pythagorean(offsetX, offsetY),
            topRight: pythagorean(width - offsetX, offsetY),
            bottomLeft: pythagorean(offsetX, height - offsetY),
            bottomRight: pythagorean(width - offsetX, height - offsetY),
        };
        let diameter = 0;
        if (positionCenter || quadrant.fourth) {
            diameter = getCorner.topLeft;
        }
        else if (quadrant.third) {
            diameter = getCorner.topRight;
        }
        else if (quadrant.second) {
            diameter = getCorner.bottomRight;
        }
        else if (quadrant.first) {
            diameter = getCorner.bottomLeft;
        }
        return diameter * 2;
    }
    _appendRipple(target, parent) {
        const FIX_ADD_RIPPLE_EFFECT = 50; // delay for active animations
        this._renderer.appendChild(parent, target);
        setTimeout(() => {
            this._renderer.addClass(target, 'active');
        }, FIX_ADD_RIPPLE_EFFECT);
    }
    _toggleUnbound(target) {
        if (this.rippleUnbound) {
            this._renderer.addClass(target, 'ripple-surface-unbound');
        }
        else {
            this._renderer.removeClass(target, 'ripple-surface-unbound');
        }
    }
    _addColor(target, parent) {
        const isBootstrapColor = BOOTSTRAP_COLORS.find((color) => color === this.rippleColor.toLowerCase());
        if (isBootstrapColor) {
            this._renderer.addClass(parent, `${'ripple-surface'}-${this.rippleColor.toLowerCase()}`);
        }
        else {
            const rgbValue = this._colorToRGB(this.rippleColor).join(',');
            const gradientImage = GRADIENT.split('{{color}}').join(`${rgbValue}`);
            target.style.backgroundImage = `radial-gradient(circle, ${gradientImage})`;
        }
    }
    _removeOldColorClasses(target) {
        const REGEXP_CLASS_COLOR = new RegExp(`${'ripple-surface'}-[a-z]+`, 'gi');
        const PARENT_CLASSS_COLOR = target.classList.value.match(REGEXP_CLASS_COLOR) || [];
        PARENT_CLASSS_COLOR.forEach((className) => {
            this._renderer.removeClass(target, className);
        });
    }
    _colorToRGB(color) {
        // tslint:disable-next-line: no-shadowed-variable
        function hexToRgb(color) {
            const HEX_COLOR_LENGTH = 7;
            const IS_SHORT_HEX = color.length < HEX_COLOR_LENGTH;
            if (IS_SHORT_HEX) {
                color = `#${color[1]}${color[1]}${color[2]}${color[2]}${color[3]}${color[3]}`;
            }
            return [
                parseInt(color.substr(1, 2), 16),
                parseInt(color.substr(3, 2), 16),
                parseInt(color.substr(5, 2), 16),
            ];
        }
        // tslint:disable-next-line: no-shadowed-variable
        function namedColorsToRgba(color) {
            const tempElem = document.body.appendChild(document.createElement('fictum'));
            const flag = 'rgb(1, 2, 3)';
            tempElem.style.color = flag;
            if (tempElem.style.color !== flag) {
                return DEFAULT_RIPPLE_COLOR;
            }
            tempElem.style.color = color;
            if (tempElem.style.color === flag || tempElem.style.color === '') {
                return DEFAULT_RIPPLE_COLOR;
            } // color parse failed
            color = getComputedStyle(tempElem).color;
            document.body.removeChild(tempElem);
            return color;
        }
        // tslint:disable-next-line: no-shadowed-variable
        function rgbaToRgb(color) {
            color = color.match(/[.\d]+/g).map((a) => +Number(a));
            color.length = 3;
            return color;
        }
        if (color.toLowerCase() === 'transparent') {
            return DEFAULT_RIPPLE_COLOR;
        }
        if (color[0] === '#') {
            return hexToRgb(color);
        }
        if (color.indexOf('rgb') === -1) {
            color = namedColorsToRgba(color);
        }
        if (color.indexOf('rgb') === 0) {
            return rgbaToRgb(color);
        }
        return DEFAULT_RIPPLE_COLOR;
    }
}
MdbRippleDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbRipple]',
                exportAs: 'mdbRipple',
            },] }
];
MdbRippleDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
MdbRippleDirective.propDecorators = {
    rippleCentered: [{ type: Input }],
    rippleColor: [{ type: Input }],
    rippleDuration: [{ type: Input }],
    rippleRadius: [{ type: Input }],
    rippleUnbound: [{ type: Input }],
    ripple: [{ type: HostBinding, args: ['class.ripple-surface',] }],
    _createRipple: [{ type: HostListener, args: ['click', ['$event'],] }]
};

class MdbRippleModule {
}
MdbRippleModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbRippleDirective],
                imports: [],
                exports: [MdbRippleDirective],
            },] }
];

let defaultIdNumber = 0;
// tslint:disable-next-line:component-class-suffix
class MdbErrorDirective {
    constructor(_elementRef, renderer) {
        this._elementRef = _elementRef;
        this.renderer = renderer;
        this.id = `mdb-error-${defaultIdNumber++}`;
        this.errorMsg = true;
        this.messageId = this.id;
        this._destroy$ = new Subject();
    }
    _getClosestEl(el, selector) {
        for (; el && el !== document; el = el.parentNode) {
            if (el.matches && el.matches(selector)) {
                return el;
            }
        }
        return null;
    }
    ngOnInit() {
        const textarea = this._getClosestEl(this._elementRef.nativeElement, 'textarea');
        if (textarea) {
            let height = textarea.offsetHeight + 4 + 'px';
            this.renderer.setStyle(this._elementRef.nativeElement, 'top', height);
            fromEvent(textarea, 'keyup')
                .pipe(takeUntil(this._destroy$))
                .subscribe(() => {
                height = textarea.offsetHeight + 4 + 'px';
                this.renderer.setStyle(this._elementRef.nativeElement, 'top', height);
            });
        }
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
MdbErrorDirective.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-error',
                template: '<ng-content></ng-content>'
            },] }
];
MdbErrorDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
MdbErrorDirective.propDecorators = {
    id: [{ type: Input }],
    errorMsg: [{ type: HostBinding, args: ['class.error-message',] }],
    messageId: [{ type: HostBinding, args: ['attr.id',] }]
};

let defaultIdNumber$1 = 0;
// tslint:disable-next-line:component-class-suffix
class MdbSuccessDirective {
    constructor(_elementRef, renderer) {
        this._elementRef = _elementRef;
        this.renderer = renderer;
        this.id = `mdb-success-${defaultIdNumber$1++}`;
        this.successMsg = true;
        this.messageId = this.id;
        this._destroy$ = new Subject();
    }
    _getClosestEl(el, selector) {
        for (; el && el !== document; el = el.parentNode) {
            if (el.matches && el.matches(selector)) {
                return el;
            }
        }
        return null;
    }
    ngOnInit() {
        const textarea = this._getClosestEl(this._elementRef.nativeElement, 'textarea');
        if (textarea) {
            let height = textarea.offsetHeight + 4 + 'px';
            this.renderer.setStyle(this._elementRef.nativeElement, 'top', height);
            fromEvent(textarea, 'keyup')
                .pipe(takeUntil(this._destroy$))
                .subscribe(() => {
                height = textarea.offsetHeight + 4 + 'px';
                this.renderer.setStyle(this._elementRef.nativeElement, 'top', height);
            });
        }
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
MdbSuccessDirective.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-success',
                template: '<ng-content></ng-content>'
            },] }
];
MdbSuccessDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
MdbSuccessDirective.propDecorators = {
    id: [{ type: Input }],
    successMsg: [{ type: HostBinding, args: ['class.success-message',] }],
    messageId: [{ type: HostBinding, args: ['attr.id',] }]
};

class MdbValidateDirective {
    constructor(renderer, _elementRef) {
        this.renderer = renderer;
        this._elementRef = _elementRef;
        this._validate = true;
        this._validateSuccess = true;
        this._validateError = true;
    }
    get validate() {
        return this._validate;
    }
    set validate(value) {
        this._validate = value;
        this.updateErrorClass();
        this.updateSuccessClass();
    }
    get validateSuccess() {
        return this._validateSuccess;
    }
    set validateSuccess(value) {
        this._validateSuccess = value;
        this.updateSuccessClass();
    }
    get validateError() {
        return this._validateError;
    }
    set validateError(value) {
        this._validateError = value;
        this.updateErrorClass();
        this.updateSuccessClass();
    }
    updateSuccessClass() {
        if (this.validate && this.validateSuccess) {
            this.renderer.addClass(this._elementRef.nativeElement, 'validate-success');
        }
        else {
            this.renderer.removeClass(this._elementRef.nativeElement, 'validate-success');
        }
    }
    updateErrorClass() {
        if (this.validate && this.validateError) {
            this.renderer.addClass(this._elementRef.nativeElement, 'validate-error');
        }
        else {
            this.renderer.removeClass(this._elementRef.nativeElement, 'validate-error');
        }
    }
    ngOnInit() {
        this.updateSuccessClass();
        this.updateErrorClass();
    }
}
MdbValidateDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbValidate]',
            },] }
];
MdbValidateDirective.ctorParameters = () => [
    { type: Renderer2 },
    { type: ElementRef }
];
MdbValidateDirective.propDecorators = {
    mdbValidate: [{ type: Input }],
    validate: [{ type: Input }],
    validateSuccess: [{ type: Input }],
    validateError: [{ type: Input }]
};

class MdbValidationModule {
}
MdbValidationModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                declarations: [MdbErrorDirective, MdbSuccessDirective, MdbValidateDirective],
                exports: [MdbErrorDirective, MdbSuccessDirective, MdbValidateDirective],
            },] }
];

class MdbScrollspyLinkDirective {
    constructor(cdRef, document) {
        this.cdRef = cdRef;
        this.document = document;
        this._scrollIntoView = true;
        this.scrollspyLink = true;
        this.active = false;
    }
    get scrollIntoView() {
        return this._scrollIntoView;
    }
    set scrollIntoView(value) {
        this._scrollIntoView = value;
    }
    get section() {
        return this._section;
    }
    set section(value) {
        if (value) {
            this._section = value;
        }
    }
    get id() {
        return this._id;
    }
    set id(newId) {
        if (newId) {
            this._id = newId;
        }
    }
    onClick() {
        if (this.section && this.scrollIntoView === true) {
            this.section.scrollIntoView();
        }
    }
    detectChanges() {
        this.cdRef.detectChanges();
    }
    assignSectionToId() {
        this.section = this.document.documentElement.querySelector(`#${this.id}`);
    }
    ngOnInit() {
        this.assignSectionToId();
    }
}
MdbScrollspyLinkDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbScrollspyLink]',
            },] }
];
MdbScrollspyLinkDirective.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
MdbScrollspyLinkDirective.propDecorators = {
    scrollIntoView: [{ type: Input }],
    id: [{ type: Input, args: ['mdbScrollspyLink',] }],
    scrollspyLink: [{ type: HostBinding, args: ['class.scrollspy-link',] }],
    active: [{ type: HostBinding, args: ['class.active',] }],
    onClick: [{ type: HostListener, args: ['click', [],] }]
};

class MdbScrollspyService {
    constructor() {
        this.scrollSpys = [];
        this.activeSubject = new Subject();
        this.active$ = this.activeSubject;
    }
    addScrollspy(scrollSpy) {
        this.scrollSpys.push(scrollSpy);
    }
    removeScrollspy(scrollSpyId) {
        const scrollSpyIndex = this.scrollSpys.findIndex((spy) => {
            return spy.id === scrollSpyId;
        });
        this.scrollSpys.splice(scrollSpyIndex, 1);
    }
    updateActiveState(scrollSpyId, activeLinkId) {
        const scrollSpy = this.scrollSpys.find((spy) => {
            return spy.id === scrollSpyId;
        });
        if (!scrollSpy) {
            return;
        }
        const activeLink = scrollSpy.links.find((link) => {
            return link.id === activeLinkId;
        });
        this.setActiveLink(activeLink);
    }
    removeActiveState(scrollSpyId, activeLinkId) {
        const scrollSpy = this.scrollSpys.find((spy) => {
            return spy.id === scrollSpyId;
        });
        if (!scrollSpy) {
            return;
        }
        const activeLink = scrollSpy.links.find((link) => {
            return link.id === activeLinkId;
        });
        if (!activeLink) {
            return;
        }
        activeLink.active = false;
        activeLink.detectChanges();
    }
    setActiveLink(activeLink) {
        if (activeLink) {
            activeLink.active = true;
            activeLink.detectChanges();
            this.activeSubject.next(activeLink);
        }
    }
    removeActiveLinks(scrollSpyId) {
        const scrollSpy = this.scrollSpys.find((spy) => {
            return spy.id === scrollSpyId;
        });
        if (!scrollSpy) {
            return;
        }
        scrollSpy.links.forEach((link) => {
            link.active = false;
            link.detectChanges();
        });
    }
}
MdbScrollspyService.decorators = [
    { type: Injectable }
];

// tslint:disable-next-line:component-class-suffix
class MdbScrollspyDirective {
    constructor(scrollSpyService) {
        this.scrollSpyService = scrollSpyService;
        this._destroy$ = new Subject();
        this.activeLinkChange = new EventEmitter();
    }
    get id() {
        return this._id;
    }
    set id(newId) {
        if (newId) {
            this._id = newId;
        }
    }
    ngOnInit() {
        this.activeSub = this.scrollSpyService.active$
            .pipe(takeUntil(this._destroy$), distinctUntilChanged())
            .subscribe((activeLink) => {
            this.activeLinkChange.emit(activeLink);
        });
    }
    ngAfterContentInit() {
        this.scrollSpyService.addScrollspy({ id: this.id, links: this.links });
    }
    ngOnDestroy() {
        this.scrollSpyService.removeScrollspy(this.id);
        this._destroy$.next();
        this._destroy$.complete();
    }
}
MdbScrollspyDirective.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line:component-selector
                selector: '[mdbScrollspy]',
                template: '<ng-content></ng-content>'
            },] }
];
MdbScrollspyDirective.ctorParameters = () => [
    { type: MdbScrollspyService }
];
MdbScrollspyDirective.propDecorators = {
    links: [{ type: ContentChildren, args: [MdbScrollspyLinkDirective, { descendants: true },] }],
    id: [{ type: Input, args: ['mdbScrollspy',] }],
    activeLinkChange: [{ type: Output }]
};

// tslint:disable-next-line: directive-class-suffix
class MdbScrollspyElementDirective {
    constructor(_elementRef, renderer, ngZone, scrollSpyService) {
        this._elementRef = _elementRef;
        this.renderer = renderer;
        this.ngZone = ngZone;
        this.scrollSpyService = scrollSpyService;
        this.offset = 0;
    }
    get host() {
        return this._elementRef.nativeElement;
    }
    get scrollSpyId() {
        return this._scrollSpyId;
    }
    set scrollSpyId(newId) {
        if (newId) {
            this._scrollSpyId = newId;
        }
    }
    isElementInViewport() {
        const scrollTop = this.container.scrollTop;
        const elTop = this.host.offsetTop - this.offset;
        const elHeight = this.host.offsetHeight;
        return scrollTop >= elTop && scrollTop < elTop + elHeight;
    }
    updateActiveState(scrollSpyId, id) {
        if (this.isElementInViewport()) {
            this.scrollSpyService.removeActiveLinks(scrollSpyId);
            this.scrollSpyService.updateActiveState(scrollSpyId, id);
        }
    }
    onScroll() {
        this.updateActiveState(this.scrollSpyId, this.id);
    }
    listenToScroll() {
        this.renderer.listen(this.container, 'scroll', () => {
            this.onScroll();
        });
    }
    ngOnInit() {
        this.id = this.host.id;
        if (!this.container) {
            this.container = this._getClosestEl(this.host, '.scrollspy-container');
        }
        this.renderer.setStyle(this.container, 'position', 'relative');
        this.ngZone.runOutsideAngular(this.listenToScroll.bind(this));
    }
    ngAfterViewInit() {
        setTimeout(() => {
            this.updateActiveState(this.scrollSpyId, this.id);
        }, 0);
    }
    _getClosestEl(el, selector) {
        for (; el && el !== document; el = el.parentNode) {
            if (el.matches && el.matches(selector)) {
                return el;
            }
        }
        return null;
    }
}
MdbScrollspyElementDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbScrollspyElement]',
            },] }
];
MdbScrollspyElementDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgZone },
    { type: MdbScrollspyService }
];
MdbScrollspyElementDirective.propDecorators = {
    container: [{ type: Input }],
    scrollSpyId: [{ type: Input, args: ['mdbScrollspyElement',] }],
    offset: [{ type: Input }]
};

class MdbScrollspyWindowDirective {
    constructor(document, el, renderer, ngZone, scrollSpyService) {
        this.document = document;
        this.el = el;
        this.renderer = renderer;
        this.ngZone = ngZone;
        this.scrollSpyService = scrollSpyService;
        this.offset = 0;
    }
    get scrollSpyId() {
        return this._scrollSpyId;
    }
    set scrollSpyId(newId) {
        if (newId) {
            this._scrollSpyId = newId;
        }
    }
    isElementInViewport() {
        const scrollTop = this.document.documentElement.scrollTop || this.document.body.scrollTop;
        const elHeight = this.el.nativeElement.offsetHeight;
        const elTop = this.el.nativeElement.offsetTop - this.offset;
        const elBottom = elTop + elHeight;
        return scrollTop >= elTop && scrollTop <= elBottom;
    }
    updateActiveState(scrollSpyId, id) {
        if (this.isElementInViewport()) {
            this.scrollSpyService.updateActiveState(scrollSpyId, id);
        }
        else {
            this.scrollSpyService.removeActiveState(scrollSpyId, id);
        }
    }
    onScroll() {
        this.updateActiveState(this.scrollSpyId, this.id);
    }
    listenToScroll() {
        this.renderer.listen(window, 'scroll', () => {
            this.onScroll();
        });
    }
    ngOnInit() {
        this.id = this.el.nativeElement.id;
        this.ngZone.runOutsideAngular(this.listenToScroll.bind(this));
    }
    ngAfterViewInit() {
        setTimeout(() => {
            this.updateActiveState(this.scrollSpyId, this.id);
        }, 0);
    }
}
MdbScrollspyWindowDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbScrollspyWindow]',
            },] }
];
MdbScrollspyWindowDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgZone },
    { type: MdbScrollspyService }
];
MdbScrollspyWindowDirective.propDecorators = {
    scrollSpyId: [{ type: Input, args: ['mdbScrollspyWindow',] }],
    offset: [{ type: Input }]
};

class MdbScrollspyModule {
}
MdbScrollspyModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    MdbScrollspyDirective,
                    MdbScrollspyLinkDirective,
                    MdbScrollspyElementDirective,
                    MdbScrollspyWindowDirective,
                ],
                exports: [
                    MdbScrollspyDirective,
                    MdbScrollspyLinkDirective,
                    MdbScrollspyElementDirective,
                    MdbScrollspyWindowDirective,
                ],
                providers: [MdbScrollspyService],
            },] }
];

const RANGE_VALUE_ACCESOR = {
    provide: NG_VALUE_ACCESSOR,
    // tslint:disable-next-line: no-use-before-declare
    useExisting: forwardRef(() => MdbRangeComponent),
    multi: true,
};
class MdbRangeComponent {
    constructor(_cdRef) {
        this._cdRef = _cdRef;
        this.min = 0;
        this.max = 100;
        this.rangeValueChange = new EventEmitter();
        this.visibility = false;
        // Control Value Accessor Methods
        this.onChange = (_) => { };
        this.onTouched = () => { };
    }
    onchange(event) {
        this.onChange(event.target.value);
    }
    onInput() {
        this.rangeValueChange.emit({ value: this.value });
        this.focusRangeInput();
    }
    ngAfterViewInit() {
        this.thumbPositionUpdate();
    }
    focusRangeInput() {
        this.input.nativeElement.focus();
        this.visibility = true;
    }
    blurRangeInput() {
        this.input.nativeElement.blur();
        this.visibility = false;
    }
    thumbPositionUpdate() {
        const rangeInput = this.input.nativeElement;
        const inputValue = rangeInput.value;
        const minValue = rangeInput.min ? rangeInput.min : 0;
        const maxValue = rangeInput.max ? rangeInput.max : 100;
        const newValue = Number(((inputValue - minValue) * 100) / (maxValue - minValue));
        this.value = inputValue;
        this.thumbStyle = { left: `calc(${newValue}% + (${8 - newValue * 0.15}px))` };
    }
    writeValue(value) {
        this.value = value;
        this._cdRef.markForCheck();
        setTimeout(() => {
            this.thumbPositionUpdate();
        }, 0);
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled) {
        this.disabled = isDisabled;
    }
}
MdbRangeComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-range',
                template: "<label for=\"customRange2\" class=\"form-label\">Example range</label>\n<div class=\"range\">\n  <input\n    #input\n    [name]=\"name\"\n    type=\"range\"\n    [disabled]=\"disabled\"\n    [id]=\"id\"\n    [min]=\"min\"\n    [max]=\"max\"\n    [step]=\"step\"\n    [value]=\"value\"\n    [(ngModel)]=\"value\"\n    class=\"form-range\"\n    min=\"0\"\n    max=\"5\"\n    [id]=\"id\"\n    (input)=\"thumbPositionUpdate()\"\n    (blur)=\"blurRangeInput()\"\n    (mousedown)=\"focusRangeInput()\"\n    (mouseup)=\"blurRangeInput()\"\n    (touchstart)=\"focusRangeInput()\"\n    (touchend)=\"blurRangeInput()\"\n  />\n  <span #thumb class=\"thumb\" [ngStyle]=\"thumbStyle\" [ngClass]=\"{ 'thumb-active': this.visibility }\">\n    <span #thumbValue class=\"thumb-value\">{{ value }}</span>\n  </span>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                providers: [RANGE_VALUE_ACCESOR]
            },] }
];
MdbRangeComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
MdbRangeComponent.propDecorators = {
    input: [{ type: ViewChild, args: ['input',] }],
    thumb: [{ type: ViewChild, args: ['thumb',] }],
    thumbValue: [{ type: ViewChild, args: ['thumbValue',] }],
    id: [{ type: Input }],
    required: [{ type: Input }],
    name: [{ type: Input }],
    value: [{ type: Input }],
    disabled: [{ type: Input }],
    min: [{ type: Input }],
    max: [{ type: Input }],
    step: [{ type: Input }],
    default: [{ type: Input }],
    defaultRangeCounterClass: [{ type: Input }],
    rangeValueChange: [{ type: Output }],
    onchange: [{ type: HostListener, args: ['change', ['$event'],] }],
    onInput: [{ type: HostListener, args: ['input',] }]
};

class MdbRangeModule {
}
MdbRangeModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule],
                declarations: [MdbRangeComponent],
                exports: [MdbRangeComponent],
            },] }
];

const MDB_TAB_CONTENT = new InjectionToken('MdbTabContentDirective');
class MdbTabContentDirective {
    constructor(template) {
        this.template = template;
    }
}
MdbTabContentDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbTabContent]',
                providers: [{ provide: MDB_TAB_CONTENT, useExisting: MdbTabContentDirective }],
            },] }
];
MdbTabContentDirective.ctorParameters = () => [
    { type: TemplateRef }
];

const MDB_TAB_TITLE = new InjectionToken('MdbTabTitleDirective');
class MdbTabTitleDirective {
    constructor(template) {
        this.template = template;
    }
}
MdbTabTitleDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbTabTitle]',
                providers: [{ provide: MDB_TAB_TITLE, useExisting: MdbTabTitleDirective }],
            },] }
];
MdbTabTitleDirective.ctorParameters = () => [
    { type: TemplateRef }
];

class MdbTabComponent {
    constructor(_elementRef, _renderer, _vcr) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._vcr = _vcr;
        this.activeStateChange$ = new Subject();
        this.disabled = false;
        this._contentPortal = null;
        this._titlePortal = null;
        this._active = false;
    }
    get active() {
        return this._active;
    }
    get content() {
        return this._contentPortal;
    }
    get titleContent() {
        return this._titlePortal;
    }
    get shouldAttach() {
        return this._lazyContent === undefined;
    }
    // tslint:disable-next-line: adjacent-overload-signatures
    set active(value) {
        if (value) {
            this._renderer.addClass(this._elementRef.nativeElement, 'show');
            this._renderer.addClass(this._elementRef.nativeElement, 'active');
        }
        else {
            this._renderer.removeClass(this._elementRef.nativeElement, 'show');
            this._renderer.removeClass(this._elementRef.nativeElement, 'active');
        }
        this._active = value;
        this.activeStateChange$.next(value);
    }
    ngOnInit() {
        this._createContentPortal();
        if (this._titleContent) {
            this._createTitlePortal();
        }
    }
    _createContentPortal() {
        const content = this._lazyContent || this._content;
        this._contentPortal = new TemplatePortal(content, this._vcr);
    }
    _createTitlePortal() {
        this._titlePortal = new TemplatePortal(this._titleContent, this._vcr);
    }
}
MdbTabComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-tab',
                template: "<ng-template><ng-content></ng-content></ng-template>\n"
            },] }
];
MdbTabComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: ViewContainerRef }
];
MdbTabComponent.propDecorators = {
    _lazyContent: [{ type: ContentChild, args: [MDB_TAB_CONTENT, { read: TemplateRef, static: true },] }],
    _titleContent: [{ type: ContentChild, args: [MDB_TAB_TITLE, { read: TemplateRef, static: true },] }],
    _content: [{ type: ViewChild, args: [TemplateRef, { static: true },] }],
    disabled: [{ type: Input }],
    title: [{ type: Input }]
};

class MdbTabChange {
}
class MdbTabsComponent {
    constructor() {
        this._destroy$ = new Subject();
        this.fill = false;
        this.justified = false;
        this.pills = false;
        this.vertical = false;
        this.activeTabChange = new EventEmitter();
    }
    ngAfterContentInit() {
        const firstActiveTabIndex = this.tabs.toArray().findIndex((tab) => !tab.disabled);
        this.setActiveTab(firstActiveTabIndex);
        // tslint:disable-next-line: deprecation
        this.tabs.changes.pipe(takeUntil(this._destroy$)).subscribe(() => {
            const hasActiveTab = this.tabs.find((tab) => tab.active);
            if (!hasActiveTab) {
                const closestTabIndex = this._getClosestTabIndex(this._selectedIndex);
                if (closestTabIndex !== -1) {
                    this.setActiveTab(closestTabIndex);
                }
            }
        });
    }
    setActiveTab(index) {
        const activeTab = this.tabs.toArray()[index];
        if (!activeTab || (activeTab && activeTab.disabled)) {
            return;
        }
        this.tabs.forEach((tab) => (tab.active = tab === activeTab));
        this._selectedIndex = index;
        const tabChangeEvent = this._getTabChangeEvent(index, activeTab);
        this.activeTabChange.emit(tabChangeEvent);
    }
    _getTabChangeEvent(index, tab) {
        const event = new MdbTabChange();
        event.index = index;
        event.tab = tab;
        return event;
    }
    _getClosestTabIndex(index) {
        const tabs = this.tabs.toArray();
        const tabsLength = tabs.length;
        if (!tabsLength) {
            return -1;
        }
        for (let i = 1; i <= tabsLength; i += 1) {
            const prevIndex = index - i;
            const nextIndex = index + i;
            if (tabs[prevIndex] && !tabs[prevIndex].disabled) {
                return prevIndex;
            }
            if (tabs[nextIndex] && !tabs[nextIndex].disabled) {
                return nextIndex;
            }
        }
        return -1;
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
MdbTabsComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-tabs',
                template: "<ul\n  class=\"nav mb-3 col-3 flex-column\"\n  [ngClass]=\"{\n    'nav-pills': pills,\n    'nav-tabs': !pills,\n    'nav-fill': fill,\n    'nav-justified': justified,\n    'flex-column': vertical,\n    'col-3': vertical,\n    'text-center': vertical\n  }\"\n  role=\"tablist\"\n>\n  <li\n    *ngFor=\"let tab of tabs; let i = index\"\n    (click)=\"setActiveTab(i)\"\n    class=\"nav-item\"\n    role=\"presentation\"\n  >\n    <a\n      href=\"javascript:void(0)\"\n      class=\"nav-link\"\n      [class.active]=\"tab.active\"\n      [class.disabled]=\"tab.disabled\"\n      role=\"tab\"\n    >\n      <ng-template [ngIf]=\"tab.titleContent\">\n        <ng-template [cdkPortalOutlet]=\"tab.titleContent\"></ng-template>\n      </ng-template>\n\n      <ng-template [ngIf]=\"!tab.titleContent\">{{ tab.title }}</ng-template>\n    </a>\n  </li>\n</ul>\n\n<div\n  class=\"tab-content\"\n  [ngClass]=\"{\n    'col-9': vertical\n  }\"\n>\n  <!-- <ng-content select=\"mdb-tab\"></ng-content> -->\n  <ng-container *ngFor=\"let tab of tabs\">\n    <div\n      class=\"tab-pane fade\"\n      [ngClass]=\"{\n        show: tab.active,\n        active: tab.active\n      }\"\n    >\n      <ng-template mdbTabPortalOutlet [tab]=\"tab\"></ng-template>\n    </div>\n  </ng-container>\n</div>\n"
            },] }
];
MdbTabsComponent.ctorParameters = () => [];
MdbTabsComponent.propDecorators = {
    tabs: [{ type: ContentChildren, args: [MdbTabComponent,] }],
    fill: [{ type: Input }],
    justified: [{ type: Input }],
    pills: [{ type: Input }],
    vertical: [{ type: HostBinding, args: ['class.row',] }, { type: Input }],
    activeTabChange: [{ type: Output }]
};

// tslint:disable-next-line: directive-class-suffix
class MdbTabPortalOutlet extends CdkPortalOutlet {
    constructor(_cfr, _vcr, _document) {
        super(_cfr, _vcr, _document);
        this._destroy$ = new Subject();
    }
    ngOnInit() {
        super.ngOnInit();
        if ((this.tab.shouldAttach || this.tab.active) && !this.hasAttached()) {
            this.attach(this.tab.content);
        }
        else {
            // tslint:disable-next-line: deprecation
            this.tab.activeStateChange$.pipe(takeUntil(this._destroy$)).subscribe((isActive) => {
                if (isActive && !this.hasAttached()) {
                    this.attach(this.tab.content);
                }
            });
        }
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
        super.ngOnDestroy();
    }
}
MdbTabPortalOutlet.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line: directive-selector
                selector: '[mdbTabPortalOutlet]',
            },] }
];
MdbTabPortalOutlet.ctorParameters = () => [
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
MdbTabPortalOutlet.propDecorators = {
    tab: [{ type: Input }]
};

class MdbTabsModule {
}
MdbTabsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    MdbTabComponent,
                    MdbTabContentDirective,
                    MdbTabTitleDirective,
                    MdbTabPortalOutlet,
                    MdbTabsComponent,
                ],
                imports: [CommonModule, PortalModule],
                exports: [
                    MdbTabComponent,
                    MdbTabContentDirective,
                    MdbTabTitleDirective,
                    MdbTabPortalOutlet,
                    MdbTabsComponent,
                ],
            },] }
];

class MdbCarouselItemComponent {
    constructor(_elementRef) {
        this._elementRef = _elementRef;
        this.interval = null;
        this.carouselItem = true;
        this.active = false;
        this.next = false;
        this.prev = false;
        this.start = false;
        this.end = false;
    }
    get host() {
        return this._elementRef.nativeElement;
    }
    ngOnInit() { }
}
MdbCarouselItemComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-carousel-item',
                template: '<ng-content></ng-content>'
            },] }
];
MdbCarouselItemComponent.ctorParameters = () => [
    { type: ElementRef }
];
MdbCarouselItemComponent.propDecorators = {
    interval: [{ type: Input }],
    carouselItem: [{ type: HostBinding, args: ['class.carousel-item',] }],
    active: [{ type: HostBinding, args: ['class.active',] }],
    next: [{ type: HostBinding, args: ['class.carousel-item-next',] }],
    prev: [{ type: HostBinding, args: ['class.carousel-item-prev',] }],
    start: [{ type: HostBinding, args: ['class.carousel-item-start',] }],
    end: [{ type: HostBinding, args: ['class.carousel-item-end',] }]
};

var Direction;
(function (Direction) {
    Direction[Direction["UNKNOWN"] = 0] = "UNKNOWN";
    Direction[Direction["NEXT"] = 1] = "NEXT";
    Direction[Direction["PREV"] = 2] = "PREV";
})(Direction || (Direction = {}));
class MdbCarouselComponent {
    constructor(_elementRef, _cdRef) {
        this._elementRef = _elementRef;
        this._cdRef = _cdRef;
        this.animation = 'slide';
        this.controls = false;
        this.dark = false;
        this.indicators = false;
        this.ride = true;
        this._interval = 5000;
        this.keyboard = true;
        this.pause = true;
        this.wrap = true;
        this.slide = new EventEmitter();
        this.slideChange = new EventEmitter();
        this._activeSlide = 0;
        this._isPlaying = false;
        this._isSliding = false;
        this._destroy$ = new Subject();
    }
    get items() {
        return this._items && this._items.toArray();
    }
    get interval() {
        return this._interval;
    }
    set interval(value) {
        this._interval = value;
        if (this.items) {
            this._restartInterval();
        }
    }
    get activeSlide() {
        return this._activeSlide;
    }
    set activeSlide(index) {
        if (this.items.length && this._activeSlide !== index) {
            this._activeSlide = index;
            this._restartInterval();
        }
    }
    onMouseEnter() {
        if (this.pause && this._isPlaying) {
            this.stop();
        }
    }
    onMouseLeave() {
        if (this.pause && !this._isPlaying) {
            this.play();
        }
    }
    ngAfterViewInit() {
        Promise.resolve().then(() => {
            this._setActiveSlide(this._activeSlide);
            if (this.interval > 0 && this.ride) {
                this.play();
            }
        });
        if (this.keyboard) {
            fromEvent(this._elementRef.nativeElement, 'keydown')
                .pipe(takeUntil(this._destroy$))
                // tslint:disable-next-line: deprecation
                .subscribe((event) => {
                if (event.key === 'ArrowRight') {
                    this.next();
                }
                else if (event.key === 'ArrowLeft') {
                    this.prev();
                }
            });
        }
    }
    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
    _setActiveSlide(index) {
        const currentSlide = this.items[this._activeSlide];
        currentSlide.active = false;
        const newSlide = this.items[index];
        newSlide.active = true;
        this._activeSlide = index;
    }
    _restartInterval() {
        this._resetInterval();
        const activeElement = this.items[this.activeSlide];
        const interval = activeElement.interval ? activeElement.interval : this.interval;
        if (!isNaN(interval) && interval > 0) {
            this._lastInterval = setInterval(() => {
                const nInterval = +interval;
                if (this._isPlaying && !isNaN(nInterval) && nInterval > 0) {
                    this.next();
                }
                else {
                    this.stop();
                }
            }, interval);
        }
    }
    _resetInterval() {
        if (this._lastInterval) {
            clearInterval(this._lastInterval);
            this._lastInterval = null;
        }
    }
    play() {
        if (!this._isPlaying) {
            this._isPlaying = true;
            this._restartInterval();
        }
    }
    stop() {
        if (this._isPlaying) {
            this._isPlaying = false;
            this._resetInterval();
        }
    }
    to(index) {
        if (index > this.items.length - 1 || index < 0) {
            return;
        }
        if (this.activeSlide === index) {
            this.stop();
            this.play();
            return;
        }
        const direction = index > this.activeSlide ? Direction.NEXT : Direction.PREV;
        this._animateSlides(direction, this.activeSlide, index);
        this.activeSlide = index;
        this._cdRef.markForCheck();
    }
    next() {
        if (!this._isSliding) {
            this._slide(Direction.NEXT);
        }
        this._cdRef.markForCheck();
    }
    prev() {
        if (!this._isSliding) {
            this._slide(Direction.PREV);
        }
        this._cdRef.markForCheck();
    }
    _slide(direction) {
        const isFirst = this._activeSlide === 0;
        const isLast = this._activeSlide === this.items.length - 1;
        if (!this.wrap) {
            if ((direction === Direction.NEXT && isLast) || (direction === Direction.PREV && isFirst)) {
                return;
            }
        }
        const newSlideIndex = this._getNewSlideIndex(direction);
        this._animateSlides(direction, this.activeSlide, newSlideIndex);
        this.activeSlide = newSlideIndex;
        this.slide.emit();
    }
    _animateSlides(direction, currentIndex, nextIndex) {
        const currentItem = this.items[currentIndex];
        const nextItem = this.items[nextIndex];
        const currentEl = currentItem.host;
        const nextEl = nextItem.host;
        this._isSliding = true;
        if (this._isPlaying) {
            this.stop();
        }
        if (direction === Direction.NEXT) {
            nextItem.next = true;
            setTimeout(() => {
                this._reflow(nextEl);
                currentItem.start = true;
                nextItem.start = true;
            }, 0);
            const transitionDuration = 600;
            fromEvent(currentEl, 'transitionend')
                .pipe(take(1))
                // tslint:disable-next-line: deprecation
                .subscribe(() => {
                nextItem.next = false;
                nextItem.start = false;
                nextItem.active = true;
                currentItem.active = false;
                currentItem.start = false;
                currentItem.next = false;
                this.slideChange.emit();
                this._isSliding = false;
            });
            this._emulateTransitionEnd(currentEl, transitionDuration);
        }
        else if (direction === Direction.PREV) {
            nextItem.prev = true;
            setTimeout(() => {
                this._reflow(nextEl);
                currentItem.end = true;
                nextItem.end = true;
            }, 0);
            const transitionDuration = 600;
            fromEvent(currentEl, 'transitionend')
                .pipe(take(1))
                // tslint:disable-next-line: deprecation
                .subscribe(() => {
                nextItem.prev = false;
                nextItem.end = false;
                nextItem.active = true;
                currentItem.active = false;
                currentItem.end = false;
                currentItem.prev = false;
                this.slideChange.emit();
                this._isSliding = false;
            });
            this._emulateTransitionEnd(currentEl, transitionDuration);
        }
        if (!this._isPlaying && this.interval > 0) {
            this.play();
        }
    }
    _reflow(element) {
        return element.offsetHeight;
    }
    _emulateTransitionEnd(element, duration) {
        let eventEmitted = false;
        const durationPadding = 5;
        const emulatedDuration = duration + durationPadding;
        fromEvent(element, 'transitionend')
            .pipe(take(1))
            // tslint:disable-next-line: deprecation
            .subscribe(() => {
            eventEmitted = true;
        });
        setTimeout(() => {
            if (!eventEmitted) {
                element.dispatchEvent(new Event('transitionend'));
            }
        }, emulatedDuration);
    }
    _getNewSlideIndex(direction) {
        let newSlideIndex;
        if (direction === Direction.NEXT) {
            newSlideIndex = this._getNextSlideIndex();
        }
        if (direction === Direction.PREV) {
            newSlideIndex = this._getPrevSlideIndex();
        }
        return newSlideIndex;
    }
    _getNextSlideIndex() {
        const isLast = this._activeSlide === this.items.length - 1;
        if (!isLast) {
            return this._activeSlide + 1;
        }
        else if (this.wrap && isLast) {
            return 0;
        }
        else {
            return this._activeSlide;
        }
    }
    _getPrevSlideIndex() {
        const isFirst = this._activeSlide === 0;
        if (!isFirst) {
            return this._activeSlide - 1;
        }
        else if (this.wrap && isFirst) {
            return this.items.length - 1;
        }
        else {
            return this._activeSlide;
        }
    }
}
MdbCarouselComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mdb-carousel',
                template: "<div\n  class=\"carousel slide\"\n  [class.carousel-fade]=\"animation === 'fade'\"\n  [class.carousel-dark]=\"dark\"\n>\n  <div class=\"carousel-indicators\" *ngIf=\"indicators\">\n    <button\n      *ngFor=\"let item of items; let i = index\"\n      type=\"button\"\n      [class.active]=\"i === activeSlide\"\n      [attr.aria-current]=\"i === activeSlide\"\n      (click)=\"to(i)\"\n    ></button>\n  </div>\n\n  <div class=\"carousel-inner\">\n    <ng-content></ng-content>\n  </div>\n\n  <button *ngIf=\"controls\" class=\"carousel-control-prev\" type=\"button\" (click)=\"prev()\">\n    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n    <span class=\"visually-hidden\">Previous</span>\n  </button>\n  <button *ngIf=\"controls\" class=\"carousel-control-next\" type=\"button\" (click)=\"next()\">\n    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n    <span class=\"visually-hidden\">Next</span>\n  </button>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush
            },] }
];
MdbCarouselComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: ChangeDetectorRef }
];
MdbCarouselComponent.propDecorators = {
    _items: [{ type: ContentChildren, args: [MdbCarouselItemComponent,] }],
    animation: [{ type: Input }],
    controls: [{ type: Input }],
    dark: [{ type: Input }],
    indicators: [{ type: Input }],
    ride: [{ type: Input }],
    interval: [{ type: Input }],
    keyboard: [{ type: Input }],
    pause: [{ type: Input }],
    wrap: [{ type: Input }],
    slide: [{ type: Output }],
    slideChange: [{ type: Output }],
    onMouseEnter: [{ type: HostListener, args: ['mouseenter',] }],
    onMouseLeave: [{ type: HostListener, args: ['mouseleave',] }]
};

class MdbCarouselModule {
}
MdbCarouselModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MdbCarouselComponent, MdbCarouselItemComponent],
                exports: [MdbCarouselComponent, MdbCarouselItemComponent],
                imports: [CommonModule],
            },] }
];

const MDB_MODULES = [
    MdbCollapseModule,
    MdbCheckboxModule,
    MdbRadioModule,
    MdbTooltipModule,
    MdbPopoverModule,
    MdbFormsModule,
    MdbModalModule,
    MdbDropdownModule,
    MdbRippleModule,
    MdbValidationModule,
    MdbScrollspyModule,
    MdbRangeModule,
    MdbTabsModule,
    MdbCarouselModule,
];
class MdbModule {
}
MdbModule.decorators = [
    { type: NgModule, args: [{
                declarations: [],
                imports: [MDB_MODULES],
                exports: [MDB_MODULES],
            },] }
];

/**
 * Generated bundle index. Do not edit.
 */

export { MDB_CHECKBOX_VALUE_ACCESSOR, MDB_RADIO_GROUP_VALUE_ACCESSOR, MdbCarouselComponent, MdbCarouselItemComponent, MdbCarouselModule, MdbCheckboxChange, MdbCheckboxDirective, MdbCheckboxModule, MdbCollapseDirective, MdbCollapseModule, MdbDropdownDirective, MdbDropdownMenuDirective, MdbDropdownModule, MdbDropdownToggleDirective, MdbErrorDirective, MdbFormControlComponent, MdbFormsModule, MdbInputDirective, MdbLabelDirective, MdbModalConfig, MdbModalContainerComponent, MdbModalModule, MdbModalRef, MdbModalService, MdbModule, MdbPopoverDirective, MdbPopoverModule, MdbRadioDirective, MdbRadioGroupDirective, MdbRadioModule, MdbRangeComponent, MdbRangeModule, MdbRippleDirective, MdbRippleModule, MdbScrollspyDirective, MdbScrollspyElementDirective, MdbScrollspyLinkDirective, MdbScrollspyModule, MdbScrollspyService, MdbSuccessDirective, MdbTabComponent, MdbTabContentDirective, MdbTabTitleDirective, MdbTabsComponent, MdbTabsModule, MdbTooltipDirective, MdbTooltipModule, MdbValidateDirective, MdbValidationModule, MdbScrollspyWindowDirective as ɵa, RANGE_VALUE_ACCESOR as ɵb, MDB_TAB_CONTENT as ɵc, MDB_TAB_TITLE as ɵd, MdbTooltipComponent as ɵe, MdbPopoverComponent as ɵf, MdbAbstractFormControl as ɵg, MdbTabPortalOutlet as ɵh };
//# sourceMappingURL=mdb-angular-ui-kit.js.map
