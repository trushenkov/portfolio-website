import { ElementRef, Renderer2 } from '@angular/core';
export declare class MdbRippleDirective {
    private _elementRef;
    private _renderer;
    rippleCentered: boolean;
    rippleColor: string;
    rippleDuration: string;
    rippleRadius: number;
    rippleUnbound: boolean;
    constructor(_elementRef: ElementRef, _renderer: Renderer2);
    get host(): HTMLElement;
    ripple: boolean;
    _createRipple(event: any): void;
    private _createHTMLRipple;
    private _removeHTMLRipple;
    private _durationToMsNumber;
    _getDiameter({ offsetX, offsetY, height, width }: {
        offsetX: any;
        offsetY: any;
        height: any;
        width: any;
    }): number;
    _appendRipple(target: HTMLElement, parent: HTMLElement): void;
    _toggleUnbound(target: HTMLElement): void;
    _addColor(target: HTMLElement, parent: HTMLElement): void;
    _removeOldColorClasses(target: HTMLElement): void;
    _colorToRGB(color: any): number[];
}
