import { ChangeDetectorRef, OnInit } from '@angular/core';
import { AnimationEvent } from '@angular/animations';
import { Subject } from 'rxjs';
export declare class MdbTooltipComponent implements OnInit {
    private _cdRef;
    title: string;
    html: boolean;
    animation: boolean;
    readonly _hidden: Subject<void>;
    animationState: string;
    constructor(_cdRef: ChangeDetectorRef);
    ngOnInit(): void;
    markForCheck(): void;
    onAnimationEnd(event: AnimationEvent): void;
}
